﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("MicrosoftCodeAnalysisCorrectness", "RS1024:正确比较符号", Justification = "<挂起>", Scope = "member", Target = "~M:DotNetCommon.HashHelper.GetHashCode``2(``0,``1)~System.Int32")]
