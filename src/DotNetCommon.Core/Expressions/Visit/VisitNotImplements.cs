﻿using DotNetCommon.Expressions.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Expressions.Visit
{
    /// <summary>
    /// 示例: checked(i += 1) ，不处理
    /// </summary>
    internal class AddAssignCheckedVisit : BaseVisit { }
    /// <summary>
    /// 示例: i+=1, 不处理
    /// </summary>
    internal class AddAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked(x + y) 不处理
    /// </summary>
    internal class AddCheckedVisit : BinaryVisit { }
    /// <summary>
    /// 示例: a	&amp;=b 不处理
    /// </summary>
    internal class AndAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: a=b 不处理，注意：
    /// <code>
    /// Expression&lt;Func&lt;Model&gt;&gt; expression = () => new Model { Id = 1 };
    /// </code>
    /// 这里面的赋值是：MemberAssignment =&gt; MemberBinding =&gt; object
    /// </summary>
    internal class AssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: {...} 不处理
    /// </summary>
    internal class BlockVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked((byte)intx), 不处理
    /// </summary>
    internal class ConvertCheckedVisit : BaseVisit { }
    /// <summary>
    /// 不处理
    /// </summary>
    internal class DebugInfoVisit : BaseVisit { }
    /// <summary>
    /// ide中写不出来 不处理
    /// </summary>
    internal class DecrementVisit : BaseVisit { }
    /// <summary>
    /// 示例: a/=b 不处理
    /// </summary>
    internal class DivideAssignVisit : BaseVisit { }
    /// <summary>
    /// dynamic相关 不处理
    /// </summary>
    internal class DynamicVisit : BaseVisit { }
    /// <summary>
    /// 示例: a ^= b 不处理
    /// </summary>
    internal class ExclusiveOrAssignVisit : BaseVisit { }
    /// <summary>
    /// 不处理
    /// </summary>
    internal class ExtensionVisit : BaseVisit { }
    /// <summary>
    /// goto 不处理
    /// </summary>
    internal class GotoVisit : BaseVisit { }
    /// <summary>
    /// ide中写不出来 不处理
    /// </summary>
    internal class IncrementVisit : BaseVisit { }
    /// <summary>
    /// ide中写不出来 不处理
    /// </summary>
    internal class IsFalseVisit : BaseVisit { }
    /// <summary>
    /// ide中写不出来 不处理
    /// </summary>
    internal class IsTrueVisit : BaseVisit { }
    /// <summary>
    /// goto相关 不处理
    /// </summary>
    internal class LabelVisit : BaseVisit { }
    /// <summary>
    /// 示例: a &lt;&lt;= b 不处理
    /// </summary>
    internal class LeftShiftAssignVisit : BaseVisit { }
    /// <summary>
    /// 循环 不处理
    /// </summary>
    internal class LoopVisit : BaseVisit { }
    /// <summary>
    /// 示例: a %= b 不处理
    /// </summary>
    internal class ModuloAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked(a *= b) 不处理
    /// </summary>
    internal class MultiplyAssignCheckedVisit : BaseVisit { }
    /// <summary>
    /// 示例: a *= b 不处理
    /// </summary>
    internal class MultiplyAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked(a * b) 不处理
    /// </summary>
    internal class MultiplyCheckedVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked(- intx) 不处理
    /// </summary>
    internal class NegateCheckedVisit : BaseVisit { }
    /// <summary>
    /// 示例: a |=b 不处理
    /// </summary>
    internal class OrAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: a-- 不处理
    /// </summary>
    internal class PostDecrementAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: a++ 不处理
    /// </summary>
    internal class PostIncrementAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: ide 中写不出(使用Math.Pow()), 不处理
    /// </summary>
    internal class PowerAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: ide 中写不出(使用Math.Pow()), 不处理
    /// </summary>
    internal class PowerVisit : BaseVisit { }
    /// <summary>
    /// 示例: --a 不处理
    /// </summary>
    internal class PreDecrementAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: ++a 不处理
    /// </summary>
    internal class PreIncrementAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: a >>= b 不处理
    /// </summary>
    internal class RightShiftAssignVisit : BaseVisit { }
    /// <summary>
    /// 和 dynamic DLR 相关 不处理
    /// </summary>
    internal class RuntimeVariablesVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked(a -= b) 不处理
    /// </summary>
    internal class SubtractAssignCheckedVisit : BaseVisit { }
    /// <summary>
    /// 示例: a -= b 不处理
    /// </summary>
    internal class SubtractAssignVisit : BaseVisit { }
    /// <summary>
    /// 示例: checked( a - b) 不处理
    /// </summary>
    internal class SubtractCheckedVisit : BaseVisit { }
    /// <summary>
    /// switch 不处理
    /// </summary>
    internal class SwitchVisit : BaseVisit { }
    /// <summary>
    /// throw 不处理
    /// </summary>
    internal class ThrowVisit : BaseVisit { }
    /// <summary>
    /// try 不处理
    /// </summary>
    internal class TryVisit : BaseVisit { }
    /// <summary>
    /// ide中写不出来 不处理
    /// </summary>
    internal class TypeEqualVisit : BaseVisit { }
}
