using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DotNetCommon.Expressions.Base
{
    internal class UnaryVisit : BaseVisit
    {
        protected override void Prepare(ExpressionNode node)
        {
            var unary = node.Expression as UnaryExpression;
            node.Children.Add(new ExpressionNode
            {
                Children = new List<ExpressionNode>(),
                Expression = unary.Operand,
                Parent = node
            });
        }

        internal override Expression Rebuild(ExpressionNode node)
        {
            var unary = node.Expression as UnaryExpression;
            return unary.Update(node.Children[0].Expression);
        }
    }
}
