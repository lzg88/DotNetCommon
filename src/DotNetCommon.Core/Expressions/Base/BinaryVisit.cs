using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCommon.Expressions.Base
{
    internal class BinaryVisit : BaseVisit
    {
        protected virtual string MarkString => throw new NotImplementedException();
        protected override void Prepare(ExpressionNode node)
        {
            var binary = node.Expression as BinaryExpression;
            node.Children.Add(new ExpressionNode
            {
                Children = new List<ExpressionNode>(),
                Expression = binary.Left,
                Parent = node
            });
            node.Children.Add(new ExpressionNode
            {
                Children = new List<ExpressionNode>(),
                Expression = binary.Right,
                Parent = node
            });
        }

        protected override string GenerateFullMarkString(ExpressionNode node)
        {
            return $"{node.Children[0].FullMarkString} {MarkString} {node.Children[1].FullMarkString}";
        }

        internal override Expression Rebuild(ExpressionNode node)
        {
            var binary = node.Expression as BinaryExpression;
            return binary.Update(node.Children[0].Expression, binary.Conversion, node.Children[1].Expression);
        }
    }
}
