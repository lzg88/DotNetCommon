﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Logger
{
    /// <summary>
    /// 日志消息上下文
    /// </summary>
    public class LogContext
    {
        /// <summary>
        /// 异常信息
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// 日志类别
        /// </summary>
        public string CategoryName { get; set; }
        /// <summary>
        /// 日志级别
        /// </summary>
        public LoggerLevel Level { get; set; }
        /// <summary>
        /// 日志内容
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime Time { get; set; }
    }
}
