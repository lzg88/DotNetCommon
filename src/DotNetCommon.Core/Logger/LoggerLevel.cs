﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNetCommon.Logger
{
    /// <summary>
    /// 日志级别
    /// </summary>
    public enum LoggerLevel
    {
        /// <summary>
        /// 跟踪
        /// </summary>
        [Description("trce")]
        Trace = 0,
        /// <summary>
        /// 调试
        /// </summary>
        [Description("dbug")]
        Debug = 1,
        /// <summary>
        /// 信息
        /// </summary>
        [Description("info")]
        Information = 2,
        /// <summary>
        /// 警告
        /// </summary>
        [Description("warn")]
        Warning = 3,
        /// <summary>
        /// 错误
        /// </summary>
        [Description("fail")]
        Error = 4,
        /// <summary>
        /// 严重错误
        /// </summary>
        [Description("crit")]
        Critical = 5,
        /// <summary>
        /// 无等级
        /// </summary>
        [Description("none")]
        None = 6
    }
}
