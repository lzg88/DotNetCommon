﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Logger
{
    /// <summary>
    /// 日志记录器接口
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="categoryName"></param>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        void Log(string categoryName, LoggerLevel level, string message, Exception exception = null);

        void LogTrace(string message);
        void LogDebug(string message);
        void LogInformation(string message);
        void LogWarning(string message);
        void LogError(string message);
        void LogError(Exception exception);
        void LogError(Exception exception, string message);
        void LogCritical(string message);
        void LogCritical(Exception exception);
        void LogCritical(Exception exception, string message);
    }

    /// <summary>
    /// 日志记录器接口
    /// </summary>
    /// <typeparam name="T">以此泛型当做类别名称</typeparam>
    public interface ILogger<T> : ILogger { }
}
