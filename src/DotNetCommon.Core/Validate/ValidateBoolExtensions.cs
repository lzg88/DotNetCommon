﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Validate
{
    /// <summary>
    /// bool类型校验方法
    /// </summary>
    public static class ValidateBoolExtensions
    {
        /// <summary>
        /// 必须为true
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<bool> MustBeTrue(this ValidateContext<bool> ctx, string errorMessage = null)
        {
            if (!ctx.Model)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为true!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为false
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<bool> MustBeFalse(this ValidateContext<bool> ctx, string errorMessage = null)
        {
            if (ctx.Model)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为false!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为true
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<bool?> MustBeTrue(this ValidateContext<bool?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model == false)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为true!"));
            }
            return ctx;
        }

        /// <summary>
        /// 必须为false
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<bool?> MustBeFalse(this ValidateContext<bool?> ctx, string errorMessage = null)
        {
            if (ctx.Model == null || ctx.Model == true)
            {
                ctx.ErrorMessage.Add((ctx.ModelPath, errorMessage ?? $"'{ctx.ModelPath}' 必须为false!"));
            }
            return ctx;
        }
    }
}
