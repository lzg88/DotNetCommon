﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Validate
{
    /// <summary>
    /// 表示出现在错误提示中的属性名称
    /// </summary>
    public class ValidateDisplayNameAttribute : Attribute
    {
        /// <summary>
        /// 属性名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 标注特定的属性名
        /// </summary>
        /// <param name="name"></param>
        public ValidateDisplayNameAttribute(string name)
        {
            Name = name;
        }
    }
}
