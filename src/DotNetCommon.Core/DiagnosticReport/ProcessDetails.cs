﻿namespace DotNetCommon.DiagnosticReport
{
    using System;

    /// <summary>
    /// 当前进程信息
    /// </summary>
    public sealed class ProcessDetails
    {
        /// <summary>
        /// 当前进程ID
        /// </summary>
        public int PID { get; internal set; }

        /// <summary>
        /// 当前进程名称
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// 当前进程启动的时间
        /// </summary>
        public DateTimeOffset Started { get; internal set; }

        /// <summary>
        /// 当前进程运行的持续时间
        /// </summary>
        public TimeSpan LoadedIn { get; internal set; }

        /// <summary>
        /// 当前进程的入口程序集是否以Release模式编译
        /// </summary>
        public string IsOptimized { get; internal set; }

        /// <summary>
        /// 当前进程是否是64位
        /// </summary>
        public bool Is64Bit { get; internal set; }

        /// <summary>
        /// 是否启用了服务器垃圾回收
        /// </summary>
        public bool IsServerGC { get; internal set; }

        /// <summary>
        /// 当前进程是否运行在 <c>Large Address Aware</c> 模式
        /// </summary>
        public bool IsLargeAddressAware { get; internal set; }

        /// <summary>
        /// 线程数量
        /// </summary>
        public uint ThreadCount { get; internal set; }

        /// <summary>
        /// 线程池中最少的辅助线程数量
        /// </summary>
        public uint ThreadPoolMinWorkerCount { get; internal set; }

        /// <summary>
        /// 线程池中最大的辅助线程数量
        /// </summary>
        public uint ThreadPoolMaxWorkerCount { get; internal set; }

        /// <summary>
        /// 线程池中最少的I/O线程数量
        /// </summary>
        public uint ThreadPoolMinCompletionPortCount { get; internal set; }

        /// <summary>
        /// 线程池中最大的I/O线程数量
        /// </summary>
        public uint ThreadPoolMaxCompletionPortCount { get; internal set; }

        /// <summary>
        /// 进程的模块名称
        /// </summary>
        public string ModuleName { get; internal set; }

        /// <summary>
        /// 进程的模块文件路径
        /// </summary>
        public string ModuleFileName { get; internal set; }

        /// <summary>
        /// 程序上标记的产品名称
        /// </summary>
        public string ProductName { get; internal set; }

        /// <summary>
        /// 程序上原来的名称(创建程序文件时)
        /// </summary>
        public string OriginalFileName { get; internal set; }

        /// <summary>
        /// 程序运行的文件名称
        /// </summary>
        public string FileName { get; internal set; }

        /// <summary>
        /// 程序运行的文件版本号
        /// </summary>
        public string FileVersion { get; internal set; }

        /// <summary>
        /// 与程序一起发行的产品的版本
        /// </summary>
        public string ProductVersion { get; internal set; }

        /// <summary>
        /// 进程运行的语言环境
        /// </summary>
        public string Language { get; internal set; }

        /// <summary>
        /// 程序的版权声明
        /// </summary>
        public string Copyright { get; internal set; }

        /// <summary>
        /// 进程运行占用的空间 单位: MB (包含用户空间和内核空间)
        /// </summary>
        public double WorkingSetInMegaBytes { get; internal set; }

        /// <summary>
        /// 进程是否在用户交互模式下运行
        /// </summary>
        public bool IsInteractive { get; internal set; }

        /// <summary>
        /// 进程运行的命令行参数
        /// </summary>
        public string[] CommandLine { get; internal set; }
    }
}