﻿namespace DotNetCommon.DiagnosticReport
{
    /// <summary>
    /// 当前电脑的磁盘分区信息
    /// </summary>
    public sealed class DriveDetails
    {
        /// <summary>
        /// 分区名称,比如: <c>C:\</c>
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// 分区类型,比如: <c>Fixed, CDRom</c>
        /// </summary>
        public string Type { get; internal set; }

        /// <summary>
        /// 分区格式,比如: <c>NTFS</c>.
        /// </summary>
        public string Format { get; internal set; }

        /// <summary>
        /// 分区标签,比如: <c>新加卷</c>
        /// </summary>
        public string Label { get; internal set; }

        /// <summary>
        /// 分区总容量，单位: GB
        /// </summary>
        public double TotalCapacityInGigaBytes { get; internal set; }

        /// <summary>
        /// 这个分区所有可用的容量，单位: GB
        /// </summary>
        public double FreeCapacityInGigaBytes { get; internal set; }

        /// <summary>
        /// 这个分区在当前用户下可用的容量，单位: GB
        /// </summary>
        public double AvailableCapacityInGigaBytes { get; internal set; }
    }
}