﻿namespace DotNetCommon.DiagnosticReport
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    /// 当前操作系统信息
    /// </summary>
    public sealed class SystemDetails
    {
        /// <summary>
        /// 操作系统名称
        /// </summary>
        public string OSName { get; internal set; }

        /// <summary>
        /// 操作系统类型 <c>Windows, Linux 或 OSX</c>.
        /// </summary>
        public string OSType { get; internal set; }

        /// <summary>
        /// 是否是64位操作系统
        /// </summary>
        public bool Is64BitOS { get; internal set; }

        /// <summary>
        /// 当前运行的 <c>.NET </c> 版本
        /// </summary>
        public string DotNetFrameworkVersion { get; internal set; }

        /// <summary>
        /// 计算机名称
        /// </summary>
        public string MachineName { get; internal set; }

        /// <summary>
        /// 计算机全限定名(含域名)
        /// </summary>
        public string FQDN { get; internal set; }

        /// <summary>
        /// 当前程序以哪个用户运行
        /// </summary>
        public string User { get; internal set; }

        /// <summary>
        /// CPU名称
        /// </summary>
        public string CPU { get; internal set; }

        /// <summary>
        /// CPU的核心数量
        /// </summary>
        public uint CPUCoreCount { get; internal set; }

        /// <summary>
        /// 安装的内存大小 单位: GB
        /// </summary>
        public long InstalledRAMInGigaBytes { get; internal set; }

        /// <summary>
        /// 操作系统目录 如: C:\Windows
        /// </summary>
        public string SystemDirectory { get; internal set; }

        /// <summary>
        /// 当前进程运行目录 如: E:\gitee\DotNetCommon\bin\Debug\netcoreapp3.0
        /// </summary>
        public string CurrentDirectory { get; internal set; }

        /// <summary>
        /// <c>CLR</c> 环境所在目录 如: C:\Program Files\dotnet\shared\Microsoft.NETCore.App\3.1.8\
        /// </summary>
        public string RuntimeDirectory { get; internal set; }

        /// <summary>
        /// 电脑开机时间
        /// </summary>
        public TimeSpan Uptime { get; internal set; }
    }
}