﻿namespace DotNetCommon.DiagnosticReport
{
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;

    /// <summary>
    /// 表示所有网卡的信息
    /// </summary>
    public sealed class NetworkDetails
    {
        /// <summary>
        /// 获取 <c>DHCP</c> 范围名
        /// </summary>
        public string DHCPScope { get; internal set; }

        /// <summary>
        /// 获取计算机域名
        /// </summary>
        public string Domain { get; internal set; }

        /// <summary>
        /// 获取计算机名
        /// </summary>
        public string Host { get; internal set; }

        /// <summary>
        /// 判断当前计算机是否是 <c>Windows Internet Name Service</c> (WINS) 代理
        /// </summary>
        public bool IsWINSProxy { get; internal set; }

        /// <summary>
        /// 获取当前系统NetBIOS类型 (<c>Network Basic Input/Output System</c>)
        /// </summary>
        public string NodeType { get; internal set; }

        /// <summary>
        /// 当前系统所有的网卡信息
        /// </summary>
        public NetworkInterfaceDetails[] InterfaceDetails { get; internal set; }
    }

    /// <summary>
    /// 表示单个网卡信息
    /// </summary>
    public sealed class NetworkInterfaceDetails
    {
        /// <summary>
        /// 网卡Mac地址 (<c>Media Access Control</c>)
        /// </summary>
        public string MAC { get; internal set; }

        /// <summary>
        /// 网卡信息
        /// </summary>
        public NetworkInterface Interface { get; internal set; }

        /// <summary>
        /// 网卡上绑定的所有IP地址信息
        /// </summary>
        public IPAddressDetails[] Addresses { get; internal set; }
    }

    /// <summary>
    /// IP地址配置信息
    /// </summary>
    public sealed class IPAddressDetails
    {
        /// <summary>
        /// Returns a <see cref="IPAddressDetails"/> from the given <paramref name="ipAddress"/>.
        /// </summary>
        internal static IPAddressDetails From(IPAddress ipAddress) => new IPAddressDetails
        {
            AddressFamily = ipAddress.AddressFamily,
            IsIPv6Multicast = ipAddress.IsIPv6Multicast,
            IsIPv6LinkLocal = ipAddress.IsIPv6LinkLocal,
            IsIPv6SiteLocal = ipAddress.IsIPv6SiteLocal,
            IsIPv6Teredo = ipAddress.IsIPv6Teredo,
            IsIPv4MappedToIPv6 = ipAddress.IsIPv4MappedToIPv6,
            AsString = ipAddress.ToString()
        };

        /// <summary>
        /// 当前IP配置的 <seealso cref="AddressFamily"/>
        /// </summary>
        public AddressFamily AddressFamily { get; private set; }

        /// <summary>
        /// 是否是IPv6多播地址
        /// </summary>
        public bool IsIPv6Multicast { get; private set; }

        /// <summary>
        /// 是否是IPv6回环地址
        /// </summary>
        public bool IsIPv6LinkLocal { get; private set; }

        /// <summary>
        /// 是否是IPv6本机地址
        /// </summary>
        public bool IsIPv6SiteLocal { get; private set; }

        /// <summary>
        /// 是否是IPv6 Teredo地址 <seealso href="https://baike.baidu.com/item/teredo/1908883"/>
        /// </summary>
        public bool IsIPv6Teredo { get; private set; }

        /// <summary>
        /// 是否为 IPv4 映射的 IPv6 地址
        /// </summary>
        public bool IsIPv4MappedToIPv6 { get; private set; }

        /// <summary>
        /// 字符串表示IP地址
        /// </summary>
        public string AsString { get; private set; }

        /// <summary>
        /// 用IP地址的字符串表示当前对象
        /// </summary>
        public override string ToString() => AsString;
    }
}