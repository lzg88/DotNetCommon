//using System;
//using System.Collections.Generic;
//using System.Text;
//
//namespace DotNetCommon.Cache
//{
//    // TODO: Granularity?
//    /// <summary>
//    /// Specifies how items are prioritized for preservation during a memory pressure triggered cleanup.
//    /// </summary>
//    internal enum CacheItemPriority
//    {
//        Low,
//        Normal,
//        High,
//        NeverRemove,
//    }
//}