﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon
{
    /// <summary>
    /// 生成假数据
    /// </summary>
    public static class FakeDataHelper
    {
        public class Peson
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
            public int Sex { get; set; }
            public string IdentityNo { get; set; }
            public string Addr { get; set; }
            public DateTime Birth { get; set; }
            public string Nick { get; set; }
            public string Ico { get; set; }
            public string PhoneNo { get; set; }
        }
    }
}
