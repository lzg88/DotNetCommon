﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 结果异常(主要和Result模型配合使用)
    /// </summary>
    public class ResultException : Exception
    {
        /// <summary>
        /// 构造异常对象
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public ResultException(string message, int code = 0) : base(message)
        {
            this.Code = code;
        }

        /// <summary>
        /// 异常代码,对应Result模型中的Code属性
        /// </summary>
        public int Code { set; get; }

        /// <summary>
        /// 抛 <c>ResultException</c> 异常,抛出的异常可以使用全局异常过滤器进行拦截
        /// </summary>
        /// <param name="message"></param>
        public static void Throw(string message) => throw new ResultException(message);

        /// <summary>
        /// 抛 <c>ResultException</c> 异常,抛出的异常可以使用全局异常过滤器进行拦截
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public static void Throw(string message, int code) => throw new ResultException(message) { Code = code };
    }
}
