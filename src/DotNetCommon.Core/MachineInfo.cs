﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Text;

//namespace DotNetCommon
//{
//    /// <summary>
//    /// 机器信息
//    /// </summary>
//    /// <remarks>摘自: https://github.com/NewLifeX/X/blob/master/NewLife.Core/Common/MachineInfo.cs</remarks>
//    public class MachineInfo
//    {
//        #region 属性
//        /// <summary>系统名称</summary>
//        public string OSName { get; set; }

//        /// <summary>系统版本</summary>
//        public string OSVersion { get; set; }

//        /// <summary>产品名称。制造商</summary>
//        public string Product { get; set; }

//        /// <summary>处理器型号</summary>
//        public string Processor { get; set; }

//        /// <summary>处理器序列号</summary>
//        public string CpuID { get; set; }

//        /// <summary>硬件唯一标识</summary>
//        public string UUID { get; set; }

//        /// <summary>系统标识</summary>
//        public string Guid { get; set; }

//        /// <summary>磁盘序列号</summary>
//        public string DiskID { get; set; }

//        /// <summary>内存总量</summary>
//        public ulong Memory { get; set; }

//        /// <summary>可用内存</summary>
//        public ulong AvailableMemory { get; private set; }

//        /// <summary>CPU占用率</summary>
//        public float CpuRate { get; private set; }

//        /// <summary>温度</summary>
//        public double Temperature { get; set; }
//        #endregion

//        /// <summary>
//        /// 当前机器信息。默认null，在RegisterAsync后才能使用
//        /// </summary>
//        public static MachineInfo Current { get; set; }

//        /// <summary>
//        /// 刷新机器信息
//        /// </summary>
//        public void Init()
//        {
//            var osv = Environment.OSVersion;
//            if (OSVersion.IsNullOrEmpty()) OSVersion = osv.Version + "";
//            if (OSName.IsNullOrEmpty()) OSName = (osv + "").TrimStart("Microsoft".ToCharArray()).TrimEnd(OSVersion.ToCharArray()).Trim();
//            if (Guid.IsNullOrEmpty()) Guid = "";

//            try
//            {
//#if NET47
//                if (Runtime.Windows)
//                    LoadWindowsInfoFx();
//                else if (Runtime.Linux)
//                    LoadLinuxInfo();
//#else
//                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
//                    LoadWindowsInfo();
//                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
//                    LoadLinuxInfo();
//#endif
//            }
//            catch (Exception ex)
//            {

//            }

//            // window+netcore 不方便读取注册表，随机生成一个guid，借助文件缓存确保其不变
//            if (Guid.IsNullOrEmpty()) Guid = "0-" + System.Guid.NewGuid().ToString();
//            if (UUID.IsNullOrEmpty()) UUID = "0-" + System.Guid.NewGuid().ToString();

//            try
//            {
//                Refresh();
//            }
//            catch { }
//        }

//        private void LoadWindowsInfo()
//        {
//            var str = "";

//            var os = ReadWmic("os", "Caption", "Version");
//            if (os != null)
//            {
//                if (os.TryGetValue("Caption", out str)) OSName = str.TrimStart("Microsoft").Trim();
//                if (os.TryGetValue("Version", out str)) OSVersion = str;
//            }

//            var csproduct = ReadWmic("csproduct", "Name", "UUID");
//            if (csproduct != null)
//            {
//                if (csproduct.TryGetValue("Name", out str)) Product = str;
//                if (csproduct.TryGetValue("UUID", out str)) UUID = str;
//            }

//            var disk = ReadWmic("diskdrive", "serialnumber");
//            if (disk != null)
//            {
//                if (disk.TryGetValue("serialnumber", out str)) DiskID = str?.Trim();
//            }

//            // 不要在刷新里面取CPU负载，因为运行wmic会导致CPU负载很不准确，影响测量
//            var cpu = ReadWmic("cpu", "Name", "ProcessorId", "LoadPercentage");
//            if (cpu != null)
//            {
//                if (cpu.TryGetValue("Name", out str)) Processor = str;
//                if (cpu.TryGetValue("ProcessorId", out str)) CpuID = str;
//                if (cpu.TryGetValue("LoadPercentage", out str)) CpuRate = (Single)(str.ToDouble() / 100);
//            }

//            // 从注册表读取 MachineGuid
//            str = Execute("reg", @"query HKLM\SOFTWARE\Microsoft\Cryptography /v MachineGuid");
//            if (!str.IsNullOrEmpty() && str.Contains("REG_SZ")) Guid = str.Substring("REG_SZ", null).Trim();
//        }

//        /// <summary>通过WMIC命令读取信息</summary>
//        /// <param name="type"></param>
//        /// <param name="keys"></param>
//        /// <returns></returns>
//        public static IDictionary<String, String> ReadWmic(String type, params String[] keys)
//        {
//            var dic = new Dictionary<String, String>(StringComparer.OrdinalIgnoreCase);

//            var args = $"{type} get {keys.Join(",")} /format:list";
//            var str = Execute("wmic", args)?.Trim();
//            if (str.IsNullOrEmpty()) return dic;

//            //return str.SplitAsDictionary("=", Environment.NewLine);

//            var ss = str.Split(Environment.NewLine);
//            foreach (var item in ss)
//            {
//                var ks = item.Split("=");
//                if (ks != null && ks.Length >= 2)
//                {
//                    var k = ks[0].Trim();
//                    var v = ks[1].Trim();
//                    if (dic.TryGetValue(k, out var val))
//                        dic[k] = val + "," + v;
//                    else
//                        dic[k] = v;
//                }
//            }

//            return dic;
//        }

//        private void LoadLinuxInfo()
//        {
//            var str = GetLinuxName();
//            if (!str.IsNullOrEmpty()) OSName = str;

//            // 树莓派的Hardware无法区分P0/P4
//            var dic = ReadInfo("/proc/cpuinfo");
//            if (dic != null)
//            {
//                if (dic.TryGetValue("Hardware", out str) ||
//                    dic.TryGetValue("cpu model", out str) ||
//                    dic.TryGetValue("model name", out str))
//                    Processor = str;

//                if (dic.TryGetValue("Model", out str)) Product = str;
//                if (dic.TryGetValue("Serial", out str)) CpuID = str;
//            }

//            var mid = "/etc/machine-id";
//            if (!File.Exists(mid)) mid = "/var/lib/dbus/machine-id";
//            if (TryRead(mid, out var value)) Guid = value;

//            var file = "/sys/class/dmi/id/product_uuid";
//            if (TryRead(file, out value)) UUID = value;
//            file = "/sys/class/dmi/id/product_name";
//            if (TryRead(file, out value)) Product = value;

//            var disks = GetFiles("/dev/disk/by-id", true);
//            if (disks.Count == 0) disks = GetFiles("/dev/disk/by-uuid", false);
//            if (disks.Count > 0) DiskID = disks.Join(",");

//            var dmi = Execute("dmidecode")?.SplitAsDictionary(":", "\n");
//            if (dmi != null)
//            {
//                if (dmi.TryGetValue("ID", out str)) CpuID = str.Replace(" ", null);
//                if (dmi.TryGetValue("UUID", out str)) UUID = str;
//                if (dmi.TryGetValue("Product Name", out str)) Product = str;
//                //if (TryFind(dmi, new[] { "Serial Number" }, out str)) Guid = str;
//            }

//            // 从release文件读取产品
//            var prd = GetProductByRelease();
//            if (!prd.IsNullOrEmpty()) Product = prd;
//        }
//    }
//}
