﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Extensions
{
    /// <summary>
    /// 扩展 byte[]
    /// </summary>
    public static class ByteArrayExtensions
    {
        /// <summary>
        /// 将字节数组转16进制字符串
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static string ToHex(this byte[] arr)
        {
            return BytesToHexConverter.ToHex(arr);
        }
    }
}
