﻿namespace DotNetCommon.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Dynamic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Runtime.Serialization;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// 通用扩展方法
    /// </summary>
    public static class GenericExtensions
    {
        /// <summary>
        /// 将当前对象转成字典(属性名->属性值)
        /// </summary>
        /// <param name="object">原对象</param>
        /// <param name="includePrivate">是否包含私有属性，即: private string name { set; get; }</param>
        /// <param name="inherit">是否包含继承的属性</param>
        public static Dictionary<string, object> ToDictionary<T>(this T @object, bool inherit = true, bool includePrivate = false)
        {
            var dic = new Dictionary<string, object>();
            if (@object == null) return dic;
            foreach (var property in @object.GetType().GetInstanceProperties(inherit, includePrivate))
            {
                dic.Add(property.Name, property.GetValue(@object, null));
            }
            return dic;
        }

        /// <summary>
        /// 判断当前对象是否所属<seealso cref="Type"/>的默认值(<c>null</c>被认为是默认值)
        /// </summary>
        public static bool IsDefault<T>(this T @object) => EqualityComparer<T>.Default.Equals(@object, default);

        /// <summary>
        /// 返回一个未初始化的实例,注意: 未执行任何的初始化逻辑,比如 构造函数/字段初始化等，由于返回的对象不在一个正常的状态，请慎用。
        /// </summary>
        /// <remarks>参照：<see href="https://msdn.microsoft.com/en-us/library/system.runtime.serialization.formatterservices.getuninitializedobject.aspx"/></remarks>
        /// <example>
        /// <code>
        /// class Person { public Person(int Id) { this.Id = Id; } public int Id { get; set; } = 5; public string Name { get; set; } = "小明"; }
        /// var person = ExpressionExtensions.GetUninitializedInstance&lt;Person&gt;();
        /// //输出: "0,null"
        /// Console.WriteLine($"{person.Id},{person.Name ?? "null"}");
        /// </code>
        /// </example>
        public static T GetUninitializedInstance<T>() => (T)FormatterServices.GetUninitializedObject(typeof(T));

        /// <summary>
        /// 获取符合条件的属性名集合
        /// </summary>
        /// <param name="object"></param>
        /// <param name="includePrivate">是否包含私有属性</param>
        /// <param name="inherit">是否包含继承属性</param>
        public static string[] GetPropertyNames<T>(this T @object, bool inherit = true, bool includePrivate = true)
        {
            if (@object is IDynamicMetaObjectProvider expando)
            {
                var dic = (IDictionary<string, object>)expando;
                return dic.Keys.ToArray();
            }

            return @object.GetType()
                .GetInstanceProperties(inherit, includePrivate)
                .Select(p => p.Name).ToArray();
        }

        /// <summary>
        /// 提供一个具有超时功能的锁
        /// </summary>
        public static Locker Lock<T>(this T obj, TimeSpan timeout) where T : class
        {
            var lockTaken = false;

            try
            {
                Monitor.TryEnter(obj, timeout, ref lockTaken);
                if (lockTaken) { return new Locker(obj); }
                throw new TimeoutException("Failed to acquire a lock within the timeout period of: " + timeout.ToString());
            }
            catch
            {
                if (lockTaken) { Monitor.Exit(obj); }
                throw;
            }
        }

        /// <summary>
        /// 具有超时功能的锁
        /// </summary>
        public struct Locker : IDisposable
        {
            private readonly object _obj;

            /// <summary>
            /// Returns an instance of <see cref="Locker"/>.
            /// </summary>
            /// <param name="obj">The <c>object</c> on which lock is taken.</param>
            internal Locker(object obj) => _obj = obj;

            /// <summary>
            /// Releases any locks taken by this instance.
            /// </summary>
            public void Dispose() => Monitor.Exit(_obj);
        }

        /// <summary>
        /// 将当前对象封住为一个完成态的Task <c>Task.FromResult(this)</c>
        /// </summary>
        [DebuggerStepThrough]
        public static Task<T> ToCompletedTask<T>(this T result) => Task.FromResult(result);

        /// <summary>
        /// Creates a ValueTask that's completed successfully with the specified <paramref name="result"/>.
        /// </summary>
        [DebuggerStepThrough]
        public static ValueTask<T> ToCompletedValueTask<T>(this T result) => new ValueTask<T>(result);
    }
}