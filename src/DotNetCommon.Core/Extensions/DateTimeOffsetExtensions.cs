﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Extensions
{
    /// <summary>
    /// <seealso cref="DateTimeOffset"/>的扩展类
    /// </summary>
    public static class DateTimeOffsetExtensions
    {
        #region 输出时区信息，使用数据指定的时区
        /// <summary>
        /// 返回"yyyy-MM-dd HH:mm zzz"格式的字符串, 如: "2021-02-02 13:45 +08:00"
        /// </summary>
        /// <returns></returns>
        public static string ToGlobalMinuteString(this DateTimeOffset value) => value.ToString("yyyy-MM-dd HH:mm zzz");

        /// <summary>
        /// 返回"yyyy-MM-dd HH:mm:ss.fff zzz"格式的字符串, 如: "2021-02-02 13:45:02.123 +08:00"
        /// </summary>
        /// <returns></returns>
        public static string ToGlobalStampString(this DateTimeOffset value) => value.ToString("yyyy-MM-dd HH:mm:ss.fff zzz");

        /// <summary>
        /// 返回"yyyy-MM-dd HH:mm:ss zzz"格式的字符串，如: "2021-02-02 13:45:02 +08:00"
        /// </summary>
        /// <returns></returns>
        public static string ToGlobalString(this DateTimeOffset value) => value.ToString("yyyy-MM-dd HH:mm:ss zzz");
        #endregion
    }
}
