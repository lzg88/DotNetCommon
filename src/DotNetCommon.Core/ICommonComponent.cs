﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon
{

    /// <summary>
    /// 公共组件,方便将非泛型类注入到容器，使用示例：<br/>
    /// <code>
    /// // Startup.cs<br/>
    /// services.AddScoped(provider => ComponentWraper.Create(new TestService()));
    /// <br />
    /// <br />
    /// //TestController.cs
    /// private readonly TestService testService;
    /// public TestController(ComponentWraper&lt;TestService> testService)
    /// {
    ///     this.testService = testService.Value;
    /// }
    /// </code>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ComponentWraper<T>
    {
        internal ComponentWraper() { }

        /// <summary>
        /// 组件
        /// </summary>
        public T Value { get; set; }
    }

    /// <summary>
    /// 公共组件,方便将非泛型类注入到容器
    /// </summary>
    public static class ComponentWraper
    {
        /// <summary>
        /// 创建实例
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static ComponentWraper<T> Create<T>(T value) => new ComponentWraper<T> { Value = value };
    }
}
