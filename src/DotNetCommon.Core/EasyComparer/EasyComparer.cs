namespace DotNetCommon.EasyComparer
{
    using DotNetCommon;
    using DotNetCommon.Extensions;
    using DotNetCommon.Accessors;
    using DotNetCommon.EasyComparer;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// 用来比较两个同类型对象属性之间差异的工具类
    /// </summary>
    /// <example>
    /// <para>示例代码:</para>
    /// <code>
    /// public class Person
    /// {
    ///     public int Id { get; set; }
    ///     public string Name { get; set; }
    /// }
    /// public class Program
    /// {
    ///     public static void Main(string[] args)
    ///     {
    ///         var person = new Person()
    ///         {
    ///             Id = 1,
    ///             Name = "小明"
    ///         };
    ///         var person2 = new Person()
    ///         {
    ///             Id = 1,
    ///             Name = "小红"
    ///         };
    ///         EasyComparer.Instance.Compare&lt;Person&gt;(person, person2, true, true, out var results);
    ///         foreach (var item in results)
    ///         {
    ///             Console.WriteLine($"属性:{item.Key.Name},是否有变化:{item.Value.Varies}");
    ///         }
    ///         /*输出比较结果如下：
    ///         属性:Id,是否有变化:False
    ///         属性:Name,是否有变化:True
    ///         */
    ///     }
    /// }
    /// </code>
    /// </example>
    public sealed class EasyComparer
    {
        private readonly ConcurrentDictionary<CacheKey, KeyValuePair<PropertyInfo, object>[]> _cache;

        private EasyComparer() => _cache = new ConcurrentDictionary<CacheKey, KeyValuePair<PropertyInfo, object>[]>();

        /// <summary>
        /// <see cref="EasyComparer"/>的默认实例
        /// </summary>
        public static EasyComparer Instance { get; } = new EasyComparer();

        /// <summary>
        /// 比较两个对象属性之间的差异
        /// </summary>
        /// <param name="includePrivate">是否比较私有属性</param>
        /// <param name="inherit">是否比较继承的属性</param>
        /// <param name="left">等待比较的对象1</param>
        /// <param name="right">等待比较的对象2</param>
        /// <param name="variances">差异结果</param>
        /// <returns>是否发生了相等</returns>
        public bool Compare<T>(T left, T right, bool inherit, bool includePrivate, out IDictionary<PropertyInfo, Variance> variances)
        {
            var type = typeof(T);
            var key = new CacheKey(type, inherit, includePrivate);

            var cache = _cache.GetOrAdd(key,
                () => type.GetInstanceProperties(inherit, includePrivate)
                    .Select(p => new KeyValuePair<PropertyInfo, object>(p, AccessorBuilder.BuildGetter<T>(p, includePrivate)))
                    .ToArray());

            var bothMatch = true;
            var result = new Dictionary<PropertyInfo, Variance>();

            // ReSharper disable once ForCanBeConvertedToForeach
            for (var i = 0; i < cache.Length; i++)
            {
                var pair = cache[i];
                var p = pair.Key;
                var getter = (Func<T, object>)pair.Value;

                var leftVal = getter(left);
                var rightVal = getter(right);

                var variance = new Variance(p, leftVal, rightVal);

                result.Add(variance.Property, variance);

                if (!bothMatch) { continue; }
                if (variance.Varies) { bothMatch = false; }
            }

            variances = result;
            return bothMatch;
        }

        private sealed class CacheKey : Equatable<CacheKey>
        {
            public CacheKey(Type type, bool inherit, bool includePrivate)
            {
                Type = type;
                Inherit = inherit;
                IncludePrivate = includePrivate;
            }

            private Type Type { get; }
            private bool Inherit { get; }
            private bool IncludePrivate { get; }

            public override int GetHashCode() => HashHelper.GetHashCode(Type, Inherit, IncludePrivate);
        }
    }
}