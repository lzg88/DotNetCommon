﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Encrypt
{
    /// <summary>
    /// Sha256加密算法，类似MD5
    /// </summary>
    public static class Sha256
    {
        private static EasyPool<SHA256> pool = new EasyPool<SHA256>(() => SHA256.Create(), s => s.Initialize(), 200);

        /// <summary>
        /// MD5加密字符串（32位大写）
        /// </summary>
        /// <param name="source">源字符串</param>
        /// <returns>加密后的字符串</returns>
        public static string SHA256Hash(string source)
        {
            if (source.IsNullOrEmpty()) return source;
            byte[] data = Encoding.UTF8.GetBytes(source);
            var sha = pool.Get();
            try
            {
                var hashBytes = sha.ComputeHash(data);
                var s = Base64UrlSafe.Encode(hashBytes);
                Array.Clear(data);
                Array.Clear(hashBytes);
                return s;
            }
            finally
            {
                pool.Return(sha);
            }
        }

        /// <summary>
        /// Sha256加密流（32位大写）,默认将流的指针回退到开始位置
        /// </summary>
        /// <param name="stream">要操作的流</param>
        /// <param name="isSeekBegin">是否将流指针回退到开始位置</param>
        /// <returns>加密后的字符串</returns>
        public static string SHA256Hash(Stream stream, bool isSeekBegin = true)
        {
            if (stream == null) throw new Exception("流不能为null!");
            if (isSeekBegin) stream.Position = 0;
            var sha = pool.Get();
            try
            {
                var hashBytes = sha.ComputeHash(stream);
                var s = Base64UrlSafe.Encode(hashBytes);
                Array.Clear(hashBytes);
                return s;
            }
            finally
            {
                pool.Return(sha);
            }
        }

        /// <summary>
        /// Sha256加密文件
        /// </summary>
        /// <param name="absPath">要操作的文件绝对路径</param>
        /// <returns>加密后的字符串</returns>
        public static string SHA256HashFile(string absPath)
        {
            if (absPath.IsNullOrEmptyOrWhiteSpace()) return absPath;
            using var stream = new FileStream(absPath, FileMode.Open);
            return SHA256Hash(stream);
        }
    }
}
