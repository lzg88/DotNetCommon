﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCommon
{
    /// <summary>
    /// 验证码帮助类, <seealso href="https://blog.csdn.net/mjkmjk485485/article/details/125602744"/>
    /// </summary>
    public static class VerifyCodeHelper
    {
        /// <summary>
        /// 获取验证码图片的base64字符串
        /// </summary>
        /// <param name="code">生成的验证码字符串</param>
        /// <param name="length">默认长度4位</param>
        /// <param name="isQuestion">是否是问题</param>
        /// <returns></returns>
        public static string GetBase64String(out string code, int length = 4, bool isQuestion = false)
        {
            var inst = new VerifyCode(length, isQuestion);
            var bs = inst.GetVerifyCodeImage();
            code = inst.SetVerifyCodeText;
            return Convert.ToBase64String(bs);
        }

        /// <summary>
        /// 根据指定长度,随机生成验证码
        /// </summary>
        /// <param name="length">默认长度4位</param>
        /// <param name="isQuestion">是否是问题</param>
        /// <returns></returns>
        public static (string code, string imgBase64) New(int length = 4, bool isQuestion = false)
        {
            var inst = new VerifyCode(length, isQuestion);
            var bs = inst.GetVerifyCodeImage();
            var code = isQuestion ? inst.VerifyCodeResult : inst.SetVerifyCodeText;
            return (code, Convert.ToBase64String(bs));
        }
    }
}
