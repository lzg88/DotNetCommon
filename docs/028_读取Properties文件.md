# 读取Properties文件

## 什么是Properties文件？

熟悉java的应该对 *.properties文件不陌生。不错，这里就是读取这个文件，先看文件示例：

```text
// 测试注释
# 又是测试注释
name=小明
age=18
id=1

//其他
person.addr=天明路

//值带空格
person.secret=\u0020空格\u0020

//无值
person.none=
person.addr=天明路2
```

## 读取示例：
```csharp
var filePath = DateTime.Now.ToFileNameGuidString("LoadNormalProperties.properties");
if (File.Exists(filePath)) File.Delete(filePath);
File.WriteAllText(filePath, @"
// 测试注释
# 又是测试注释
name=小明
age=18
id=1

//其他
person.addr=天明路

//值带空格
person.secret=\u0020空格\u0020

//无值
person.none=
person.addr=天明路2
", Encoding.UTF8);
var properties = PropertiesHelper.LoadProperties(filePath);
File.Delete(filePath);
properties.Count.ShouldBe(6);

properties["name"].ShouldBe("小明");
properties["age"].ShouldBe("18");
properties["id"].ShouldBe("1");
properties["person.addr"].ShouldBe("天明路2");
properties["person.secret"].ShouldBe(" 空格 ");
properties["person.none"].ShouldBe("");
```