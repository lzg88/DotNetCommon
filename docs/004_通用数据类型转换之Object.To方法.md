# 通用数据类型转换之Object.To方法

> 所属包: DotNetCommon.Core



## 1. 简介

这个```To<T>()```方法主要是简化不同数据类型之间的转换调用的。和```Mapper<T>()```一样，它也是Object上的一个扩展方法。

> To<T>()转换方法针对的是数据类型，而Mapper<T>()扩展方法针对的是类。

它能完成的功能如下：

- 字符串转数字；
- 数字之间转换；
- 字符串转枚举；
- 字符串转日期；
- 枚举转数字；
- 数字转枚举；
- 枚举转字符串；
- 字符串转bool；



## 2. 字符串转数字

直接看示例：

```csharp
var str = "1.2";
//字符串转数字
str.To<double>().ShouldBe(1.2);

str = null;
str.To<double?>().ShouldBe(null);

str = "12";
str.To<int>().ShouldBe(12);
```

## 3. 数字之间转换

```csharp
//内部调用 Convert.Toxxxx(value); 
//decimal转double
1.88m.To<double>().ShouldBe(1.88d);

//double转decimal
1.88d.To<decimal>().ShouldBe(1.88m);

//int转double
20.To<double>().ShouldBe(20.0d);

//double转int
1.88d.To<int>().ShouldBe(2);
```

## 4. 字符串转枚举

```csharp
//字符串转枚举
str = "Close";
str.To<EnumState>().ShouldBe(EnumState.Close);
str = "Close,Active";
var state = str.To<EnumState>();
state.Contains(EnumState.Close).ShouldBeTrue();
state.Contains(EnumState.Active).ShouldBeTrue();
state.Contains(EnumState.Open).ShouldBeFalse();

// 枚举定义
[Flags]
public enum EnumState { Open = 1, Close = 2, Active = 4 }
```



## 5. 字符串转日期

```csharp
//简单字符串转日期
str = "2020-01-01";
str.To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01"));
str = null;
str.To<DateTime?>().ShouldBe(null);

//字符串转DateTimeOffSet
//字符串->DateTime
"2020-01-01".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01"));
"2020-01-01 02:03:04.123".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01 02:03:04.123"));
"2020-01-02T01:08:07.123Z".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-02T01:08:07.123Z"));
"2020-01-02 09:08:07 +08:00".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-02 09:08:07 +08:00"));


string str = null;
str.To<DateTime?>().ShouldBe(null);

//字符串->DateTimeOffset
"2020-01-01".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-01"));
"2020-01-01 02:03:04.123".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-01 02:03:04.123"));
"2020-01-02T01:08:07.123Z".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-02T01:08:07.123Z"));
"2020-01-02 09:08:07 +08:00".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-02 09:08:07 +08:00"));

//字符串->DateTime 指定格式
"2021-05-09 23:16:03.503".To<DateTime>("yyyy-MM-dd HH:mm:ss.fff").ShouldBe(DateTime.Parse("2021-05-09 23:16:03.503"));
"2021年的05月09日啊".To<DateTime>("yyyy年的MM月dd日啊").ShouldBe(DateTime.Parse("2021-05-09"));
"2020-01-02 09:08:07.123 +08:00".To<DateTime>("yyyy-MM-dd HH:mm:ss.fff zzz").ShouldBe(DateTime.Parse("2020-01-02 09:08:07.123 +08:00"));
"2020-01-02T01:08:07Z".To<DateTime>("yyyy-MM-ddThh:mm:ssZ").ShouldBe(DateTime.Parse("2020-01-02T01:08:07Z"));

//字符串->DateTimeOffset 指定格式
"2021-05-09 23:16:03.503".To<DateTimeOffset>("yyyy-MM-dd HH:mm:ss.fff").ShouldBe(DateTimeOffset.Parse("2021-05-09 23:16:03.503"));
"2021年的05月09日啊".To<DateTimeOffset>("yyyy年的MM月dd日啊").ShouldBe(DateTimeOffset.Parse("2021-05-09"));
"2020-01-02 09:08:07.123 +08:00".To<DateTimeOffset>("yyyy-MM-dd HH:mm:ss.fff zzz").ShouldBe(DateTimeOffset.Parse("2020-01-02 09:08:07.123 +08:00"));
"2020-01-02T01:08:07Z".To<DateTimeOffset>("yyyy-MM-ddThh:mm:ssZ").ShouldBe(DateTimeOffset.Parse("2020-01-02T01:08:07Z"));
```



## 6. 枚举转数字

```csharp
EnumState.Open.To<int>().ShouldBe(1);
EnumState.Close.To<int>().ShouldBe(2);
(EnumState.Close | EnumState.Open).To<int>().ShouldBe(3);
```



## 7. 数字转枚举

```csharp
1.To<EnumState>().ShouldBe(EnumState.Open);
2.To<EnumState>().ShouldBe(EnumState.Close);
3.To<EnumState>().ShouldBe(EnumState.Open | EnumState.Close);
4.To<EnumState>().ShouldBe(EnumState.Active);
7.To<EnumState>().ShouldBe(EnumState.Open | EnumState.Close | EnumState.Active);

//枚举定义
[Flags]
public enum EnumState { Open = 1, Close = 2, Active = 4 }
```



## 8. 枚举转字符串

```csharp
//枚举转字符串
EnumState.Active.To<string>().ShouldBe("Active");
//注意，多个逗号后面还有一个空格，和 (EnumState.Active | EnumState.Close).ToString() 一致
(EnumState.Active | EnumState.Close).To<string>().ShouldBe("Close, Active");
```



## 9. 字符串转bool

```csharp
 //字符串转bool
//"true"、"ok"、"yes"、"1"、"是" 转为true，其他转为false，不区分大小写
var str = "true";
str.To<bool>().ShouldBeTrue();
str = "True";
str.To<bool>().ShouldBeTrue();
str = "false";
str.To<bool>().ShouldBeFalse();
str = "False";
str.To<bool>().ShouldBeFalse();

str = "Ok";
str.To<bool>().ShouldBeTrue();
str = "notOk";
str.To<bool>().ShouldBeFalse();

str = "yes";
str.To<bool>().ShouldBeTrue();
str = "no";
str.To<bool>().ShouldBeFalse();

str = "1";
str.To<bool>().ShouldBeTrue();
str = "0";
str.To<bool>().ShouldBeFalse();

str = "是";
str.To<bool>().ShouldBeTrue();
str = "否";
str.To<bool>().ShouldBeFalse();

str = null;
str.To<bool?>().ShouldBeNull();
```


## 10. 指定转换失败时使用默认值

```csharp
var str = "lp1";
str.ToWithDefault<int>().ShouldBe(default(int));
str.ToWithDefault<int>(2).ShouldBe(2);
```

