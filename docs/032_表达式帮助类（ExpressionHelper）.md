# 表达式帮助类(ExpressionHelper)
这个类能够快速处理lambda表达式。

## 1. ExpressionHelper.GetInitOrReturnPropNames
遍历表达式，返回lambda表达式初始化或返回的对象属性名称。
示例：
```csharp
Expression<Func<Person>> expression = () => new Person
{
    Id = 1,
    Name = "",
    Age = 2
};
var names = ExpressionHelper.GetInitOrReturnPropNames(expression);
names.ShouldBe(new List<string> { "Id", "Name", "Age" });

Expression<Func<Person, Person>> expression2 = (p) => new Person
{
    Id = 1,
    Name = "",
    Age = 2
};
names = ExpressionHelper.GetInitOrReturnPropNames(expression2);
names.ShouldBe(new List<string> { "Id", "Name", "Age" });

expression2 = p => p;
names = ExpressionHelper.GetInitOrReturnPropNames(expression2);
names.ShouldBe(new List<string> { });

Expression<Func<Person, Object>> expression3 = (p) => p.Id;
names = ExpressionHelper.GetInitOrReturnPropNames(expression3);
names.ShouldBe(new List<string> { "Id" });
```

## 2. ExpressionHelper.GetAccessNames
遍历表达式，返回其中访问到的参数属性名称。
示例：
```csharp
Expression<Func<Person, Object>> expression = p => p.Id;
var cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Id" });

expression = p => new { p.Id, p.Name };
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Id", "Name" });

expression = p => new { p.Id, p.Name, p.Obj };
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Id", "Name", "Obj" });

expression = p => p.Id + p.Name + p.Obj;
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Id", "Name", "Obj" });

expression = p => p.Age + p.InnerPerson.Id;
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Age", "InnerPerson" });

expression = p => p.Id > 0 ? p.Name : $"小明{p.Birth}";
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>() { "Id", "Name", "Birth" });

expression = p => p;
cols = ExpressionHelper.GetAccessNames(expression);
cols.ShouldBe(new List<string>());
```

另外，当参数是多个时，使用：ExpressionHelper.GetAccessMutilNames。

## 3. ExpressionHelper.ReduceLambda
能快速简化lambda表达式。

对于下面表达式我们能不能将它简化呢？
```csharp
var ids = new List<int>() { 1, 2, 3 };
Expression<Func<Person, bool>> expression = p => p.Id < 100 || ids.Count > 1;
```

当我们思考一下，是可以简化的，如下图所示：

![alt](../docs/imgs/reducelambda1.png)

在整棵树中，只有涉及到lambda表达式的参数部分不能先求出值，其他的都可以先求出来做简化。

于是有了下面的：

```csharp
var exp = ExpressionHelper.ReduceLambda(expression);
exp.NodeType.ShouldBe(ExpressionType.Constant);
(exp as ConstantExpression).Value.ShouldBe(true);
```
即，简化后就成了一个常量，True。这里变成了true，是因为 "||" 运算符本身的特性（一个为true，整体为true），它的右侧已经被求出是常量 True 了，所以左侧就没有存在的意义了。


那么，```ExpressionHelper.ReduceLambda```的原理也就很清晰了：
- 对于表达式树中不牵扯到参数的部分进行 compile 求职，并组装成新的表达式树；
- 对于具有短路功能的运算符进一步简化，包括："&&"、"||"、"?:"、"??"；

更多的示例不再演示，可以参考：https://gitee.com/jackletter/DotNetCommon/blob/master/tests/DotNetCommon.Test/ExpressionHelperTests/ReduceLambdaTests.cs


关于 ```ExpressionHelper.ReduceLambda``` 的性能，可以参考下面一段简化示例：

![alt](../docs/imgs/reducelambda2.png)

可以看到只有再预热时才会有大的性能消耗，这是因为需要先将 expression 编译成委托并放入缓存，后面的就很快了。