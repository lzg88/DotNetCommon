# 异步锁



我们知道 lock 关键字不能锁异步代码，IDE会直接报错，而 Monitor 就是lock关键字的实现。

所以，在异步代码中，我们不能指望 ```lock``` 或```Monitor```了。



为此，这里封装了一个```AsyncLocker```类用于给异步代码块加锁。它本身是基于```SemaphoreSlim```实现的，参照：[《c#多线程同步之临界区（lock、Monitor、ReadWriterLock）/互斥量（Mutex）/信号量（Semaphore[Slim]）/事件（Auto/ManualResetEvent）》](https://blog.csdn.net/u010476739/article/details/104937749)



用法示例：

```csharp
//常规使用,可设置超时时间, 超出后抛 TimeoutException 异常
await AsyncLocker.LockAsync("123", async () =>
{
    //somecode ...
},TimeSpan.FromSeconds(30));

//尝试锁内运行, 成功在锁内运行后返回true, 进入锁失败后返回false
if (!await AsyncLocker.TryLockAsync("123", async () =>
{
    //somecode...
}, TimeSpan.FromSeconds(30)))
{
    return Result.NotOk("操作超时,请稍后重试!");
}

//有返回值的锁
var person = await AsyncLocker.LockRetAsync("123", async () =>
{
    //somecode ...
    return new Person();
},TimeSpan.FromSeconds(30));
```



