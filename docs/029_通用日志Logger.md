# 通用日志Logger

日志组件是蛮重要的，尤其是在快速定位线上问题的时候。

基于此，```DotNetCommon```类库提供了通用日志组件入口，它有如下特点：

1. 代码轻量，总共5个类，代码共计：327行（含注释）；

2. 向```Microsoft.Extensions.Logging```看起，里面包含日志类别、日志级别的概念；

   > DotNetCommon日志级别和微软日志框架的日志级别是一一对应的，不同的是，微软日志框架在类别和级别概念上做了过滤的功能，而在```DotNetCommon```类库中，日志组件的类别和级别仅仅是一个标记，不含其他功能。

3. ```DotNetCommon```类库的日志组件本身提供了文本记录器，默认在<kbd>AppDomain.Current.BaseDirectory/.logs</kbd>目录，并默认开启；

4. 可以轻松集成其他日志框架，下面展示如何将```DotNetCommon```类库的日志输出指向```Microsoft.Extensions.Logging```日志框架：

   ```csharp
   //startup.cs
   //Configure
   var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
   DotNetCommon.Logger.LoggerFactory.SetLogger(ctx =>
   {
        loggerFactory.CreateLogger(ctx.CategoryName).Log(ctx.Level.To<LogLevel>(), ctx.Message);
   }, true);
   //设置后，所有使用DotNetCommon日志框架中输出的日志能够自动流向微软日志框架，统一web环境和简单非web环境的日志使用需求
   //第二个参数: 设置为true表示继续使用默认的文件输出(输出到.logs目录下)，如果为false，则取消默认的文件输出
   ```

5. 灵活的配置，可以控制日志是否输出、输出到的目录、输出的格式等。

   

> 注意：无论是DotNetCommon日志框架还是微软日志框架，每个类别的ILogger实例都是单例的，如下面代码：
>
> ```csharp
> public class DemoController:ControllerBase
> {
>         private readonly ILogger<DemoController> logger = null;
>         public DemoController(ILogger<DemoController> logger)
>         {
>             this.logger = logger;
>         }
> }
> ```
>
> 虽然每次请求都会创建新的DemoController实例，但获取到的<kbd>logger</kbd>却是同一个。



## 一、使用方法示例

### 1.1 非主机环境

> 在这个环境下，没有微软依赖容器，没有微软日志框架。

一个普通控制台程序使用如下：

```csharp
public class Program
{
    public static void Main(string[] args)
    {
        var logger = DotNetCommon.Logger.LoggerFactory.CreateLogger<Startup>();
        //输出到exe文件平行的.logs目录下面
        logger.LogTrace("LogTrace");
        logger.LogDebug("LogDebug");
        logger.LogInformation("LogInformation");
        logger.LogWarning("LogWarning");
        logger.LogError("LogError");
        logger.LogCritical("LogCritical");
    }
}
```



### 1.2 asp.net core环境

```csharp
public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
{
	var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
	DotNetCommon.Logger.LoggerFactory.SetLogger(ctx =>
	{
		loggerFactory.CreateLogger(ctx.CategoryName).Log(ctx.Level.To<LogLevel>(), ctx.Message);
	}, true);

	app.UseRouting();

	app.UseEndpoints(endpoints =>
	{
		endpoints.MapGet("/", async context =>
		{
			var logger = DotNetCommon.Logger.LoggerFactory.CreateLogger<Startup>();
			logger.LogTrace("LogTrace");
			logger.LogDebug("LogDebug");
			logger.LogInformation("LogInformation");
			logger.LogWarning("LogWarning");
			logger.LogError("LogError");
			logger.LogCritical("LogCritical");
			await context.Response.WriteAsync("Hello World!");
		});
	});
}
```



## 二、 日志输出的流程


直接看图：

![image-20210827115634741](./imgs/logger-flow.png)

可以看到，日志提供了4中配置分别作用在上面不同的节点上。

- 设置1：可以通过此设置将日志的输出转向到其他日志框架，比如：asp.net core等；
- 设置2：可以拦截某些日志是否能输出，比如：根据类别和级别进行控制等；
- 设置3：可以手动指定日志的输出格式，比如：输出json格式以方便ELK收集；
- 设置4：可以手动指定日志文件的输出路径，比如：根据类别和级别输出到不同的目录；

上面的这几个配置，在示例工程：DotNetCommonLoggerConsoleTest 、DotNetCommonLoggerWebTest中有体现。



## 三、与其他组件协作关系



首先，我们将代码分为三类：

1. 应用代码（业务代码）：比如，我们创建了一个网站，那么我们自己写的代码就属于应用代码；
2. DotNetCommon日志组件：就是这个；
3. 其他三方组件：比如某个工具库xxxx；



那么，建议的使用情况（web环境）：

- xxxx工具库依赖DotNetCommon日志组件, 在需要时直接调用DotNetCommon日志组件输出就行，不需要对它进行任何配置（比如：输出目录、格式等）；
- 业务代码输出日志时继续调用asp.net core 框架，不要调用DotNetCommon日志组件输出（不是不行，是不建议）；

- 在网站启动时（startup的Configure方法）配置DotNetCommon日志实际向asp.net core框架输出；



在非web环境下，建议的使用情况（如：桌面软件、控制台等）：

- xxxx工具库依赖DotNetCommon日志组件, 在需要时直接调用DotNetCommon日志组件输出就行，不需要对它进行任何配置（比如：输出目录、格式等）；
- 业务代码也调用DotNetCommon日志组件进行输出；
- 在软件启动时，根据需要配置DotNetCommon日志组件的输出格式、路径、能否输出等；
