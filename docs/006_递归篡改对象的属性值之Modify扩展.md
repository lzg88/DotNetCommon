# 递归篡改对象的属性值之Modify扩展

> 所属包: DotNetCommon.Core



在写web接口时，需要将敏感属性（如：身份证号）脱敏处理，于是写了这个接口，以全局过滤器的方式拦截响应实体进行篡改数据。

> 同Mapper扩展方法一样，这个功能也是基于反射实现的。

直接看示例：

```csharp
public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string IdentityCard { get; set; }
 }
//隐藏身份证号
public void Test()
{
    var ent=new Person()
    {
        Id=1,
        Name="小明",
        IdentityCard="411234198801021234"
    }
    ent.Modify("IdentityCard", old => "****");
    //输出: ****
	Console.Writeline(ent.IdentityCard);
}
```



上面演示过于简单，看下面复杂的情景：

```csharp
public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string IdentityCard { get; set; }

    public List<Student> Students { get; set; }
    public Student[] OtherStudents { set; get; }
}

//隐藏身份证号
public void Test()
{
    var ent=new Person()
    {
        Id=1,
        Name="小明",
        IdentityCard="411234198801021234",
        Students = new List<Person>()
        {
            new Person()
            {
                Id=2,
                Name="张飞",
                IdentityCard="411234198801021234"
            },
            new Person()
            {
                Id=3,
                Name="东方不败",
                IdentityCard="411234198801021234"
            }
        },
        OtherStudents = new Person[3]
        {
            new Person()
            {
                Id=4,
                Name="张三",
                IdentityCard="411234198801021234"
            },
            null
        }
    }
    ent.Modify("IdentityCard", old => "****");
    //ent中所有的IdentityCard属性的值都会变成****，包括：Person直接属性、Students和OtherStudents中元素的属性
}
```



这个方法的作用是递归更改实体内某个属性的值，而无论这个属性的位置。





