# 常用扩展方法

> 所属包: DotNetCommon.Core



> 扩展方法都在```DotNetCommon.Extensions```命名空间下面，上面介绍的```Mapper()```、```To<T>()```、```Modify()```都是扩展方法。



## 1. DateTime扩展方法

直接看示例：

```csharp
//更快捷的打印时间戳
//ToCommonString、ToCommonDateString、ToCommonTimeString、ToCommonMinuteString、ToCommonStampString
// 使用全球化的方法: ToGlobalString、ToGlobalStampString
var dt = new DateTime(2021, 2, 2, 13, 45, 02, 123);
var str = dt.ToCommonString();
str.ShouldBe("2021-02-02 13:45:02");

str = dt.ToCommonDateString();
str.ShouldBe("2021-02-02");

str = dt.ToCommonTimeString();
str.ShouldBe("13:45:02");

str = dt.ToCommonMinuteString();
str.ShouldBe("2021-02-02 13:45");

str = dt.ToCommonStampString();
str.ShouldBe("2021-02-02 13:45:02.123");

str = dt.ToGlobalString();
str.ShouldBe("2021-02-02 13:45:02 +08:00");

str = dt.ToGlobalStampString();
str.ShouldBe("2021-02-02 13:45:02.123 +08:00");

str = dt.ToFileNameString();
str.ShouldBe("20210202134502");

str = dt.ToFileNameGuidString();
//输出如: 20210202134502123_5223ed80c21a4facbc30225ed7e061d5.log

//还有其他的扩展方法，比如，判断闰年(IsLeapYear)等
```

## 2. 集合扩展

除了```ToTree```、```FetchToTree```、```ToFlat```、```RecurseTree()```、```FilterTree```扩展方法，常用的集合方法还有如下：

- ToDictionary： 将集合对象转为字典

  ```csharp
  var list = new List<Person>()
  {
      new Person(){Id=1,Name="张三",Birth=new DateTime(1990,01,01)},
      new Person(){Id=2,Name="李四",Birth=new DateTime(1991,01,01)},
  };
  var dic = list.ToDictionary(i => i.Id, i => i.Name);
  dic.Count.ShouldBe(2);
  dic[1].ShouldBe("张三");
  dic[2].ShouldBe("李四");
  ```
  
- Remove: 根据条件表达式批量移除元素

  ```csharp
  var list = new List<Person>()
  {
      new Person(){Id=1,Name="张三",Birth=new DateTime(1990,01,01)},
      new Person(){Id=2,Name="李四",Birth=new DateTime(1991,01,01)},
  };
  list.Remove(i => i.Id >= 1);
  list.Count.ShouldBe(0);
  ```
  
- GetPage: 简化 Skip(pageIndex * pageSize).Take(pageSize)调用

  ```csharp
  var list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
  var newList = list.GetPage(2, 2).ToList();
  newList.ShouldBe(new List<int>() { 3, 4 });
  ```

- IsNotNullOrEmpty: 判断集合不为空

  ```csharp
  List<int> list = null;
  list.IsNotNullOrEmpty().ShouldBeFalse();
  list = new List<int>();
  list.IsNotNullOrEmpty().ShouldBeFalse();
  list = new List<int>() { 1 };
  list.IsNotNullOrEmpty().ShouldBeTrue();
  ```

- IsNullOrEmpty: 判断集合是否为空

- ToStringSeparated: 将当前集合中的元素根据指定的分隔符拼接成字符串

  ```csharp
  var list = new List<int>() { 1, 2, 3 };
  list.ToStringSeparated(",").ShouldBe("1,2,3");
  var arr = new int[] { 1, 2 };
  arr.ToStringSeparated(",").ShouldBe("1,2");
  ```

## 3. 文件和文件夹扩展

- DirectoryInfo.GetSizeInBytes:  获取文件夹大小（单位：B）
- DirectoryInfo.GetSizeInHumanize：获取文件夹大小（人类易读大小）
- DirectoryInfo.IsHidden：判断文件夹是否是隐藏的
- FileInfo.IsHidden：判断文件是否是隐藏的
- FileInfo.Rename：重命名文件
- FileInfo.IsBinary：判断文件是否是二进制格式



## 4. 泛型扩展

- T.ToDictionary： 将当前对象转成字典(属性名->属性值)

  ```csharp
  var obj = new { Id = 1, Name = "小明", Age = 20 };
  var dic = obj.ToDictionary(true);
  dic.ShouldBe(new Dictionary<string, object>() { { "Id", 1 }, { "Name", "小明" }, { "Age", 20 } });
  
  var person = new Person()
  {
      Id = 1,
      Name = "小明"
  };
  dic = person.ToDictionary(true);
  dic.ShouldBe(new Dictionary<string, object>() { { "Id", 1 }, { "Name", "小明" } });
  ```

- T.IsDefault: 判断当前对象是否是所属类型的默认值

  ```csharp
  int? i = null;
  i.IsDefault().ShouldBeTrue();
  
  int j = 0;
  j.IsDefault().ShouldBeTrue();
  
  object obj = null;
  obj.IsDefault().ShouldBeTrue();
  ```

- T.ToCompletedTask: 将当前对象封住为一个完成态的Task

  ```csharp
  var i = 2;
  var task = i.ToCompletedTask();
  task.IsCompleted.ShouldBeTrue();
  task.Result.ShouldBe(2);
  ```



## 5. string扩展

- Format：string.Format 的简化方法

  ```csharp
  var str = "hello {0} {1}";
  str.Format("tom", "什么").ShouldBe("hello tom 什么");
  ```

- IsNullOrEmpty: ```public static bool IsNullOrEmpty(this string value) => string.IsNullOrEmpty(value);```

- IsNotNullOrEmpty： ```public static bool IsNotNullOrEmpty(this string value) => !value.IsNullOrEmpty();```

- IsNullOrEmptyOrWhiteSpace: ```public static bool IsNullOrEmptyOrWhiteSpace(this string value) => string.IsNullOrWhiteSpace(value);```

- IsNotNullOrEmptyOrWhiteSpace: ```public static bool IsNotNullOrEmptyOrWhiteSpace(this string value) => value.IsNullOrEmptyOrWhiteSpace();```

- SplitAndTrimTo: 将字符串按照分隔符拆分为指定数据类型的集合

  ```csharp
  var str = "1,2,4";
  str.SplitAndTrimTo<int>(",").ShouldBe(new List<int>() { 1, 2, 4 });
  str=",1,245,42"; //注意：DotNetCommon2.7.1(DotNetCommon.Core1.7.1)后不再兼容 ",1,245,pokpl,42" 这种写法，原来不报异常，之后会报异常
  str.SplitAndTrimTo<int>(",").ShouldBe(new List<int>() { 1, 245, 42 });
  ```

- ToObject: 使用json的反序列化将字符串转为对象

  ```csharp
  var str = "[1,2,3]";
  str.ToObject<List<int>>().ShouldBe(new List<int>() { 1, 2, 3 });
  ```



## 6. Type扩展

- IsNullable：判断是否是Nullable类型的

  ```csharp
  typeof(int).IsNullable().ShouldBeFalse();
  typeof(int?).IsNullable().ShouldBeTrue();
  ```

- IsNumeric: 判断当前类型是否是数字类型

  ```csharp
  typeof(int).IsNumeric().ShouldBeTrue();
  typeof(int?).IsNullable().ShouldBeTrue();
  
  typeof(bool).IsNullable().ShouldBeFalse();
  ```

- IsSequence: 判断当前类型是否是序列(数组、集合) 注意：包括字符串

  ```csharp
  typeof(List<int>).IsSequence(out var seqType).ShouldBeTrue();
  typeof(int).IsSequence(out var seqType2).ShouldBeFalse();
  typeof(string).IsSequence(out var seqType3).ShouldBeTrue();
  typeof(object).IsSequence(out var seqType4).ShouldBeFalse();
  ```

- IsArrayOf: 判断当前类型是否是数组

  ```csharp
  typeof(List<int>).IsArrayOf<int>().ShouldBeFalse();
  typeof(int[]).IsArrayOf<int>().ShouldBeTrue();
  typeof(object[]).IsArrayOf<object>().ShouldBeTrue();
  ```

- IsGenericList: 判断当前类型是否是泛型集合（List<>、IList<>）

  ```csharp
  typeof(List<int>).IsGenericList().ShouldBeTrue();
  typeof(IList<int>).IsGenericList().ShouldBeTrue();
  typeof(ICollection<int>).IsGenericList().ShouldBeFalse();
  ```

  

## 7. Uri扩展

- ParseQueryString：从查询字符串中提取参数和值

  ```csharp
  var uri1 = new Uri("http://www.baz.com/abc.svc?%23name=bar&age=10");
  
  var queryStringParams1 = uri1.ParseQueryString().ToArray();
  
  queryStringParams1.ShouldNotBeNull();
  queryStringParams1.ShouldNotBeEmpty();
  queryStringParams1.Length.ShouldBe(2);
  
  queryStringParams1[0].Key.ShouldBe("#name");
  queryStringParams1[0].Value.ShouldBe("bar");
  
  queryStringParams1[1].Key.ShouldBe("age");
  queryStringParams1[1].Value.ShouldBe("10");
  ```

- AddParametersToQueryString: 添加参数到指定的url中

  ```csharp
  var uri = new Uri("http://www.baz.com/abc.svc");
  
  var parameters = new Dictionary<string, string>
  {
      ["#foo"] = "#some value, & stuff",
      ["baz"] = "bar"
  };
  
  var newUri = uri.AddParametersToQueryString(parameters);
  newUri.Query.ShouldBe("?%23foo=%23some+value%2C+%26+stuff&baz=bar");
  
  var parsed = newUri.ParseQueryString().ToArray();
  parsed.Length.ShouldBe(2);
  
  parsed[0].Key.ShouldBe("#foo");
  parsed[0].Value.ShouldBe("#some value, & stuff");
  
  parsed[1].Key.ShouldBe("baz");
  parsed[1].Value.ShouldBe("bar");
  ```

## 8. 类型和方法的反射扩展
> 看这里之前，我们先明确一个概念：具化。
> 比如：
> - ```public class Person<T>``` 是定义了一个泛型类，而```Person<int>```则是这个泛型类的具化，可以将它们看做是一个模板和实例的关系（注意：不是继承）；
> - 同理```public class Person{ public static void Show<T>(){} }``` 是定义了一个泛型方法，而```Person.Show<int>()```则是将它具化后的调用；
> - 另外，我们也常见泛型类中定义泛型方法，如：```public class Person<T>{ public static void Show<T2>(){} }```；
> - 补充一点：代码运行时，类和方法都必须是具化的，否则是不能正常使用（比如：创建实例、方法调用等）的。
>
> 下面的扩展方法，将围绕反射类名、方法名展开（考虑泛型、嵌套类）。

### 8.1 Type.GetClassFullName
获取一个类的名称描述，支持非泛型类、泛型类以及泛型类的具化。
输出的格式:

```命名空间[.外部类名].类名[<泛型1[, 泛型2]>]```

注意：
- 多个泛型之间有空格；

简单示例:
```csharp
namespace DotNetCommon.Test.Extensions.TypeExtensions
{
  public class Normal { }
  public struct Point { }
  public enum EnumType { }
}

//测试方法
//简单自定义类型
typeof(Normal).GetClassFullName().ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.Normal");
typeof(Point).GetClassFullName().ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.Point");
typeof(EnumType).GetClassFullName().ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.EnumType");
typeof(EnumType?).GetClassFullName().ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.EnumType?");

//系统预设类型
typeof(int).GetClassFullName().ShouldBe("int");
typeof(int?).GetClassFullName().ShouldBe("int?");
typeof((int?, string[])).GetClassFullName().ShouldBe("System.ValueTuple<int?, string[]>");
typeof(List<>).GetClassFullName().ShouldBe("System.Collections.Generic.List<T>");
typeof(List<List<int?[][]>>).GetClassFullName().ShouldBe("System.Collections.Generic.List<System.Collections.Generic.List<int?[][]>>");
```

泛型类的支持示例：
```csharp
namespace DotNetCommon.Test.Extensions.TypeExtensions
{
    public class DemomoFu<T>
    {
        public class DemoZi<T2, T3>
        {
            public class DemoSun<T5> { }
        }
    }
}

//测试方法
var type = typeof(DemomoFu<int?>.DemoZi<long[], double?[]>.DemoSun<string>);
var name = type.GetClassFullName();
name.ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.DemomoFu<int?>.DemoZi<long[], double?[]>.DemoSun<string>");

type = typeof(DemomoFu<>.DemoZi<,>.DemoSun<>);
name = type.GetClassFullName();
name.ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.DemomoFu<T>.DemoZi<T2, T3>.DemoSun<T5>");
```

### 8.2 Type.GetClassGenericFullName
相比于 Type.GetClassFullName 方法，这个总是输出泛型的定义模板，并列出具化的映射关系。
注意：
  - 当类有多层泛型时，只取最外层（看示例）;
  - 可空类型将转为 System.Nullable\<T\> （看示例）；

示例：

```csharp
namespace DotNetCommon.Test.Extensions.TypeExtensions
{
    public class HE<T5, T8>
    {
        public class OK<T9> { }
    }
}

//测试方法1
var res = new HE<int, string>.OK<double?>().GetType().GetClassGenericFullName();
res.Name.ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.HE<T5, T8>.OK<T9>");
res.GenericTypes.Count.ShouldBe(3);
res.GenericTypes[0].name.ShouldBe("T5");
res.GenericTypes[1].name.ShouldBe("T8");
res.GenericTypes[2].name.ShouldBe("T9");
res.GenericTypes[0].isGeneric.ShouldBeFalse();
res.GenericTypes[1].isGeneric.ShouldBeFalse();
res.GenericTypes[2].isGeneric.ShouldBeFalse();
res.GenericTypes[0].type.ShouldBe(typeof(int));
res.GenericTypes[1].type.ShouldBe(typeof(string));
res.GenericTypes[2].type.ShouldBe(typeof(double?));

//测试方法2
res = typeof(HE<,>.OK<>).GetClassGenericFullName();
res.Name.ShouldBe("DotNetCommon.Test.Extensions.TypeExtensions.HE<T5, T8>.OK<T9>");
res.GenericTypes.Count.ShouldBe(3);
res.GenericTypes[0].name.ShouldBe("T5");
res.GenericTypes[1].name.ShouldBe("T8");
res.GenericTypes[2].name.ShouldBe("T9");
res.GenericTypes[0].isGeneric.ShouldBeTrue();
res.GenericTypes[1].isGeneric.ShouldBeTrue();
res.GenericTypes[2].isGeneric.ShouldBeTrue();
res.GenericTypes[0].type.Name.ShouldBe("T5");
res.GenericTypes[1].type.Name.ShouldBe("T8");
res.GenericTypes[2].type.Name.ShouldBe("T9");

//注意点：
res = typeof(int?).GetClassGenericFullName();
res.Name.ShouldBe("System.Nullable<T>");

res = typeof(int?[][]).GetClassGenericFullName();
res.Name.ShouldBe("System.Nullable<T>[][]");

res = typeof(List<List<List<int?>>>).GetClassGenericFullName();
res.Name.ShouldBe("System.Collections.Generic.List<T>");
```

### 8.3 MethodInfo.GetMethodFullName
获取一个方法的描述。它的输出格式：

```返回值类型 方法类名.方法名[<泛型1>[, 泛型2]]([参数1类型名 参数1名][, 参数2类型名 参数2名])```

注意点：
- 涉及到类型名的同 Type.GetClassFullName；
- 方法的多个泛型之间有空格；
- 方法的多个参数之间有空格；
- (int,string) 这种元组将会转成 System.ValueTuple\<int, string\>
- 没有任何其他修饰符，如: public virtual abstract ref in out this static 等；

简单示例：
```csharp
namespace DotNetCommon.Test.Extensions.MethodInfoExtensions
{
    public class Person
    {
        public void Show() => throw new NotImplementedException();
        public string Show2(int x, Person y) => throw new NotImplementedException();
        public (int i, Person p) Show3(int x, Person y) => throw new NotImplementedException();
        public static T2 Show4<T, T2>(T t, T2 t3) => throw new NotImplementedException();
    }
}

//测试方法
typeof(Person).GetMethod("Show").GetMethodFullName().ShouldBe("void DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show()");
typeof(Person).GetMethod("Show2").GetMethodFullName().ShouldBe("string DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show2(int x, DotNetCommon.Test.Extensions.MethodInfoExtensions.Person y)");
typeof(Person).GetMethod("Show3").GetMethodFullName().ShouldBe("System.ValueTuple<int, DotNetCommon.Test.Extensions.MethodInfoExtensions.Person> DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show3(int x, DotNetCommon.Test.Extensions.MethodInfoExtensions.Person y)");
typeof(Person).GetMethod("Show4").GetMethodFullName().ShouldBe("T2 DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show4<T, T2>(T t, T2 t3)");
typeof(Person).GetMethod("Show4").MakeGenericMethod(typeof(int), typeof(string)).GetMethodFullName().ShouldBe("string DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show4<int, string>(int t, string t3)");

Expression<Func<List<int>, int>> exp = list => list.FirstOrDefault(i => i > 1);
var call = (exp as LambdaExpression).Body as MethodCallExpression;
call.Method.GetMethodFullName().ShouldBe("int System.Linq.Enumerable.FirstOrDefault<int>(System.Collections.Generic.IEnumerable<int> source, System.Func<int, bool> predicate)");
```

### 8.4 MethodInfo.GetMethodGenericFullName
相比于 MethodInfo.GetMethodFullName 方法，这个总是输出泛型的定义，并列出具化的映射关系（包含泛型类和泛型方法的映射，类的映射在前，方法的映射在后）。

注意：
 - 当类、参数有多层泛型时，只取最外层（看示例）;

简单示例：
```csharp
namespace DotNetCommon.Test.Extensions.MethodInfoExtensions
{
    public class Person
    {
        public string Show2(int x, Person y) => throw new NotImplementedException();
        public static T2 Show4<T, T2>(T t, T2 t3) => throw new NotImplementedException();
    }
}

//测试方法（非泛型）
var res = typeof(Person).GetMethod("Show2").GetMethodGenericFullName();
res.Name.ShouldBe("string DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show2(int x, DotNetCommon.Test.Extensions.MethodInfoExtensions.Person y)");
res.GenericTypes.Count.ShouldBe(0);

//测试方法（泛型方法具化后）
res = typeof(Person).GetMethod("Show4").MakeGenericMethod(typeof(int), typeof(string)).GetMethodGenericFullName();
res.Name.ShouldBe("T2 DotNetCommon.Test.Extensions.MethodInfoExtensions.Person.Show4<T, T2>(T t, T2 t3)");
res.GenericTypes.Count.ShouldBe(2);
res.GenericTypes[0].name.ShouldBe("T");
res.GenericTypes[0].isGeneric.ShouldBe(false);
res.GenericTypes[0].type.ShouldBe(typeof(int));
res.GenericTypes[1].name.ShouldBe("T2");
res.GenericTypes[1].isGeneric.ShouldBe(false);
res.GenericTypes[1].type.ShouldBe(typeof(string));
```

再看个复杂点的(当类也是泛型时)：
```csharp
namespace Complex
{
    public class Top<TTop>
    {
        public class Middle<TMiddle>
        {
            public class Low<TLow>
            {
                public static void Show<T2>(TTop top, TMiddle middle, TLow low, T2 t2) => throw new NotImplementedException();
            }
        }
    }
}

//测试方法1（泛型方法具化后）
var res = typeof(Complex.Top<int>.Middle<string>.Low<double>).GetMethod("Show").MakeGenericMethod(typeof(bool)).GetMethodGenericFullName();
res.Name.ShouldBe("void Complex.Top<TTop>.Middle<TMiddle>.Low<TLow>.Show<T2>(TTop top, TMiddle middle, TLow low, T2 t2)");
res.GenericTypes.Count.ShouldBe(4);
res.GenericTypes[0].name.ShouldBe("TTop");
res.GenericTypes[0].isGeneric.ShouldBe(false);
res.GenericTypes[0].type.ShouldBe(typeof(int));
res.GenericTypes[1].name.ShouldBe("TMiddle");
res.GenericTypes[1].isGeneric.ShouldBe(false);
res.GenericTypes[1].type.ShouldBe(typeof(string));
res.GenericTypes[2].name.ShouldBe("TLow");
res.GenericTypes[2].isGeneric.ShouldBe(false);
res.GenericTypes[2].type.ShouldBe(typeof(double));
res.GenericTypes[3].name.ShouldBe("T2");
res.GenericTypes[3].isGeneric.ShouldBe(false);
res.GenericTypes[3].type.ShouldBe(typeof(bool));

//测试方法2（泛型方法非具化）
res = typeof(Complex.Top<>.Middle<>.Low<>).GetMethod("Show").GetMethodGenericFullName();
res.Name.ShouldBe("void Complex.Top<TTop>.Middle<TMiddle>.Low<TLow>.Show<T2>(TTop top, TMiddle middle, TLow low, T2 t2)");
res.GenericTypes.Count.ShouldBe(4);
res.GenericTypes[0].name.ShouldBe("TTop");
res.GenericTypes[0].isGeneric.ShouldBe(true);
res.GenericTypes[0].type.Name.ShouldBe("TTop");
res.GenericTypes[1].name.ShouldBe("TMiddle");
res.GenericTypes[1].isGeneric.ShouldBe(true);
res.GenericTypes[1].type.Name.ShouldBe("TMiddle");
res.GenericTypes[2].name.ShouldBe("TLow");
res.GenericTypes[2].isGeneric.ShouldBe(true);
res.GenericTypes[2].type.Name.ShouldBe("TLow");
res.GenericTypes[3].name.ShouldBe("T2");
res.GenericTypes[3].isGeneric.ShouldBe(true);
res.GenericTypes[3].type.Name.ShouldBe("T2");
```