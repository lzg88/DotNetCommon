# DotNetCommon

#### 介绍
搜集.neter开发常用的功能，运行环境： net6.0； 

> QQ交流群：864844127

**主要包版本:**

|          包           |                             版本                             |                             下载                             | License                                                      |
| :-------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | ------------------------------------------------------------ |
|     DotNetCommon      | [![DotNetCommon](https://img.shields.io/nuget/v/DotNetCommon.svg?style=flat-square)](https://www.nuget.org/packages/DotNetCommon) | [![stats](https://img.shields.io/nuget/dt/DotNetCommon.svg?style=flat-square)](https://www.nuget.org/stats/packages/DotNetCommon?groupby=Version) | [![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/jackletter/DotNetCommon/blob/master/LICENSE) |
|   DotNetCommon.Core   | [![DotNetCommon.Core](https://img.shields.io/nuget/v/DotNetCommon.Core.svg?style=flat-square)](https://www.nuget.org/packages/DotNetCommon.Core) | [![stats](https://img.shields.io/nuget/dt/DotNetCommon.Core.svg?style=flat-square)](https://www.nuget.org/stats/packages/DotNetCommon.Core?groupby=Version) | [![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/jackletter/DotNetCommon/blob/master/LICENSE) |
|  DotNetCommon.PinYin  | [![DotNetCommon.PinYin](https://img.shields.io/nuget/v/DotNetCommon.PinYin.svg?style=flat-square)](https://www.nuget.org/packages/DotNetCommon.PinYin) | [![stats](https://img.shields.io/nuget/dt/DotNetCommon.PinYin.svg?style=flat-square)](https://www.nuget.org/stats/packages/DotNetCommon.PinYin?groupby=Version) | [![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/jackletter/DotNetCommon/blob/master/LICENSE) |
| DotNetCommon.Compress | [![DotNetCommon.Compress](https://img.shields.io/nuget/v/DotNetCommon.Compress.svg?style=flat-square)](https://www.nuget.org/packages/DotNetCommon.Compress) | [![stats](https://img.shields.io/nuget/dt/DotNetCommon.Compress.svg?style=flat-square)](https://www.nuget.org/stats/packages/DotNetCommon.Compress?groupby=Version) | [![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/jackletter/DotNetCommon/blob/master/LICENSE) |

**注意：**

- **从 4.0 后移除了对`Newtonsoft.Json`的依赖，这样`DotNetCommon.Core`将不依赖任何三方包；**

**整体关系如下图：**

![image-20221101092958287](./imgs/image-20221101092958287.png)



> 说明：
>
> 1. DotNetCommon包 具有所有功能，引用了其他的各个功能包；
>
> 2. DotNeCommon.Core 是核心包，在 `https://github.com/NimaAra/Easy.Common`基础上扩充而成，从DotNetCommon.Core4.0.0开始移除了对Newtonsoft.Json的依赖，之后将不再依赖任何三方包；
>
> 3. DotNetCommon.PinYin 是汉字转拼音包，从`https://github.com/toolgood/ToolGood.Words.Pinyin`搬运；
>
> 4. DotNetCommon.Window.Registry 是操作window注册表，里面就一个`RegistryHelper.cs`；
>
> 5. DotNetCommon.VerfiyCode 是生成验证码的，从 [csdn博文：SkiaSharp 生成验证码 .net6 生成验证码](https://blog.csdn.net/mjkmjk485485/article/details/125602744) 处搬运；
>
>    .net6平台下，System.Drawing.Common包已被标记为Window平台下专用，所以此功能依赖`SkiaSharp`，这个包是跨平台的，微软在维护。
>
> 6. DotNetCommon.Compress是压缩/解压缩的，里面就一个 `CompressHelper.cs`；



## 功能列表
1. 通用数据模型；
2. 通用树状结构&平铺数据的访问；
3. 通用身份认证模型；
4. 通用数据类型转换之Object.To方法；
5. 通用Dto间转换之Object.Mapper扩展；
6. 递归篡改对象的属性值之Modify扩展；
7. 将Dto属性投影到Entity之ModifyByDto扩展；
8. 校验框架;
9. 分布式id&分布式流水号；
10. 编码和加解密；
11. 序列化；
12. 汉字转拼音；
13. 压缩&解压缩；
14. 注册表；
15. 验证码生成;
16. 随机数；
17. 对象池；
18. 基于内存的并发消息队列；
19. 反射工具；
20. 主机诊断报告；
21. 对象深度比对工具；
22. 网络帮助类；
23. 单位转换器（B/KB/MS/GB）；
24. 金额大小写转换；
25. 枚举类型扩展方法；
26. 常用扩展方法；
27. 中文乱码检测（GBK or UTF-8）；
28. 读取Properties文件；
29. 通用日志Logger；
30. 常用内存缓存；
31. 异步锁（AsyncLocker）；
32. 表达式帮助类(ExpressionHelper)；
33. 对象(poco)深度克隆;

## 更多功能介绍

查看：https://gitee.com/jackletter/DotNetCommon/tree/master/docs



## 快速开始

### 1.安装包

```
dotnet add package DotNetCommon
```
### 2. 引入命名空间

```csharp
using DotNetCommon;
using DotNetCommon.Extensions;
```
### 3. 功能示例
#### 3.1 数据模型

```csharp
public Result<Person> GetUserById(int id)
{
    if (id < 0) return Result.NotOk("id必须大于0!");
    //...
    return Result.Ok(new Person());
}
```
#### 3.2 加解密
```csharp
public void EncryptTest()
{
    var sensitiveData = "敏感信息";
    var key = "12345678"; 
    //加密
    var res = DESEncrypt.Encrypt(sensitiveData, key);
    //解密
	var res2= DESEncrypt.Decrypt(res, key);
}
```

#### 3.3 分布式Id & 分布式流水号
```csharp
public void Test()
{
    //首先设置当前机器id: 0-1023
    Machine.SetMachineId(1);
    //生成分布式id
    //输出示例: 185081270290616320
    long id = DistributeGenerator.NewId("key");
    //生成分布式流水号
    //输出示例: sno202105250001000001
    string sno = DistributeGenerator.NewSNO("sno", SerialFormat.CreateDistributeFast("sno", "yyyyMMdd", 6));
}
```

#### 3.4 序列化
```csharp
/// <summary>
/// 指定常用的设置,序列化为json字符串(使用System.Text.Json)
/// </summary>
public static string ToJson(this object obj, JsonSerializerOptions options = null)

/// <summary>
/// 指定常用的设置,序列化为json字符串
/// </summary>
/// <param name="obj"></param>
/// <param name="dateTimeFormatString">日期时间格式(DateTime)</param>
/// <param name="dateTimeOffsetFormatString">日期时间格式(DateTimeOffset)</param>
/// <param name="dateOnlyOffsetFormatString">日期时间格式(DateOnly)</param>
/// <param name="timeOnlyOffsetFormatString">日期时间格式(TimeOnly)</param>
/// <param name="number2String">是否将数字转为字符串</param>
/// <param name="ignoreNull">是否忽略null值的属性</param>
/// <param name="enum2String">是否将枚举转换为字符串</param>
/// <param name="lowerCamelCase">属性名称的首字母是否小写</param>
/// <param name="lowerCamelCaseDictionaryKey">字典key首字母是否小写</param>
/// <param name="isIntend">是否格式缩进</param>
/// <param name="otherSettings">其他的设置</param>
/// <remarks>注意: 虽然提供了 <c>lowerCamelCase</c> 和 <c>lowerCamelCaseDictionaryKey</c>, 但没有办法将 <c>JsonObject</c> 中的key首字母小写</remarks>
public static string ToJsonFast(this object obj,
    string dateTimeFormatString = null,
    string dateTimeOffsetFormatString = null,
    string dateOnlyOffsetFormatString = null,
    string timeOnlyOffsetFormatString = null,
    bool ignoreNull = false,
    bool enum2String = false,
    bool lowerCamelCase = false,
    bool lowerCamelCaseDictionaryKey = false,
    bool isIntend = false,
    bool number2String = false,
    Action<JsonSerializerOptions> otherSettings = null)

/// <summary>
/// 对已有的 <c>JsonSerializerOptions</c> 进行配置,以增强兼容性,可用在 web开发中,如:
/// <code>
/// services.AddControllers().AddJsonOptions(options =>JsonHelper.Configure(options.JsonSerializerOptions, dateTimeFormatString: "yyyy-MM-dd", lowerCamelCase: true));
/// </code>
/// </summary>
public static void Configure(JsonSerializerOptions options,
    string dateTimeFormatString = null,
    string dateTimeOffsetFormatString = null,
    string dateOnlyOffsetFormatString = null,
    string timeOnlyOffsetFormatString = null,
    bool enum2String = false,
    bool ignoreNull = false,
    bool lowerCamelCase = false,
    bool lowerCamelCaseDictionaryKey = false,
    bool isIntend = false,
    bool number2String = false)
```
#### 3.5 压缩&解压缩
```csharp
//压缩多个文件
CompressHelper.CompressZip("c:/test.zip", new Dictionary<string, string>
{
	{"中文B222.txt","c:/testfolder/中文B.txt" },
	{"testsubfolder/suba222.txt","c:/testfolder/testsubfolder/testsubfolder-suba.txt" },
	{"testfolder-a.txt","c:/testfolder/testfolder-a.txt" }
});
//压缩目录
CompressHelper.CompressZipFolder("c:/test.zip", "c:/testfolder");

//解压缩 支持 zip/7z/rar/gz/bz2/tar.gz/tar.bz2
CompressHelper.UnCompress("c:/testfolder.7z"), "c:/testfolder");
CompressHelper.UnCompress("c:/testfolder.tar.gz"), "c:/testfolder2");
```

#### 3.6 类似AutoMapper的转换
```csharp
public class Cat
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public DateTime Birth { get; set; }
}

public class CatDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age
    {
        get
        {
            return DateTime.Now.Year - Birth.Year;
        }
    }
    public DateTime Birth { get; set; }
}

//转换示例
var cat = new Cat()
{
    Id = 1,
    Name = "小明",
    Birth = DateTime.Parse("1989-01-02"),
    Age = 20
};
var dto = cat.Mapper<CatDto>();
dto.ShouldNotBeNull();
dto.Id.ShouldBe(1);
dto.Name.ShouldBe("小明");
dto.Age.ShouldNotBe(20);
```



#### 3.7 类FluentValidation校验组件

```csharp
//Service层方法，添加实体
public Result<bool> AddStudent(Student student)
{
    var res = ValidateModelHelper.ValidResult(student, Student.ValidAdd);
    if (!res.Success) return res;
    //...新增操作
    return Result.Ok(true);
}

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int? Age { get; set; }
    public DateTime? Birth { get; set; }
    public string IdCard { get; set; }
    public string Addr { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }

    /// <summary>
    /// 校验新增Student
    /// </summary>
    /// <param name="ctx"></param>
    public static void ValidAdd(ValidateContext<Student> ctx)
    {
        //请求实体不能为null，否则直接中断校验
        ctx.MustNotNull().IfFailThenExit();
        //Id必须为0
        ctx.RuleFor(i => i.Id).MustEqualTo(0);
        //姓名不能为空且长度在1-4之间
        ctx.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace().MustLengthInRange(1, 4);
        //年龄要么为null，要么>=0
        ctx.RuleFor(i => i.Age).When(i => i != null, ctx => ctx.MustGreaterThanOrEuqalTo(0));
        //出生日期要么为null,要么>=1800-01-01
        ctx.RuleFor(i => i.Birth).When(i => i != null, ctx => ctx.MustGreaterThanOrEuqalTo(DateTime.Parse("1800-01-01")));
        //校验身份证号
        ctx.RuleFor(i => i.IdCard).MustIdCard();
        //如果手机号码不为null就校验格式
        ctx.RuleFor(i => i.Phone).When(i => i != null, ctx => ctx.MustCellPhone());
        //如果邮箱不为null就校验格式
        ctx.RuleFor(i => i.Email).When(i => i != null, ctx => ctx.MustEmailAddress());
    }
}
```

#### 3.8 注册表

```csharp
public void Test2()
{
    var path = @"HKEY_CURRENT_USER\TestApplication\Res";
    //判断是否存在
    RegistryHelper.Exists(path);
    //删除项
    RegistryHelper.DeletePath(path);
    //设置值
    RegistryHelper.SetString(path, "name", "小明");
    //读取值
    var name = RegistryHelper.GetString(path, "name");
}
```



更多介绍，参考：https://gitee.com/jackletter/DotNetCommon/tree/master/docs