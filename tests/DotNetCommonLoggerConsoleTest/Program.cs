﻿using DotNetCommon.Extensions;
using DotNetCommon.Logger;
using System;
using System.IO;
using System.Threading;

namespace DotNetCommonLoggerConsoleTest
{
    internal class Program
    {
        static ILogger<Program> logger = DotNetCommon.Logger.LoggerFactory.CreateLogger<Program>();
        static void Main(string[] args)
        {
            //SetBaseDirectory
            DotNetCommon.Logger.LoggerFactory.SetBaseDirectory("c:/commonlog");
            //SetOutDirectory
            DotNetCommon.Logger.LoggerFactory.SetOutFile(arg =>
            {
                if (arg.LogContext.Level == DotNetCommon.Logger.LoggerLevel.Error || arg.LogContext.Level == DotNetCommon.Logger.LoggerLevel.Critical)
                {
                    return new[] { Path.Combine(arg.BaseDir, $"errors/{DateTime.Now.ToString("err-yyyyMMdd")}.txt"), arg.PreLogFile, arg.PreErrorLogFile };
                }
                return new[] { arg.PreLogFile };
            });
            ////SetCanOut
            //DotNetCommon.Logger.LoggerFactory.SetCanOut(arg =>
            //{
            //    return arg.Level >= DotNetCommon.Logger.LoggerLevel.Information;
            //});

            //SetOutFormat
            //DotNetCommon.Logger.LoggerFactory.SetOutFormat(arg =>
            //{
            //    if (arg.LogContext.CategoryName.StartsWith("DotNetCommonLoggerConsoleTest"))
            //    {
            //        return arg.LogContext.ToJson();
            //    }
            //    return arg.PreMessage;
            //});


            //logger.LogTrace("LogTrace");
            //logger.LogDebug("LogDebug");
            //logger.LogInformation("LogInformation");
            //logger.LogWarning("LogWarning");
            //logger.LogError("LogError");
            //logger.LogError(new Exception("测试异常"), "LogError");
            //logger.LogCritical("LogCritical");

            logger.LogInformation($"-------InnerException below-----------");
            try
            {
                Func1();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "testmain");
            }

            Thread.Sleep(1000);
            Console.WriteLine($"日志已输出...");
            Console.ReadLine();
        }

        static void Func1()
        {
            try
            {
                Func2();
            }
            catch (Exception ex)
            {
                throw new Exception("func1", ex);
            }
        }

        static void Func2()
        {
            try
            {
                Func3();
            }
            catch (Exception ex)
            {
                throw new Exception("func2", ex);
            }
        }

        static void Func3()
        {
            try
            {
                Func4();
            }
            catch (Exception ex)
            {
                throw new Exception("func3", ex);
            }
        }

        static void Func4()
        {
            try
            {
                var i = 0;
                i = i / i;
            }
            catch (Exception ex)
            {
                throw new Exception("func4", ex);
            }
        }
    }
}
