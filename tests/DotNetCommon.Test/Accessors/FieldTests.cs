﻿using DotNetCommon.Accessors;
using NUnit.Framework;
using Shouldly;
using System.Linq;

namespace DotNetCommon.Test.Accessors
{
    [TestFixture]
    internal sealed class FieldTests
    {
        public class Person
        {
            public int Id { get; set; }
            public string Name;
            public int Age;
            public int? AgeNullAble;
            private int _id;
            private int? _idNullAble;

            public readonly int IdReadOnly = 5;
            private readonly int? _idReadOnly2 = 10;
        }

        public class Student : Person
        {
            public int AgeStudent;
            public int? AgeNullAbleStudent;
            private int _idStudent;
            private int? _idNullAbleStudent;
        }

        [Test]
        public void TestObjectAccessorCreate()
        {
            var obj = new Person();
            //
            var accessor = Accessor.Build(obj);
            accessor.Fields.Count.ShouldBe(4);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();

            //
            accessor = Accessor.Build(obj, false, true);
            accessor.Fields.Count.ShouldBe(7);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();

            accessor.Fields.FirstOrDefault(i => i.Key == "_id").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "_idNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "_idReadOnly2").ShouldNotBeNull();

            //子类
            var obj2 = new Student();
            accessor = Accessor.Build(obj2);
            accessor.Fields.Count.ShouldBe(6);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeStudent").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAbleStudent").ShouldNotBeNull();

            //
            accessor = Accessor.Build(obj2, false, true);
            accessor.Fields.Count.ShouldBe(8);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeStudent").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAbleStudent").ShouldNotBeNull();

            accessor.Fields.FirstOrDefault(i => i.Key == "_idStudent").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "_idNullAbleStudent").ShouldNotBeNull();
        }

        [Test]
        public void TestObjectAccessorUse()
        {
            //父类
            var obj = new Person { Id = 1, Name = "小明", Age = 18 };
            var accessor = Accessor.Build(obj, false, true);
            accessor[obj, "Id"].ShouldBe(1);
            accessor[obj, "Name"].ShouldBe("小明");
            accessor[obj, "Age"].ShouldBe(18);
            accessor[obj, "AgeNullAble"].ShouldBe(null);
            accessor[obj, "_id"].ShouldBe(0);
            accessor[obj, "_idNullAble"].ShouldBe(null);
            accessor[obj, "IdReadOnly"].ShouldBe(5);
            accessor[obj, "_idReadOnly2"].ShouldBe(10);

            accessor[obj, "Id"] = 11;
            accessor[obj, "Name"] = "othername";
            accessor[obj, "Age"] = 28;
            accessor[obj, "AgeNullAble"] = 99;
            accessor[obj, "_id"] = 23;
            accessor[obj, "_idNullAble"] = 56;

            accessor[obj, "Id"].ShouldBe(11);
            accessor[obj, "Name"].ShouldBe("othername");
            accessor[obj, "Age"].ShouldBe(28);
            accessor[obj, "AgeNullAble"].ShouldBe(99);
            accessor[obj, "_id"].ShouldBe(23);
            accessor[obj, "_idNullAble"].ShouldBe(56);

            //子类
            var obj2 = new Student { Id = 1, Name = "小明", Age = 18 };
            accessor = Accessor.Build(obj2, false, true);
            accessor[obj2, "Id"].ShouldBe(1);
            accessor[obj2, "Name"].ShouldBe("小明");
            accessor[obj2, "Age"].ShouldBe(18);
            accessor[obj2, "AgeNullAble"].ShouldBe(null);
            accessor[obj2, "IdReadOnly"].ShouldBe(5);

            accessor[obj2, "AgeStudent"].ShouldBe(0);
            accessor[obj2, "AgeNullAbleStudent"].ShouldBe(null);
            accessor[obj2, "_idStudent"].ShouldBe(0);
            accessor[obj2, "_idNullAbleStudent"].ShouldBe(null);

            accessor[obj2, "Id"] = 11;
            accessor[obj2, "Name"] = "othername";
            accessor[obj2, "Age"] = 28;
            accessor[obj2, "AgeNullAble"] = 99;

            accessor[obj2, "AgeStudent"] = 66;
            accessor[obj2, "AgeNullAbleStudent"] = 77;
            accessor[obj2, "_idStudent"] = 88;
            accessor[obj2, "_idNullAbleStudent"] = 99;

            accessor[obj2, "Id"].ShouldBe(11);
            accessor[obj2, "Name"].ShouldBe("othername");
            accessor[obj2, "Age"].ShouldBe(28);
            accessor[obj2, "AgeNullAble"].ShouldBe(99);

            accessor[obj2, "AgeStudent"].ShouldBe(66);
            accessor[obj2, "AgeNullAbleStudent"].ShouldBe(77);
            accessor[obj2, "_idStudent"].ShouldBe(88);
            accessor[obj2, "_idNullAbleStudent"].ShouldBe(99);
        }

        [Test]
        public void TestGenericAccessorCreate()
        {
            var accessor = Accessor.Build<Person>();
            accessor.Fields.Count.ShouldBe(4);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();

            //
            accessor = Accessor.Build<Person>(false, true);
            accessor.Fields.Count.ShouldBe(7);
            accessor.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();

            accessor.Fields.FirstOrDefault(i => i.Key == "_id").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "_idNullAble").ShouldNotBeNull();
            accessor.Fields.FirstOrDefault(i => i.Key == "_idReadOnly2").ShouldNotBeNull();

            //子类
            var accessor2 = Accessor.Build<Student>();
            accessor2.Fields.Count.ShouldBe(6);
            accessor2.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeStudent").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeNullAbleStudent").ShouldNotBeNull();

            //
            accessor2 = Accessor.Build<Student>(false, true);
            accessor2.Fields.Count.ShouldBe(8);
            accessor2.Fields.FirstOrDefault(i => i.Key == "Name").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "Age").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeNullAble").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "IdReadOnly").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeStudent").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "AgeNullAbleStudent").ShouldNotBeNull();

            accessor2.Fields.FirstOrDefault(i => i.Key == "_idStudent").ShouldNotBeNull();
            accessor2.Fields.FirstOrDefault(i => i.Key == "_idNullAbleStudent").ShouldNotBeNull();
        }

        [Test]
        public void TestGenericAccessorUse()
        {
            //父类
            var obj = new Person { Id = 1, Name = "小明", Age = 18 };
            var accessor = Accessor.Build<Person>(false, true);
            accessor[obj, "Id"].ShouldBe(1);
            accessor[obj, "Name"].ShouldBe("小明");
            accessor[obj, "Age"].ShouldBe(18);
            accessor[obj, "AgeNullAble"].ShouldBe(null);
            accessor[obj, "_id"].ShouldBe(0);
            accessor[obj, "_idNullAble"].ShouldBe(null);
            accessor[obj, "IdReadOnly"].ShouldBe(5);
            accessor[obj, "_idReadOnly2"].ShouldBe(10);

            accessor[obj, "Id"] = 11;
            accessor[obj, "Name"] = "othername";
            accessor[obj, "Age"] = 28;
            accessor[obj, "AgeNullAble"] = 99;
            accessor[obj, "_id"] = 23;
            accessor[obj, "_idNullAble"] = 56;

            accessor[obj, "Id"].ShouldBe(11);
            accessor[obj, "Name"].ShouldBe("othername");
            accessor[obj, "Age"].ShouldBe(28);
            accessor[obj, "AgeNullAble"].ShouldBe(99);
            accessor[obj, "_id"].ShouldBe(23);
            accessor[obj, "_idNullAble"].ShouldBe(56);

            //子类
            var obj2 = new Student { Id = 1, Name = "小明", Age = 18 };
            var accessor2 = Accessor.Build<Student>(false, true);
            accessor2[obj2, "Id"].ShouldBe(1);
            accessor2[obj2, "Name"].ShouldBe("小明");
            accessor2[obj2, "Age"].ShouldBe(18);
            accessor2[obj2, "AgeNullAble"].ShouldBe(null);
            accessor2[obj2, "IdReadOnly"].ShouldBe(5);

            accessor2[obj2, "AgeStudent"].ShouldBe(0);
            accessor2[obj2, "AgeNullAbleStudent"].ShouldBe(null);
            accessor2[obj2, "_idStudent"].ShouldBe(0);
            accessor2[obj2, "_idNullAbleStudent"].ShouldBe(null);

            accessor2[obj2, "Id"] = 11;
            accessor2[obj2, "Name"] = "othername";
            accessor2[obj2, "Age"] = 28;
            accessor2[obj2, "AgeNullAble"] = 99;

            accessor2[obj2, "AgeStudent"] = 66;
            accessor2[obj2, "AgeNullAbleStudent"] = 77;
            accessor2[obj2, "_idStudent"] = 88;
            accessor2[obj2, "_idNullAbleStudent"] = 99;

            accessor2[obj2, "Id"].ShouldBe(11);
            accessor2[obj2, "Name"].ShouldBe("othername");
            accessor2[obj2, "Age"].ShouldBe(28);
            accessor2[obj2, "AgeNullAble"].ShouldBe(99);

            accessor2[obj2, "AgeStudent"].ShouldBe(66);
            accessor2[obj2, "AgeNullAbleStudent"].ShouldBe(77);
            accessor2[obj2, "_idStudent"].ShouldBe(88);
            accessor2[obj2, "_idNullAbleStudent"].ShouldBe(99);
        }

        [Test]
        public void TestGenericAccessorUseTry()
        {
            //父类
            var obj = new Person { Id = 1, Name = "小明", Age = 18 };
            var accessor = Accessor.Build<Person>(false, true);
            accessor.TryGet<int>(obj, "Id", out var id).ShouldBe(true); id.ShouldBe(1);
            accessor.TryGet<string>(obj, "Name", out var name).ShouldBe(true); name.ShouldBe("小明");
            accessor.TryGet<int>(obj, "Age", out var age).ShouldBe(true); age.ShouldBe(18);
            accessor.TryGet<int?>(obj, "AgeNullAble", out var ageNullAble).ShouldBe(true); ageNullAble.ShouldBe(null);
            accessor.TryGet<int>(obj, "_id", out var _id).ShouldBe(true); _id.ShouldBe(0);
            accessor.TryGet<int?>(obj, "_idNullAble", out var _idNullAble).ShouldBe(true); _idNullAble.ShouldBe(null);
            accessor.TryGet<int>(obj, "IdReadOnly", out var IdReadOnly).ShouldBe(true); IdReadOnly.ShouldBe(5);
            accessor.TryGet<int?>(obj, "_idReadOnly2", out var _idReadOnly2).ShouldBe(true); _idReadOnly2.ShouldBe(10);

            accessor.TrySet<int>(obj, "Id", 11).ShouldBeTrue();
            accessor.TrySet<string>(obj, "Name", "othername").ShouldBeTrue();
            accessor.TrySet<int>(obj, "Age", 28).ShouldBeTrue();
            accessor.TrySet<int?>(obj, "AgeNullAble", 99).ShouldBeTrue();
            accessor.TrySet<int>(obj, "_id", 23).ShouldBeTrue();
            accessor.TrySet<int?>(obj, "_idNullAble", 56).ShouldBeTrue();

            accessor[obj, "Id"].ShouldBe(11);
            accessor[obj, "Name"].ShouldBe("othername");
            accessor[obj, "Age"].ShouldBe(28);
            accessor[obj, "AgeNullAble"].ShouldBe(99);
            accessor[obj, "_id"].ShouldBe(23);
            accessor[obj, "_idNullAble"].ShouldBe(56);

            //子类
            var obj2 = new Student { Id = 1, Name = "小明", Age = 18 };
            var accessor2 = Accessor.Build<Student>(false, true);
            accessor2.TryGet<int>(obj2, "Id", out var id2).ShouldBe(true); id2.ShouldBe(1);
            accessor2.TryGet<string>(obj2, "Name", out var name2).ShouldBe(true); name2.ShouldBe("小明");
            accessor2.TryGet<int>(obj2, "Age", out var age2).ShouldBe(true); age2.ShouldBe(18);
            accessor2.TryGet<int?>(obj2, "AgeNullAble", out var AgeNullAble).ShouldBe(true); AgeNullAble.ShouldBe(null);
            accessor2.TryGet<int>(obj2, "IdReadOnly", out var IdReadOnly2).ShouldBe(true); IdReadOnly2.ShouldBe(5);
            accessor2.TryGet<int>(obj2, "AgeStudent", out var AgeStudent).ShouldBe(true); AgeStudent.ShouldBe(0);

            accessor2.TryGet<int?>(obj2, "AgeNullAbleStudent", out var AgeNullAbleStudent).ShouldBe(true); AgeNullAbleStudent.ShouldBe(null);
            accessor2.TryGet<int>(obj2, "_idStudent", out var _idStudent).ShouldBe(true); _idStudent.ShouldBe(0);
            accessor2.TryGet<int?>(obj2, "_idNullAbleStudent", out var _idNullAbleStudent).ShouldBe(true); _idNullAbleStudent.ShouldBe(null);

            accessor2.TrySet<int>(obj2, "Id", 11).ShouldBeTrue();
            accessor2.TrySet<string>(obj2, "Name", "othername").ShouldBeTrue();
            accessor2.TrySet<int>(obj2, "Age", 28).ShouldBeTrue();
            accessor2.TrySet<int>(obj2, "AgeNullAble", 99).ShouldBeTrue();

            accessor2.TrySet<int>(obj2, "AgeStudent", 66).ShouldBeTrue();
            accessor2.TrySet<int?>(obj2, "AgeNullAbleStudent", 77).ShouldBeTrue();
            accessor2.TrySet<int>(obj2, "_idStudent", 88).ShouldBeTrue();
            accessor2.TrySet<int?>(obj2, "_idNullAbleStudent", 99).ShouldBeTrue();

            accessor2[obj2, "Id"].ShouldBe(11);
            accessor2[obj2, "Name"].ShouldBe("othername");
            accessor2[obj2, "Age"].ShouldBe(28);
            accessor2[obj2, "AgeNullAble"].ShouldBe(99);

            accessor2[obj2, "AgeStudent"].ShouldBe(66);
            accessor2[obj2, "AgeNullAbleStudent"].ShouldBe(77);
            accessor2[obj2, "_idStudent"].ShouldBe(88);
            accessor2[obj2, "_idNullAbleStudent"].ShouldBe(99);
        }
    }
}