﻿namespace DotNetCommon.Test
{
    using NUnit.Framework;
    using Shouldly;
    using System;

    [TestFixture]
    public class Base36Tests
    {
        [TestCase]
        public void When_encoding() => Base36.Encode(10).ShouldBe("A");

        [TestCase]
        public void When_decoding() => Base36.Decode("A").ShouldBe(10);

        [Test]
        public void Test()
        {
            var str = "hello,小明";
            var bs = System.Text.Encoding.UTF8.GetBytes(str);
            //base64urlsafe对字节数组进行编码
            var str2 = Base64UrlSafe.Encode(bs);
            //base64urlsafe将字符串解码为字符串
            var bs2 = Base64UrlSafe.Decode(str2);
            var str3 = System.Text.Encoding.UTF8.GetString(bs2);



        }
    }
}
