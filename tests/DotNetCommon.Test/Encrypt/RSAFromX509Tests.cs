﻿using DotNetCommon.Encrypt;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Tests.Encrypt
{
    [TestFixture]
    public class RSAFromX509Tests
    {
        string privateKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Encrypt/user-rsa.pfx");
        string publicKey = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Encrypt/public-rsa.cer");
        string pwd = "123456";

        [Test]
        public void EncryptTest()
        {
            var rsa = new RSAFromX509(publicKey, privateKey, pwd);
            var encode_str = rsa.Encrypt("https://www.baidu.com");
            encode_str.ShouldNotBeNullOrWhiteSpace();
            var decode_str = rsa.Decrypt(encode_str);
            decode_str.ShouldNotBeNullOrWhiteSpace();
            Assert.IsTrue("https://www.baidu.com" == decode_str);

            encode_str = rsa.Encrypt(null);
            encode_str.ShouldBeNull();

            encode_str = rsa.Encrypt("");
            encode_str.ShouldBeEmpty();

            encode_str = rsa.Encrypt("  ");
            encode_str.ShouldNotBeNullOrWhiteSpace();
        }

        [Test]
        public void SignTest()
        {
            var rsa = new RSAFromX509(publicKey, privateKey, pwd);
            var encode_str = rsa.Sign("https://www.baidu.com");
            encode_str.ShouldNotBeNullOrWhiteSpace();
            var res = rsa.Verify("https://www.baidu.com", encode_str);
            Assert.IsTrue(res);

            encode_str = rsa.Sign(null);
            encode_str.ShouldBeNull();

            encode_str = rsa.Sign("");
            encode_str.ShouldBeEmpty();

            encode_str = rsa.Sign("  ");
            encode_str.ShouldNotBeNullOrWhiteSpace();

        }

        /// <summary>
        /// 使用公钥加密，使用私钥解密
        /// </summary>
        [Test]
        public void EncryptTest2()
        {
            var encode_str = RSAFromX509.Encrypt(publicKey, "https://www.baidu.com");
            encode_str.ShouldNotBeNullOrWhiteSpace();
            var decode_str = RSAFromX509.Decrypt(privateKey, pwd, encode_str);
            Assert.IsTrue("https://www.baidu.com" == decode_str);

            encode_str = RSAFromX509.Sign(privateKey, pwd, null);
            encode_str.ShouldBeNull();

            encode_str = RSAFromX509.Sign(privateKey, pwd, "");
            encode_str.ShouldBeEmpty();

            encode_str = RSAFromX509.Sign(privateKey, pwd, "  ");
            encode_str.ShouldNotBeNullOrWhiteSpace();
        }

        /// <summary>
        /// 使用私钥签名，使用公钥验签
        /// </summary>
        [Test]
        public void SignTest2()
        {
            var encode_str = RSAFromX509.Sign(privateKey, pwd, "https://www.baidu.com");
            var res = RSAFromX509.Verify(publicKey, "https://www.baidu.com", encode_str);
            Assert.IsTrue(res);

            encode_str = RSAFromX509.Sign(privateKey, pwd, null);
            encode_str.ShouldBeNull();
            res = RSAFromX509.Verify(publicKey, null, encode_str);
            Assert.IsTrue(res);

            encode_str = RSAFromX509.Sign(privateKey, pwd, "");
            encode_str.ShouldBeEmpty();
            res = RSAFromX509.Verify(publicKey, "", encode_str);
            Assert.IsTrue(res);

            encode_str = RSAFromX509.Sign(privateKey, pwd, "  ");
            encode_str.ShouldNotBeNullOrWhiteSpace();
            res = RSAFromX509.Verify(publicKey, "  ", encode_str);
            Assert.IsTrue(res);
        }
    }
}
