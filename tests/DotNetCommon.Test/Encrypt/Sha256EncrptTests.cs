﻿using DotNetCommon.Encrypt;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Test.Encrypt
{
    [TestFixture]
    public class Sha256EncrptTests
    {
        [Test]
        public void EncodeTest()
        {
            var res = Sha256.SHA256Hash("abcdefghijklmnopqrstuvwxyz");
            res.ShouldNotBeNullOrWhiteSpace();
            var res2 = Sha256.SHA256Hash("abcdefghijklmnopqrstuvwxy");
            res.ShouldNotBeNullOrWhiteSpace();
            Assert.AreNotEqual(res, res2);

            res = Sha256.SHA256Hash("");
            res.ShouldBe("");

            string str = null;
            res = Sha256.SHA256Hash(str);
            res.ShouldBe(null);
        }

        [Test]
        public void EncodeStreamTest()
        {
            var memo1 = new MemoryStream(Encoding.UTF8.GetBytes("小明你好1"));
            var memo2 = new MemoryStream(Encoding.UTF8.GetBytes("小明你好2"));
            var res = Sha256.SHA256Hash(memo1);
            var res2 = Sha256.SHA256Hash(memo2);
            Assert.AreNotEqual(res, res2);

            memo1 = null;
            Should.Throw<Exception>(() => Sha256.SHA256Hash(memo1));
        }

        [Test]
        public void EncodeFileTest()
        {
            var file1 = Path.GetFullPath("Encrypt/public-rsa.cer");
            var file2 = Path.GetFullPath("Encrypt/user-rsa.pfx");
            var res = Sha256.SHA256HashFile(file1);
            res.ShouldNotBeNullOrWhiteSpace();
            var res2 = Sha256.SHA256HashFile(file2);
            res2.ShouldNotBeNullOrWhiteSpace();
            Assert.AreNotEqual(res, res2);

            res = Sha256.SHA256HashFile(null);
            res.ShouldBeNull();

            res = Sha256.SHA256HashFile("");
            res.ShouldBeEmpty();

            res = Sha256.SHA256HashFile("   ");
            res.ShouldBe("   ");
        }
    }
}
