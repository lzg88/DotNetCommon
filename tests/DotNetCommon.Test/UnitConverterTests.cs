﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Shouldly;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class UnitConverterTests
    {
        [Test]
        public void Test()
        {
            //输出: 12 B
            var str = UnitConverter.Humanize(12d);
            str.ShouldBe("12 B");
            //输出: 0 B
            str = UnitConverter.Humanize(0d);
            str.ShouldBe("0 B");
            //输出: 1 KB
            str = UnitConverter.Humanize(1024.23d);
            str.ShouldBe("1 KB");
            //输出: 53.01 GB
            str = UnitConverter.Humanize(1024.23 * 1024 * 1024 * 53d);
            str.ShouldBe("53.01 GB");
            //B、KB、MB、GB、TB相互转换
            //GB转MB,输出: 54282.24 MB
            str = UnitConverter.GigaBytesToMegaBytes(53.01d) + " MB";
            str.ShouldBe("54282.24 MB");

        }

        [Test]
        public void Test2()
        {
            //壹拾贰圆叁角贰分
            var str = UnitConverter.MoneyToUpper(12.32.ToString());
            str.ShouldBe("壹拾贰圆叁角贰分");

            str = UnitConverter.MoneyToUpper(12.323.ToString());
            str.ShouldBe("壹拾贰圆叁角贰分");
            str = UnitConverter.MoneyToUpper(100.ToString());
            str.ShouldBe("壹佰圆整");
            str = UnitConverter.MoneyToUpper((-10.2).ToString());
            str.ShouldBe("负壹拾圆贰角");
            str = UnitConverter.MoneyToUpper((-10.02).ToString());
            str.ShouldBe("负壹拾圆贰分");
            str = UnitConverter.MoneyToUpper((-10).ToString());
            str.ShouldBe("负壹拾圆整");

            str = UnitConverter.MoneyToUpper(1953_0000_0000.ToString());
            str.ShouldBe("壹仟玖佰伍拾叁亿圆整");
            str = UnitConverter.MoneyToUpper(1_0000_0000_0000.ToString());
            str.ShouldBe("壹万亿圆整");

            str = UnitConverter.MoneyToUpper(9999_0000_0000_0000.ToString());
            str.ShouldBe("玖仟玖佰玖拾玖万亿圆整");

            str = UnitConverter.MoneyToUpper("");
            str.ShouldBeEmpty();

        }
    }
}
