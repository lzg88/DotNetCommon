﻿//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Text;
//using helper = DotNetCommon.ZipHelper;

//namespace DotNetCommon.Test
//{
//    [TestFixture]
//    public class ZipHelperTests
//    {
//        private string rootPath = Path.Combine(Path.GetFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)), "ZipHelperTests");
//        private string testfolderPath = "";

//        [SetUp]
//        public void SetUp()
//        {
//            //准备文件夹和文件
//            if (Directory.Exists(rootPath))
//            {
//                Directory.Delete(rootPath, true);
//            }
//            Directory.CreateDirectory(rootPath);

//            //准备目录
//            /*
//            C:\Users\Administrator\Documents\ZipHelperTests
//                \testfolder\
//                    testsubfolder\
//                        testsubfolder-suba.txt "我是testsubfolder-suba.txt"
//                    testemptysubfolder\                        
//                    testfolder-a.txt "我是testfolder-a.txt"
//                    testfolder-中文B.txt "testfolder-中文B.txt"
//            */
//            testfolderPath = Path.Combine(rootPath, "testfolder");
//            Directory.CreateDirectory(testfolderPath);
//            File.AppendAllText(Path.Combine(testfolderPath, "testfolder-a.txt"), "我是testfolder-a.txt");
//            File.AppendAllText(Path.Combine(testfolderPath, "testfolder-中文B.txt"), "testfolder-中文B.txt");

//            Directory.CreateDirectory(Path.Combine(testfolderPath, "testsubfolder"));
//            Directory.CreateDirectory(Path.Combine(testfolderPath, "testemptysubfolder"));
//            File.AppendAllText(Path.Combine(testfolderPath, "testsubfolder", "testsubfolder-suba.txt"), "我是testsubfolder-suba.txt");
//        }

//        [TearDown]
//        public void TearDown()
//        {
//            if (Directory.Exists(rootPath))
//            {
//                Directory.Delete(rootPath, true);
//            }
//        }

//        /// <summary>
//        /// 压缩文件夹
//        /// </summary>
//        [Test]
//        public void TestZipFolder()
//        {
//            var zipfolderTarget = Path.Combine(rootPath, "testzip.zip");
//            Assert.DoesNotThrow(() => helper.ZipFolder(zipfolderTarget, testfolderPath));
//        }

//        /// <summary>
//        /// 压缩单个文件
//        /// </summary>
//        [Test]
//        public void TestZipSingleFile()
//        {
//            var zipSingleFileTarget = Path.Combine(rootPath, "zipSingleFileTarget.zip");
//            Assert.DoesNotThrow(() => helper.ZipFile(zipSingleFileTarget, Path.Combine(testfolderPath, "testfolder-中文B.txt")));
//        }

//        /// <summary>
//        /// 压缩多个文件
//        /// </summary>
//        [Test]
//        public void TestZipMultiFile()
//        {
//            var zipMultiFileTarget = Path.Combine(rootPath, "zipMultiFileTarget.zip");
//            Assert.DoesNotThrow(() => helper.ZipFile(zipMultiFileTarget, Path.Combine(testfolderPath, "testfolder-中文B.txt"), Path.Combine(testfolderPath, "testsubfolder", "testsubfolder-suba.txt")));
//        }

//        /// <summary>
//        /// 压缩多个文件并且重命名
//        /// </summary>
//        [Test]
//        public void TestZipMultiFileWithRename()
//        {
//            var zipMultiFileWithRename = Path.Combine(rootPath, "zipMultiFileWithRename.zip");
//            Assert.DoesNotThrow(() => helper.ZipFile(zipMultiFileWithRename,
//                (Path.Combine(testfolderPath, "testfolder-中文B.txt"), "重命名1.txt"),
//                (Path.Combine(testfolderPath, "testsubfolder", "testsubfolder-suba.txt"), "重命名2.txt")
//                ));
//        }

//        /// <summary>
//        /// 压缩流
//        /// </summary>
//        [Test]
//        public void TestZipStream()
//        {
//            using var memoStream = new MemoryStream(Encoding.UTF8.GetBytes("abc测试文件1"));
//            memoStream.ReadByte();
//            using var memoStream2 = new MemoryStream(Encoding.UTF8.GetBytes("def测试文件1"));
//            memoStream2.ReadByte();
//            memoStream2.ReadByte();
//            var zipStreamPath = Path.Combine(rootPath, "zipStream.zip");
//            using var fs = new FileStream(zipStreamPath, FileMode.CreateNew);
//            ZipHelper.ZipFile(fs, (memoStream, "目录1/文件1.txt"), (memoStream2, "文件2.txt"));
//            fs.Close();
//            Assert.IsTrue(File.Exists(zipStreamPath));
//            Assert.IsTrue(new FileInfo(zipStreamPath).Length > 0);
//        }
//    }
//}
