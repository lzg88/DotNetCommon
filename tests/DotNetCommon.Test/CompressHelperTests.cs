﻿using DotNetCommon.Compress;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;

namespace DotNetCommon.Test
{
    [TestFixture]
    internal class CompressHelperTests
    {
        private string rootPath = Path.Combine(Path.GetFullPath(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)), "DotNetCommon.Compress.TestFiles");
        private string testfolderPath = "";

        [SetUp]
        public void SetUp()
        {
            //准备文件夹和文件
            if (Directory.Exists(rootPath)) Directory.Delete(rootPath, true);
            Directory.CreateDirectory(rootPath);

            //准备目录
            /*
            C:\Users\Administrator\Documents\DotNetCommon.Compress.TestFiles
                \testfolder\
                    testsubfolder\
                        testsubfolder-suba.txt "我是testsubfolder-suba.txt"
                    testemptysubfolder\                        
                    testsubfolder2\
                        testsubfolder-suba.txt "我是testsubfolder-suba.txt"
                    testfolder-a.txt "我是testfolder-a.txt"
                    testfolder-中文B.txt "testfolder-中文B.txt"
            */
            testfolderPath = Path.Combine(rootPath, "testfolder");
            Directory.CreateDirectory(testfolderPath);
            File.AppendAllText(Path.Combine(testfolderPath, "testfolder-a.txt"), "我是testfolder-a.txt");
            File.AppendAllText(Path.Combine(testfolderPath, "testfolder-中文B.txt"), "testfolder-中文B.txt");

            Directory.CreateDirectory(Path.Combine(testfolderPath, "testsubfolder"));
            File.AppendAllText(Path.Combine(testfolderPath, "testsubfolder", "testsubfolder-suba.txt"), "我是testsubfolder-suba.txt");
            Directory.CreateDirectory(Path.Combine(testfolderPath, "testsubfolder2"));
            File.AppendAllText(Path.Combine(testfolderPath, "testsubfolder2", "testsubfolder-suba.txt"), "我是testsubfolder-suba.txt");
            Directory.CreateDirectory(Path.Combine(testfolderPath, "testemptysubfolder"));
        }

        public void UnCompressTests()
        {
            var destDirPath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destDirPath)) File.Delete(destDirPath);
            //7z
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder.7z"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            Directory.Delete(destDirPath, true);
            //rar
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder.rar"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            Directory.Delete(destDirPath, true);
            //bz2
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder.tar.bz2"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            Directory.Delete(destDirPath, true);
            //gz
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder.tar.gz"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            Directory.Delete(destDirPath, true);
            //zip
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder.zip"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destDirPath, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            Directory.Delete(destDirPath, true);

            //单文件压缩
            //gz
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder-中文B.txt.gz"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            Directory.Delete(destDirPath, true);
            //bz2
            CompressHelper.UnCompress(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testfolder-中文B.txt.bz2"), destDirPath);
            File.ReadAllText(Path.Combine(destDirPath, "testfolder-中文B.txt")).ShouldBe("testfolder-中文B");
            Directory.Delete(destDirPath, true);
        }

        public void CompressStreamTests()
        {
            var destFilePath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destFilePath)) File.Delete(destFilePath);
            using (var destFs = new FileStream(destFilePath, FileMode.CreateNew))
            using (var stream1 = new FileStream(Path.Combine(rootPath, "testfolder/testfolder-中文B.txt"), FileMode.Open))
            using (var stream2 = new FileStream(Path.Combine(rootPath, "testfolder/testsubfolder/testsubfolder-suba.txt"), FileMode.Open))
            using (var stream3 = new FileStream(Path.Combine(rootPath, "testfolder/testfolder-a.txt"), FileMode.Open))
            {
                CompressHelper.CompressZip(destFs, new Dictionary<string, Stream>
                {
                    {"testfolder-中文B222.txt",stream1 },
                    {"testsubfolder/testsubfolder-suba222.txt",stream2 },
                    {"testfolder-a.txt",stream3 }
                });
            }
            File.Exists(destFilePath).ShouldBe(true);
            //解压
            var destUnZipFolder = Path.Combine(rootPath, "testcompress_stream.unzip");
            CompressHelper.UnCompress(destFilePath, destUnZipFolder);

            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-中文B222.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testsubfolder/testsubfolder-suba222.txt")).ShouldBe("我是testsubfolder-suba.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-a.txt")).ShouldBe("我是testfolder-a.txt");
        }

        public void CompressFilesTests()
        {
            var destFilePath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destFilePath)) File.Delete(destFilePath);
            CompressHelper.CompressZip(destFilePath, new Dictionary<string, string>
            {
                {"testfolder-中文B222.txt",Path.Combine(rootPath, "testfolder/testfolder-中文B.txt") },
                {"testsubfolder/testsubfolder-suba222.txt",Path.Combine(rootPath, "testfolder/testsubfolder/testsubfolder-suba.txt") },
                {"testfolder-a.txt",Path.Combine(rootPath, "testfolder/testfolder-a.txt") }
            });
            File.Exists(destFilePath).ShouldBe(true);
            //解压
            var destUnZipFolder = Path.Combine(rootPath, "testcompress_stream.unzip");
            CompressHelper.UnCompress(destFilePath, destUnZipFolder);

            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-中文B222.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testsubfolder/testsubfolder-suba222.txt")).ShouldBe("我是testsubfolder-suba.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-a.txt")).ShouldBe("我是testfolder-a.txt");
        }

        public void CompressZipFolderTests()
        {
            var destFilePath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destFilePath)) File.Delete(destFilePath);
            CompressHelper.CompressZipFolder(destFilePath, new Dictionary<string, string>
            {
                {"testsubfolder1",Path.Combine(rootPath, "testfolder/testsubfolder") },
                {"testsubfolder2",Path.Combine(rootPath, "testfolder/testsubfolder2") },
            });
            File.Exists(destFilePath).ShouldBe(true);
            //解压
            var destUnZipFolder = Path.Combine(rootPath, "testcompress_stream.unzip");
            CompressHelper.UnCompress(destFilePath, destUnZipFolder);

            File.ReadAllText(Path.Combine(destUnZipFolder, "testsubfolder1/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testsubfolder2/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
        }

        public void CompressZipFolderTests2()
        {
            var destFilePath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destFilePath)) File.Delete(destFilePath);
            CompressHelper.CompressZipFolder(destFilePath, Path.Combine(rootPath, "testfolder"));
            File.Exists(destFilePath).ShouldBe(true);
            //解压
            var destUnZipFolder = Path.Combine(rootPath, "testcompress_stream.unzip");
            CompressHelper.UnCompress(destFilePath, destUnZipFolder);
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder/testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder/testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder/testfolder-a.txt")).ShouldBe("我是testfolder-a.txt");
        }

        public void CompressZipFolderContentTests()
        {
            var destFilePath = Path.Combine(rootPath, "testcompress_stream.zip");
            if (File.Exists(destFilePath)) File.Delete(destFilePath);
            CompressHelper.CompressZipFolderContent(destFilePath, Path.Combine(rootPath, "testfolder"));
            File.Exists(destFilePath).ShouldBe(true);
            //解压
            var destUnZipFolder = Path.Combine(rootPath, "testcompress_stream.unzip");
            CompressHelper.UnCompress(destFilePath, destUnZipFolder);
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-中文B.txt")).ShouldBe("testfolder-中文B.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testsubfolder/testsubfolder-suba.txt")).ShouldBe("我是testsubfolder-suba.txt");
            File.ReadAllText(Path.Combine(destUnZipFolder, "testfolder-a.txt")).ShouldBe("我是testfolder-a.txt");
        }

        [Test]
        public void TestFlow()
        {
            CompressZipFolderContentTests();
            CompressZipFolderTests2();
            CompressZipFolderTests();
            CompressFilesTests();
            CompressStreamTests();
            UnCompressTests();
        }
    }
}
