﻿using DotNetCommon.Validate;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class ValidWithDisplayNameTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Name = "小明",
                Book = new Book()
                {
                    Id = 2,
                    Name = "语文"
                }
            };
            /*
            'Id' 不能等于 '1'!
            '姓名' 不能等于 '小明'!
            '书籍.主键' 不能等于 '2'!
            '书籍.名称' 不能等于 '语文'!
            */
            var errorMessage = ValidateModelHelper.ValidErrorMessage(person, Person.ValidateTest);
            errorMessage.ShouldNotBeNullOrWhiteSpace();
        }

        #region 特性实体
        [DisplayName("人员")]
        public class Person
        {
            public int Id { get; set; }

            [DisplayName("姓名")]
            public string Name { get; set; }

            [DisplayName("书籍")]
            public Book Book { get; set; }

            public static void ValidateTest(ValidateContext<Person> ctx)
            {
                ctx.RuleFor(i => i.Id).MustNotEqualTo(1);
                ctx.RuleFor(i => i.Name).MustNotEqualTo("小明");
                var ctxBook = ctx.RuleFor(i => i.Book).MustNotNull();
                ctxBook.RuleFor(i => i.Id).MustNotEqualTo(2);
                ctxBook.RuleFor(i => i.Name).MustNotEqualTo("语文");
            }
        }

        [DisplayName("书籍实体")]
        public class Book
        {
            [DisplayName("主键")]
            public int Id { get; set; }

            [DisplayName("名称")]
            public string Name { get; set; }
        }
        #endregion
    }
}
