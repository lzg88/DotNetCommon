﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using Shouldly;
using DotNetCommon.Data;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class ValidTests
    {
        [Test]
        public void UseValidateActionTest()
        {
            var person = new Person()
            {
                Id = 1,
                Name = "小王"
            };

            //'Book' 不能为空!
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Book).MustNotNull();
            });
            msg.ShouldNotBeNullOrEmpty();

            person.Book = new Book()
            {
                Id = 2,
                Name = "数学"
            };

            //'Book.Id' 必须小于 '2'!
            //'Book.Name' 必须为空!
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
             {
                 ctx.RuleFor(i => i.Book).UseValidateAction(cbook =>
                 {
                     cbook.RuleFor(i => i.Id).MustLessThan(2);
                     cbook.RuleFor(i => i.Name).MustBeNull();
                 });
             });
            msg.ShouldNotBeNullOrEmpty();
            person.Book.FromShop = new BookShop()
            {
                Id = 0
            };
            //'Book.Id' 必须小于 '2'!
            //'Book.Name' 必须为空!
            //'Book.FromShop.Id' 必须大于0!
            //'Book.FromShop.Name' 不能为空!
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Book).UseValidateAction(cbook =>
                {
                    cbook.RuleFor(i => i.Id).MustLessThan(2);
                    cbook.RuleFor(i => i.Name).MustBeNull();
                    cbook.RuleFor(i => i.FromShop).UseValidateAction(cshop =>
                    {
                        cshop.RuleFor(i => i.Id).MustGreaterThanZero();
                        cshop.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace();
                    });
                });
            });
            msg.ShouldNotBeNullOrEmpty();
        }

        [Test]
        public void CustomeValidateTest()
        {
            var persons = new List<Person>();
            var msg = ValidateModelHelper.ValidErrorMessage(persons, ctx =>
            {
                ctx.CustomeValidate(p => p.Count == 2, "数量必须为2!");
            });
            msg.ShouldNotBeEmpty();

            var person = new Person();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Name).CustomeValidate(name => name == "小王");
            });
            msg.ShouldNotBeEmpty();
            person.Book = new Book();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Book).RuleFor(i => i.Name).CustomeValidate(name => name == "小王");
            });
            msg.ShouldNotBeEmpty();
        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Book Book { get; set; }
        }

        public class Book
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public BookShop FromShop { set; get; }
        }

        public class BookShop
        {
            public int Id { get; set; }

            [System.ComponentModel.Description("名字")]
            public string Name { get; set; }
        }

        #region actual use
        public Result<bool> AddStudent(Student student)
        {
            var res = ValidateModelHelper.ValidResult(student, Student.ValidAdd);
            if (!res.Success) return res;
            //...新增操作
            return Result.Ok(true);
        }

        [Test]
        public void Test()
        {
            var student = new Student()
            {
                Id = 5,
                Age = -18,
                Birth = DateTime.Parse("1220-01-01"),
                Email = "sdhiahsd.com",
                IdCard = "415551515",
                Phone = "4521"
            };
            var res = AddStudent(student);
            res.Success.ShouldBeFalse();
            res.Message.ShouldNotBeNullOrWhiteSpace();
        }


        public class Student
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? Age { get; set; }
            public DateTime? Birth { get; set; }
            public string IdCard { get; set; }
            public string Addr { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }

            /// <summary>
            /// 校验新增Student
            /// </summary>
            /// <param name="ctx"></param>
            public static void ValidAdd(ValidateContext<Student> ctx)
            {
                ctx.MustNotNull().IfFailThenExit();
                ctx.RuleFor(i => i.Id).MustEqualTo(0);
                ctx.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace().MustLengthInRange(1, 4);
                ctx.RuleFor(i => i.Age).When(i => i != null, c => c.MustGreaterThanOrEuqalTo(0));
                ctx.RuleFor(i => i.Birth).When(i => i != null, c => c.MustGreaterThanOrEuqalTo(DateTime.Parse("1800-01-01")));
                ctx.RuleFor(i => i.IdCard).MustIdCard();
                ctx.RuleFor(i => i.Phone).When(i => i != null, c => c.MustCellPhone());
                ctx.RuleFor(i => i.Email).When(i => i != null, c => c.MustEmailAddress());
            }
        }

        #endregion
    }
}
