﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using Shouldly;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class ListValidTests
    {
        [Test]
        public void NotNullOrEmptyTest()
        {
            //PersonList
            var person = new PersonList()
            {
                Id = 1,
                Name = "小王",
                Books = null
            };
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
              {
                  ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
              });
            msg.ShouldNotBeNullOrWhiteSpace();

            person.Books = new List<Book>();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldNotBeNullOrWhiteSpace();

            person.Books = new List<Book>() { new Book() };
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldBeNullOrWhiteSpace();

            //PersonCollection
            var personCollection = new PersonCollection()
            {
                Id = 1,
                Name = "小王",
                Books = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(personCollection, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldNotBeNullOrWhiteSpace();

            personCollection.Books = new List<Book>();
            msg = ValidateModelHelper.ValidErrorMessage(personCollection, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldNotBeNullOrWhiteSpace();

            personCollection.Books = new List<Book>() { new Book() };
            msg = ValidateModelHelper.ValidErrorMessage(personCollection, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldBeNullOrWhiteSpace();

            //PersonEnumerable
            var personEnumerable = new PersonEnumerable()
            {
                Id = 1,
                Name = "小王",
                Books = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(personEnumerable, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldNotBeNullOrWhiteSpace();

            personEnumerable.Books = new List<Book>();
            msg = ValidateModelHelper.ValidErrorMessage(personEnumerable, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldNotBeNullOrWhiteSpace();

            personEnumerable.Books = new List<Book>() { new Book() };
            msg = ValidateModelHelper.ValidErrorMessage(personEnumerable, ctx =>
            {
                ctx.RuleFor(i => i.Books).MustNotNullOrEmpty();
            });
            msg.ShouldBeNullOrWhiteSpace();
        }

        [Test]
        public void RuleForEachTest()
        {
            var person = new PersonList()
            {
                Id = 1,
                Name = "小王",
                Books = new List<Book>() {
                    new Book()
                    {
                        Id=1,
                        Name="数学"
                    }
                }
            };
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
              {
                  ctx.RuleFor(i => i.Books).MustNotNullOrEmpty().IfFailThenExit();
                  ctx.RuleFor(i => i.Books).RuleForEach(cbook =>
                  {
                      cbook.RuleFor(i => i.Id).MustGreaterThanZero();
                      cbook.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace();
                  });
              });
            msg.ShouldBeNullOrEmpty();

            person.Books.Add(null);
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Books).RuleForEach(cbook =>
                {
                    cbook.MustNotNull().IfFailThenExit();
                });
            });
            msg.ShouldNotBeNullOrEmpty();

            person.Books.RemoveAt(person.Books.Count - 1);
            person.Books.Add(new Book()
            {
                Id = -1,
                Name = ""
            });
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Books).RuleForEach(cbook =>
                {
                    cbook.MustNotNull().IfFailThenExit();
                    cbook.RuleFor(i => i.Id).MustGreaterThanZero();
                    cbook.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace();
                });
            });
            msg.ShouldNotBeNullOrEmpty();

        }

        public class PersonList
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public List<Book> Books { get; set; }
        }

        public class PersonCollection
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public ICollection<Book> Books { get; set; }
        }

        public class PersonEnumerable
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public IEnumerable<Book> Books { get; set; }
        }

        public class Book
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
