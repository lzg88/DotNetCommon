﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using Shouldly;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class DictionaryExtensionTests
    {
        [Test]
        public void TestFluent()
        {
            var dic = new Dictionary<string, string>();
            dic.SetFluent("1", "小明").SetFluent("2", "小王").SetFluent("3", "小周");
            dic.ToJson().ShouldBe("{\"1\":\"小明\",\"2\":\"小王\",\"3\":\"小周\"}");
            dic.RemoveFluent("1").RemoveFluent("2");
            dic.ToJson().ShouldBe("{\"3\":\"小周\"}");
            dic.ClearFluent().ToJson().ShouldBe("{}");

            IDictionary<string, string> dic2 = new Dictionary<string, string>();
            dic2.SetFluent("1", "小明").SetFluent("2", "小王").SetFluent("3", "小周");
            dic2.ToJson().ShouldBe("{\"1\":\"小明\",\"2\":\"小王\",\"3\":\"小周\"}");
            dic2.RemoveFluent("1").RemoveFluent("2");
            dic2.ToJson().ShouldBe("{\"3\":\"小周\"}");
            dic2.ClearFluent().ToJson().ShouldBe("{}");
        }
    }
}
