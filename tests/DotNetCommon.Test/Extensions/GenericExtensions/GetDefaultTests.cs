﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test.Extensions.GenericExtensions
{
    [TestFixture]
    internal class GetDefaultTests
    {
        [Test]
        public void Test()
        {
            typeof(int).GetDefault().ShouldBe(0);
            typeof(int?).GetDefault().ShouldBe(null);

            typeof(GetDefaultTests).GetDefault().ShouldBe(null);
            typeof(string).GetDefault().ShouldBe(null);

            typeof(double).GetDefault().ShouldBe(default(double));

            typeof(DateTime).GetDefault().ShouldBe(default(DateTime));
        }
    }
}
