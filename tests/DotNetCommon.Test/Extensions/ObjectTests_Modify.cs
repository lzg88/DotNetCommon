﻿using DotNetCommon.Data;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class ObjectTests_Modify
    {
        [Test]
        public void Test()
        {
            var res = getData();
            res.Data.Students.Count.ShouldBeGreaterThan(1);
            res.Data.Students[0].CellPhone.ShouldNotBe("****");
            res.Data.Students[1].CellPhone.ShouldNotBe("****");
            res.Data.OtherStudents.Length.ShouldBeGreaterThan(1);
            res.Data.OtherStudents[0].CellPhone.ShouldNotBe("****");

            res = res.Modify("CellPhone", old => "****");

            res.Data.Students.Count.ShouldBeGreaterThan(1);
            res.Data.Students[0].CellPhone.ShouldBe("****");
            res.Data.Students[1].CellPhone.ShouldBe("****");
            res.Data.OtherStudents.Length.ShouldBeGreaterThan(1);
            res.Data.OtherStudents[0].CellPhone.ShouldBe("****");

            res = res.Modify("Students", old => new List<Student>());
            res.Data.Students.Count.ShouldBe(0);
        }

        private Result<Person> getData()
        {
            return Result.Ok<Person>(new Person()
            {
                Id = 1,
                Name = "王二小",
                IdentityCard = "412314200001021234",
                Students = new List<Student>()
                {
                    new Student()
                    {
                        Id=2,
                        Name="张飞",
                        CellPhone="18742312541"
                    },
                    new Student()
                    {
                        Id=3,
                        Name="东方不败",
                        CellPhone="15012345678"
                    }
                },
                OtherStudents = new Student[3]
                {
                    new Student()
                    {
                        Id=4,
                        Name="张三",
                        CellPhone="16554321234"
                    },
                    new Student()
                    {
                        Id=5,
                        Name=null,
                        CellPhone="12345678901"
                    },
                    null
                }
            });
        }
    }

    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IdentityCard { get; set; }

        public List<Student> Students { get; set; }
        public Student[] OtherStudents { set; get; }
    }

    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CellPhone { get; set; }
    }
}
