﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public sealed class EnumExtensionsTests
    {
        [Test]
        public void EnumToDescriptionTest()
        {
            //普通枚举无特性
            EnumTest test = EnumTest.B;
            test.ToDescription().ShouldBe("B");

            //普通枚举有特性
            var test2 = EnumTest2.B;
            test2.ToDescription().ShouldBe("测试描述");
            test2 = EnumTest2.C;
            test2.ToDescription().ShouldBe("C");

            //位枚举无特性
            var test3 = EnumTest3.B | EnumTest3.C;
            test3.ToDescription().ShouldBe("B, C");

            //位枚举有特性
            var test4 = EnumTest4.B | EnumTest4.C;
            test4.ToDescription().ShouldBe("这是B, 这是C");

            //位枚举混合特性
            var test5 = EnumTest4.A | EnumTest4.B | EnumTest4.C;
            test5.ToDescription().ShouldBe("A, 这是B, 这是C");
        }

        /// <summary>
        /// ToCodeKeyDescriptionList
        /// </summary>
        [Test]
        public void ToCodeKeyDescriptionListTest()
        {
            EnumTest2 test = EnumTest2.B;
            var list = test.ToCodeKeyDescriptionList();
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].key.ShouldBe("A");
            list[0].desc.ShouldBe("A");

            list[1].code.ShouldBe(1);
            list[1].key.ShouldBe("B");
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].key.ShouldBe("C");
            list[2].desc.ShouldBe("C");

            list = test.ToCodeKeyDescriptionList(false);
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].key.ShouldBe("A");
            list[0].desc.ShouldBe("");

            list[1].code.ShouldBe(1);
            list[1].key.ShouldBe("B");
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].key.ShouldBe("C");
            list[2].desc.ShouldBe("");
        }

        /// <summary>
        /// ToCodeDescriptionList
        /// </summary>
        [Test]
        public void ToCodeDescriptionListTest()
        {
            EnumTest2 test = EnumTest2.B;
            var list = test.ToCodeDescriptionList();
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].desc.ShouldBe("A");

            list[1].code.ShouldBe(1);
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].desc.ShouldBe("C");

            list = test.ToCodeDescriptionList(false);
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].desc.ShouldBe("");

            list[1].code.ShouldBe(1);
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].desc.ShouldBe("");
        }

        /// <summary>
        /// ToCodeDescriptionList
        /// </summary>
        [Test]
        public void ToCodeKeyDescriptionReferenceClassListTest()
        {
            EnumTest2 test = EnumTest2.B;
            var list = test.ToCodeKeyDescriptionReferenceClassList();
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].desc.ShouldBe("A");
            list[0].refer.ShouldBe(typeof(List<int>));

            list[1].code.ShouldBe(1);
            list[1].desc.ShouldBe("测试描述");
            list[1].refer.ShouldBe(typeof(Stack<int>));

            list[2].code.ShouldBe(2);
            list[2].desc.ShouldBe("C");
            list[2].refer.ShouldBe(typeof(Queue<int>));

            list = test.ToCodeKeyDescriptionReferenceClassList(false);
            list.Count.ShouldBe(3);
            list[0].code.ShouldBe(0);
            list[0].desc.ShouldBe("");

            list[1].code.ShouldBe(1);
            list[1].desc.ShouldBe("测试描述");

            list[2].code.ShouldBe(2);
            list[2].desc.ShouldBe("");
        }

        [Test]
        public void ContainsTest()
        {
            // EnumTest3.A=1,EnumTest3.B=2,EnumTest3.C=4
            var test = EnumTest3.A | EnumTest3.C;
            test.Contains(EnumTest3.A).ShouldBeTrue();
            test.Contains(EnumTest3.C).ShouldBeTrue();
            test.Contains(EnumTest3.B).ShouldBeFalse();

            test.Contains(EnumTest3.A | EnumTest3.B).ShouldBeFalse();
            test.Contains(EnumTest3.A | EnumTest3.C).ShouldBeTrue();


            test.ContainsAny(EnumTest3.A).ShouldBeTrue();
            test.ContainsAny(EnumTest3.C).ShouldBeTrue();
            test.ContainsAny(EnumTest3.B).ShouldBeFalse();

            test.ContainsAny(EnumTest3.A | EnumTest3.C).ShouldBeTrue();
            test.ContainsAny(EnumTest3.A | EnumTest3.B).ShouldBeTrue();

        }

        [Test]
        public void ToReferTest()
        {
            var test = EnumTest2.B;
            var type = test.ToReferType();
            type.ShouldBe(typeof(Stack<int>));

            var test2 = EnumTest3.B;
            var types = test2.ToReferTypes();
            types.ShouldBe(new List<Type>
            {
                typeof(List)
            });

            var test3 = EnumTest3.A | EnumTest3.B;
            test3.ToReferTypes().ShouldBe(new List<Type>()
            {
                typeof(STAThreadAttribute),
                typeof(List)
            });

            test3 = EnumTest3.A | EnumTest3.B | EnumTest3.C;
            test3.ToReferTypes().ShouldBe(new List<Type>()
            {
                typeof(STAThreadAttribute),
                typeof(List),
                null
            });

        }

        [Test]
        public void TestToIntList()
        {
            //复合枚举
            var obj = EnumTest3.A | EnumTest3.C;
            var list = obj.ToIntList();
            list.ShouldBe(new List<int>() { 1, 4 });

            obj = EnumTest3.C;
            list = obj.ToIntList();
            list.ShouldBe(new List<int>() { 4 });

            //非复合枚举
            EnumTest.C.ToIntList().ShouldBe(new List<int>() { 2 });
        }

        [Test]
        public void TestToEnumList()
        {
            //复合枚举
            var obj = EnumTest3.A | EnumTest3.C;
            var list = obj.ToEnumList();
            list.ShouldBe(new List<EnumTest3>() { EnumTest3.A, EnumTest3.C });

            obj = EnumTest3.C;
            list = obj.ToEnumList();
            list.ShouldBe(new List<EnumTest3>() { EnumTest3.C });

            //非复合枚举
            EnumTest.C.ToEnumList().ShouldBe(new List<EnumTest>() { EnumTest.C });
        }

        [Test]
        public void TestCombineFlagEnum()
        {
            //复合枚举
            var list = new List<EnumTest3> { EnumTest3.A, EnumTest3.B };
            EnumTest3 t3 = list.CombineFlagEnumOrNumber();
            Assert.IsTrue(t3 == (EnumTest3.A | EnumTest3.B));

            var list2 = new EnumTest3[0];
            EnumTest3 t4 = list2.CombineFlagEnumOrNumber();
            Assert.IsTrue(t4 == 0);

            var newList = new List<int> { 1, 2, 4, 8 };
            var t5 = newList.CombineFlagEnumOrNumber();
            Assert.IsTrue(t5 == 15);

            //非复合枚举（因为枚举本身也是一个数字,直接看成数字进行运算即可）
            var list3 = new List<EnumTest> { EnumTest.A, EnumTest.B };
            EnumTest t6 = list3.CombineFlagEnumOrNumber();
            Assert.IsTrue(t6 == (EnumTest.A | EnumTest.B));
        }
    }

    public enum EnumTest
    {
        A, B, C
    }
    public enum EnumTest2
    {
        [ReferenceClass(typeof(List<int>))]
        A,
        [ReferenceClass(typeof(Stack<int>))]
        [System.ComponentModel.Description("测试描述")]
        B,
        [ReferenceClass(typeof(Queue<int>))]
        C
    }

    [Flags]
    public enum EnumTest3
    {
        [ReferenceClass(typeof(STAThreadAttribute))]
        A = 1,
        [ReferenceClass(typeof(List))]
        B = 2,
        C = 4
    }

    [Flags]
    public enum EnumTest4
    {
        A = 1,
        [System.ComponentModel.Description("这是B")]
        B = 2,
        [System.ComponentModel.Description("这是C")]
        C = 4
    }
}
