﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

#region 模型 GetClassFullName & GetClassGenericFullName
namespace DemoNameSpace
{
    public class Outer
    {
        public class Middle
        {
            public class Inner { }
        }
    }

    public enum DemoEnum { }
    public struct DemoStruct { }
    public delegate void DemoDelegate();
    public interface DemoInterface { }

    namespace Generic
    {
        public class Outer<TOuter1, TOuter2>
        {
            public class Middle<TMiddle>
            {
                public class Inner<TInner1, TInner2> { }
            }
        }
    }
}
#endregion

#region 模型方法 GetMethodFullName & GetMethodGenericFullName
namespace DemoMethod
{
    public class NormalClass
    {
        //普通方法
        public static void NormalMethod1() { }
        public void NormalMethod2() { }
        public string NormalMethod3(int i, double? d) => throw new NotImplementedException();
        public List<string> NormalMethod4(Dictionary<string, byte> dic, List<NormalClass> classes) => throw new NotImplementedException();
        public (string name, int age) NormalMethod5(List<(string i, int a)> values, NormalClass normalClass, IDictionary<string, int> dic) => throw new NotImplementedException();

        //泛型方法
        public static string GenericMethod1<T>(T t, int age, NormalClass normalClass) => throw new NotImplementedException();
        public T2 GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
        public (T2, T3) GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
        public List<(T2, T1)> GenericMethod4<T1, T2>(T1 t1, T2 t2, (T1, T2) no, List<T1> li, Dictionary<T2, List<T2>> dic) => throw new NotImplementedException();
    }
    public class Outer
    {
        public class Middle
        {
            public class Inner
            {
                //普通方法
                public static void NormalMethod1() { }
                public void NormalMethod2() { }
                public string NormalMethod3(int i, double? d) => throw new NotImplementedException();
                public List<string> NormalMethod4(Dictionary<string, byte> dic, List<Inner> classes) => throw new NotImplementedException();
                public (string name, int age) NormalMethod5(List<(string i, int a)> values, Inner normalClass, IDictionary<string, int> dic) => throw new NotImplementedException();

                //泛型方法
                public static string GenericMethod1<T>(T t, int age, Inner normalClass) => throw new NotImplementedException();
                public T2 GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
                public (T2, T3) GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
                public List<(T2, T1)> GenericMethod4<T1, T2>(T1 t1, T2 t2, (T1, T2) no, List<T1> li, Dictionary<T2, List<T2>> dic) => throw new NotImplementedException();
            }
        }
    }

    namespace Generic
    {
        public class Outer<TOuter1, TOuter2>
        {
            public class Middle<TMiddle>
            {
                public class Inner<TInner1, TInner2>
                {
                    //普通方法
                    public static void NormalMethod1() { }
                    public void NormalMethod2() { }
                    public string NormalMethod3(int i, double? d) => throw new NotImplementedException();
                    public List<string> NormalMethod4(Dictionary<string, byte> dic, List<Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>> classes) => throw new NotImplementedException();
                    public (string name, int age) NormalMethod5(List<(string i, int a)> values, Outer<int, int?>.Middle<string>.Inner<List<string>, Dictionary<string, Point>> normalClass, IDictionary<string, int> dic) => throw new NotImplementedException();

                    //泛型方法
                    public static string GenericMethod1<T>(T t, int age, Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2> normalClass) => throw new NotImplementedException();
                    public T2 GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
                    public (T2, T3) GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3) => throw new NotImplementedException();
                    public List<(T2, T1)> GenericMethod4<T1, T2>(T1 t1, T2 t2, (T1, T2) no, List<T1> li, Dictionary<T2, List<T2>> dic) => throw new NotImplementedException();
                }
            }
        }
    }
}

#endregion

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    internal class GetClassMethodFullNameTests
    {
        #region GetClassFullName
        [Test]
        public void TestArrayCustome()
        {
            //简单类
            typeof(DemoNameSpace.Outer).GetClassFullName().ShouldBe("DemoNameSpace.Outer");
            typeof(DemoNameSpace.Outer[]).GetClassFullName().ShouldBe("DemoNameSpace.Outer[]");
            typeof(DemoNameSpace.Outer[][]).GetClassFullName().ShouldBe("DemoNameSpace.Outer[][]");
            typeof(DemoNameSpace.Outer[][][]).GetClassFullName().ShouldBe("DemoNameSpace.Outer[][][]");

            //其他类型
            //结构体
            typeof(DemoNameSpace.DemoStruct).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct");
            typeof(DemoNameSpace.DemoStruct[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct[]");
            typeof(DemoNameSpace.DemoStruct[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct[][]");
            typeof(DemoNameSpace.DemoStruct[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct[][][]");

            //枚举
            typeof(DemoNameSpace.DemoEnum).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum");
            typeof(DemoNameSpace.DemoEnum[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum[]");
            typeof(DemoNameSpace.DemoEnum[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum[][]");
            typeof(DemoNameSpace.DemoEnum[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum[][][]");

            //委托
            typeof(DemoNameSpace.DemoDelegate).GetClassFullName().ShouldBe("DemoNameSpace.DemoDelegate");
            typeof(DemoNameSpace.DemoDelegate[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoDelegate[]");
            typeof(DemoNameSpace.DemoDelegate[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoDelegate[][]");
            typeof(DemoNameSpace.DemoDelegate[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoDelegate[][][]");

            //接口
            typeof(DemoNameSpace.DemoInterface).GetClassFullName().ShouldBe("DemoNameSpace.DemoInterface");
            typeof(DemoNameSpace.DemoInterface[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoInterface[]");
            typeof(DemoNameSpace.DemoInterface[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoInterface[][]");
            typeof(DemoNameSpace.DemoInterface[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoInterface[][][]");

            //可为空
            //结构体
            typeof(DemoNameSpace.DemoStruct?).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct?");
            typeof(DemoNameSpace.DemoStruct?[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct?[]");
            typeof(DemoNameSpace.DemoStruct?[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct?[][]");
            typeof(DemoNameSpace.DemoStruct?[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoStruct?[][][]");

            //枚举
            typeof(DemoNameSpace.DemoEnum?).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum?");
            typeof(DemoNameSpace.DemoEnum?[]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum?[]");
            typeof(DemoNameSpace.DemoEnum?[][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum?[][]");
            typeof(DemoNameSpace.DemoEnum?[][][]).GetClassFullName().ShouldBe("DemoNameSpace.DemoEnum?[][][]");
        }

        [Test]
        public void TestArraySystem()
        {
            //简单类
            typeof(System.Array).GetClassFullName().ShouldBe("System.Array");
            typeof(System.Array[]).GetClassFullName().ShouldBe("System.Array[]");
            typeof(System.Array[][]).GetClassFullName().ShouldBe("System.Array[][]");
            typeof(System.Array[][][]).GetClassFullName().ShouldBe("System.Array[][][]");

            //其他类型
            //结构体
            typeof(System.Drawing.Point).GetClassFullName().ShouldBe("System.Drawing.Point");
            typeof(System.Drawing.Point[]).GetClassFullName().ShouldBe("System.Drawing.Point[]");
            typeof(System.Drawing.Point[][]).GetClassFullName().ShouldBe("System.Drawing.Point[][]");
            typeof(System.Drawing.Point[][][]).GetClassFullName().ShouldBe("System.Drawing.Point[][][]");

            //枚举
            typeof(System.Threading.Tasks.TaskStatus).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus");
            typeof(System.Threading.Tasks.TaskStatus[]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus[]");
            typeof(System.Threading.Tasks.TaskStatus[][]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus[][]");
            typeof(System.Threading.Tasks.TaskStatus[][][]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus[][][]");

            //委托
            typeof(System.Action).GetClassFullName().ShouldBe("System.Action");
            typeof(System.Action[]).GetClassFullName().ShouldBe("System.Action[]");
            typeof(System.Action[][]).GetClassFullName().ShouldBe("System.Action[][]");
            typeof(System.Action[][][]).GetClassFullName().ShouldBe("System.Action[][][]");

            //接口
            typeof(System.ICloneable).GetClassFullName().ShouldBe("System.ICloneable");
            typeof(System.ICloneable[]).GetClassFullName().ShouldBe("System.ICloneable[]");
            typeof(System.ICloneable[][]).GetClassFullName().ShouldBe("System.ICloneable[][]");
            typeof(System.ICloneable[][][]).GetClassFullName().ShouldBe("System.ICloneable[][][]");

            //可为空
            //结构体
            typeof(System.Drawing.Point?).GetClassFullName().ShouldBe("System.Drawing.Point?");
            typeof(System.Drawing.Point?[]).GetClassFullName().ShouldBe("System.Drawing.Point?[]");
            typeof(System.Drawing.Point?[][]).GetClassFullName().ShouldBe("System.Drawing.Point?[][]");
            typeof(System.Drawing.Point?[][][]).GetClassFullName().ShouldBe("System.Drawing.Point?[][][]");

            //枚举
            typeof(System.Threading.Tasks.TaskStatus?).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus?");
            typeof(System.Threading.Tasks.TaskStatus?[]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus?[]");
            typeof(System.Threading.Tasks.TaskStatus?[][]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus?[][]");
            typeof(System.Threading.Tasks.TaskStatus?[][][]).GetClassFullName().ShouldBe("System.Threading.Tasks.TaskStatus?[][][]");
        }

        [Test]
        public void TestOther()
        {
            typeof(List<DemoNameSpace.DemoEnum?[]>).GetClassFullName().ShouldBe("System.Collections.Generic.List<DemoNameSpace.DemoEnum?[]>");
            typeof(Dictionary<string, List<DemoNameSpace.DemoEnum?[]>>).GetClassFullName().ShouldBe("System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<DemoNameSpace.DemoEnum?[]>>");

            //系统预设类型
            typeof(int).GetClassFullName().ShouldBe("int");
            typeof(int[]).GetClassFullName().ShouldBe("int[]");
            typeof(int[][]).GetClassFullName().ShouldBe("int[][]");
            typeof(int?).GetClassFullName().ShouldBe("int?");
            typeof(int?[]).GetClassFullName().ShouldBe("int?[]");
            typeof(int?[][]).GetClassFullName().ShouldBe("int?[][]");
            typeof(List<int?>).GetClassFullName().ShouldBe("System.Collections.Generic.List<int?>");
            typeof(List<int?[][][]>).GetClassFullName().ShouldBe("System.Collections.Generic.List<int?[][][]>");
            typeof(List<List<List<int?[][][]>>>).GetClassFullName().ShouldBe("System.Collections.Generic.List<System.Collections.Generic.List<System.Collections.Generic.List<int?[][][]>>>");
            typeof(Dictionary<string, List<int?[][][]>>).GetClassFullName().ShouldBe("System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<int?[][][]>>");
        }

        [Test]
        public void TestInnerClass()
        {
            typeof(DemoNameSpace.Outer.Middle).GetClassFullName().ShouldBe("DemoNameSpace.Outer.Middle");
            typeof(DemoNameSpace.Outer.Middle[]).GetClassFullName().ShouldBe("DemoNameSpace.Outer.Middle[]");
            typeof(DemoNameSpace.Outer.Middle[][][]).GetClassFullName().ShouldBe("DemoNameSpace.Outer.Middle[][][]");
            typeof(List<DemoNameSpace.Outer.Middle[]>).GetClassFullName().ShouldBe("System.Collections.Generic.List<DemoNameSpace.Outer.Middle[]>");
            typeof(Dictionary<string, List<DemoNameSpace.Outer.Middle[]>>).GetClassFullName().ShouldBe("System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<DemoNameSpace.Outer.Middle[]>>");

            typeof(DemoNameSpace.Outer.Middle.Inner).GetClassFullName().ShouldBe("DemoNameSpace.Outer.Middle.Inner");
            typeof(Dictionary<string, List<DemoNameSpace.Outer.Middle.Inner[][]>>).GetClassFullName().ShouldBe("System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<DemoNameSpace.Outer.Middle.Inner[][]>>");
        }

        [Test]
        public void TestGeneric()
        {
            typeof(DemoNameSpace.Generic.Outer<int, double>.Middle<string>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<int, double>.Middle<string>");
            typeof(DemoNameSpace.Generic.Outer<int, double>.Middle<string>.Inner<bool, byte>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<int, double>.Middle<string>.Inner<bool, byte>");

            typeof(DemoNameSpace.Generic.Outer<int?, double?[]>.Middle<string>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<int?, double?[]>.Middle<string>");
            typeof(DemoNameSpace.Generic.Outer<int, double?[][]>.Middle<string>.Inner<bool?, byte[][]>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<int, double?[][]>.Middle<string>.Inner<bool?, byte[][]>");

            typeof(DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum, DemoNameSpace.DemoEnum?>.Middle<string>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum, DemoNameSpace.DemoEnum?>.Middle<string>");
            typeof(DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum?[], double>.Middle<List<DemoNameSpace.DemoEnum?>>.Inner<DemoNameSpace.DemoEnum[][], byte>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum?[], double>.Middle<System.Collections.Generic.List<DemoNameSpace.DemoEnum?>>.Inner<DemoNameSpace.DemoEnum[][], byte>");

            typeof(List<IEnumerable<Func<int>>>).GetClassFullName().ShouldBe("System.Collections.Generic.List<System.Collections.Generic.IEnumerable<System.Func<int>>>");
            typeof(List<IEnumerable<DemoNameSpace.DemoEnum?[][]>>).GetClassFullName().ShouldBe("System.Collections.Generic.List<System.Collections.Generic.IEnumerable<DemoNameSpace.DemoEnum?[][]>>");
        }


        [Test]
        public void TestGenericDefine()
        {
            typeof(List<>).GetClassFullName().ShouldBe("System.Collections.Generic.List<T>");
            typeof(IEnumerable<>).GetClassFullName().ShouldBe("System.Collections.Generic.IEnumerable<T>");
            typeof(Func<>).GetClassFullName().ShouldBe("System.Func<TResult>");

            typeof(DemoNameSpace.Generic.Outer<,>.Middle<>.Inner<,>).GetClassFullName().ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>");
        }
        #endregion

        #region GetClassGenericFullName
        [Test]
        public void TestArrayCustome2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res; ;

            var name = typeof(IEnumerable<>).GetClassGenericFullName().Name;

            //简单类
            res = typeof(DemoNameSpace.Outer).GetClassGenericFullName();
            res.Name.ShouldBe("DemoNameSpace.Outer");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoNameSpace.Outer[]).GetClassGenericFullName();
            res.Name.ShouldBe("DemoNameSpace.Outer[]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoNameSpace.Outer[][]).GetClassGenericFullName();
            res.Name.ShouldBe("DemoNameSpace.Outer[][]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoNameSpace.Outer[][][]).GetClassGenericFullName();
            res.Name.ShouldBe("DemoNameSpace.Outer[][][]");
            res.GenericTypes.Count.ShouldBe(0);

            //其他类型
            //结构体
            res = typeof(DemoNameSpace.DemoStruct).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoStruct"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoStruct[]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoStruct[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoStruct[][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoStruct[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoStruct[][][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoStruct[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //枚举
            res = typeof(DemoNameSpace.DemoEnum).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoEnum"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoEnum[]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoEnum[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoEnum[][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoEnum[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoEnum[][][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoEnum[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //委托
            res = typeof(DemoNameSpace.DemoDelegate).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoDelegate"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoDelegate[]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoDelegate[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoDelegate[][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoDelegate[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoDelegate[][][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoDelegate[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //接口
            res = typeof(DemoNameSpace.DemoInterface).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoInterface"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoInterface[]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoInterface[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoInterface[][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoInterface[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(DemoNameSpace.DemoInterface[][][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.DemoInterface[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //可为空
            //结构体
            res = typeof(DemoNameSpace.DemoStruct?).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoStruct));

            res = typeof(DemoNameSpace.DemoStruct?[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoStruct));

            res = typeof(DemoNameSpace.DemoStruct?[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoStruct));

            res = typeof(DemoNameSpace.DemoStruct?[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoStruct));

            //枚举
            res = typeof(DemoNameSpace.DemoEnum?).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum));

            res = typeof(DemoNameSpace.DemoEnum?[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum));

            res = typeof(DemoNameSpace.DemoEnum?[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum));

            res = typeof(DemoNameSpace.DemoEnum?[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum));
        }

        [Test]
        public void TestArraySystem2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            //简单类
            res = typeof(System.Array).GetClassGenericFullName(); res.Name.ShouldBe("System.Array"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Array[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Array[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Array[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Array[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Array[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Array[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //其他类型
            //结构体
            res = typeof(System.Drawing.Point).GetClassGenericFullName(); res.Name.ShouldBe("System.Drawing.Point"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Drawing.Point[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Drawing.Point[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Drawing.Point[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Drawing.Point[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Drawing.Point[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Drawing.Point[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //枚举
            res = typeof(System.Threading.Tasks.TaskStatus).GetClassGenericFullName(); res.Name.ShouldBe("System.Threading.Tasks.TaskStatus"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Threading.Tasks.TaskStatus[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Threading.Tasks.TaskStatus[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Threading.Tasks.TaskStatus[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Threading.Tasks.TaskStatus[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Threading.Tasks.TaskStatus[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Threading.Tasks.TaskStatus[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //委托
            res = typeof(System.Action).GetClassGenericFullName(); res.Name.ShouldBe("System.Action"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Action[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Action[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Action[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Action[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.Action[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Action[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //接口
            res = typeof(System.ICloneable).GetClassGenericFullName(); res.Name.ShouldBe("System.ICloneable"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.ICloneable[]).GetClassGenericFullName(); res.Name.ShouldBe("System.ICloneable[]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.ICloneable[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.ICloneable[][]"); res.GenericTypes.Count.ShouldBe(0);
            res = typeof(System.ICloneable[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.ICloneable[][][]"); res.GenericTypes.Count.ShouldBe(0);

            //可为空
            //结构体
            res = typeof(System.Drawing.Point?).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Drawing.Point));

            res = typeof(System.Drawing.Point?[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Drawing.Point));

            res = typeof(System.Drawing.Point?[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Drawing.Point));

            res = typeof(System.Drawing.Point?[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Drawing.Point));

            //枚举
            res = typeof(System.Threading.Tasks.TaskStatus?).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Threading.Tasks.TaskStatus));

            res = typeof(System.Threading.Tasks.TaskStatus?[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Threading.Tasks.TaskStatus));

            res = typeof(System.Threading.Tasks.TaskStatus?[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Threading.Tasks.TaskStatus));

            res = typeof(System.Threading.Tasks.TaskStatus?[][][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T"); res.GenericTypes[0].isGeneric.ShouldBe(false); res.GenericTypes[0].type.ShouldBe(typeof(System.Threading.Tasks.TaskStatus));
        }

        [Test]
        public void TestOther2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            res = typeof(List<DemoNameSpace.DemoEnum?[]>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum?[]));

            res = typeof(Dictionary<string, List<DemoNameSpace.DemoEnum?[]>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.Dictionary<TKey, TValue>");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("TKey");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(string));
            res.GenericTypes[1].name.ShouldBe("TValue");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(List<DemoNameSpace.DemoEnum?[]>));

            //系统预设类型
            res = typeof(int).GetClassGenericFullName(); res.Name.ShouldBe("int");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(int[]).GetClassGenericFullName(); res.Name.ShouldBe("int[]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(int[][]).GetClassGenericFullName(); res.Name.ShouldBe("int[][]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(int?).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));

            res = typeof(int?[]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));

            res = typeof(int?[][]).GetClassGenericFullName(); res.Name.ShouldBe("System.Nullable<T>[][]");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));

            res = typeof(List<int?>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int?));

            res = typeof(List<int?[][][]>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int?[][][]));

            res = typeof(List<List<List<int?[][][]>>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(List<List<int?[][][]>>));

            res = typeof(Dictionary<string, List<int?[][][]>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.Dictionary<TKey, TValue>");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("TKey");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(string));
            res.GenericTypes[1].name.ShouldBe("TValue");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(List<int?[][][]>));
        }

        [Test]
        public void TestInnerClass2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            res = typeof(DemoNameSpace.Outer.Middle).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Outer.Middle");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoNameSpace.Outer.Middle[]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Outer.Middle[]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoNameSpace.Outer.Middle[][][]).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Outer.Middle[][][]");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(List<DemoNameSpace.Outer.Middle[]>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.Outer.Middle[]));

            res = typeof(Dictionary<string, List<DemoNameSpace.Outer.Middle[]>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.Dictionary<TKey, TValue>");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("TKey");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(string));
            res.GenericTypes[1].name.ShouldBe("TValue");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(List<DemoNameSpace.Outer.Middle[]>));

            res = typeof(DemoNameSpace.Outer.Middle.Inner).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Outer.Middle.Inner");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(Dictionary<string, List<DemoNameSpace.Outer.Middle.Inner[][]>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.Dictionary<TKey, TValue>");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("TKey");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(string));
            res.GenericTypes[1].name.ShouldBe("TValue");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(List<DemoNameSpace.Outer.Middle.Inner[][]>));

        }

        [Test]
        public void TestGeneric2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            res = typeof(DemoNameSpace.Generic.Outer<int, double>.Middle<string>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(double));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(string));

            res = typeof(DemoNameSpace.Generic.Outer<int, double>.Middle<string>.Inner<bool, byte>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(double));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(string));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(bool));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(byte));

            res = typeof(DemoNameSpace.Generic.Outer<int?, double?[]>.Middle<string>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int?));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(double?[]));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(string));

            res = typeof(DemoNameSpace.Generic.Outer<int, double?[][]>.Middle<string>.Inner<bool?, byte[][]>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(double?[][]));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(string));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(bool?));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(byte[][]));

            res = typeof(DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum, DemoNameSpace.DemoEnum?>.Middle<string>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(DemoNameSpace.DemoEnum?));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(string));

            res = typeof(DemoNameSpace.Generic.Outer<DemoNameSpace.DemoEnum?[], double>.Middle<List<DemoNameSpace.DemoEnum?>>.Inner<DemoNameSpace.DemoEnum[][], byte>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(DemoNameSpace.DemoEnum?[]));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(double));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(List<DemoNameSpace.DemoEnum?>));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(DemoNameSpace.DemoEnum[][]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(byte));


            res = typeof(List<IEnumerable<Func<int>>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(IEnumerable<Func<int>>));


            res = typeof(List<IEnumerable<DemoNameSpace.DemoEnum?[][]>>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(IEnumerable<DemoNameSpace.DemoEnum?[][]>));
        }

        [Test]
        public void TestGenericDefine2()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            res = typeof(List<>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.List<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T");

            res = typeof(IEnumerable<>).GetClassGenericFullName(); res.Name.ShouldBe("System.Collections.Generic.IEnumerable<T>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T");

            res = typeof(Func<>).GetClassGenericFullName(); res.Name.ShouldBe("System.Func<TResult>");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("TResult");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TResult");

            res = typeof(DemoNameSpace.Generic.Outer<,>.Middle<>.Inner<,>).GetClassGenericFullName(); res.Name.ShouldBe("DemoNameSpace.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TOuter1");
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("TOuter2");
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("TMiddle");
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(true);
            res.GenericTypes[3].type.Name.ShouldBe("TInner1");
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(true);
            res.GenericTypes[4].type.Name.ShouldBe("TInner2");
        }
        #endregion

        #region GetClassGenericFullName -> containsOutOrIn
        [Test]
        public void GetClassGenericFullName_containsOutOrIn_Test()
        {
            var res = typeof(IEnumerable<int>).GetClassGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.IEnumerable<T>");
            res = typeof(IEnumerable<int>).GetClassGenericFullName(true);
            res.Name.ShouldBe("System.Collections.Generic.IEnumerable<out T>");

            res = typeof(Action<>).GetClassGenericFullName();
            res.Name.ShouldBe("System.Action<T>");

            res = typeof(Action<int>).GetClassGenericFullName(true);
            res.Name.ShouldBe("System.Action<in T>");
        }

        #endregion

        #region GetMethodFullName      
        //普通类
        [Test]
        public void TestMethodFullNameNormal()
        {
            //普通方法
            typeof(DemoMethod.NormalClass).GetMethod("NormalMethod1").GetMethodFullName().ShouldBe("void DemoMethod.NormalClass.NormalMethod1()");
            typeof(DemoMethod.NormalClass).GetMethod("NormalMethod2").GetMethodFullName().ShouldBe("void DemoMethod.NormalClass.NormalMethod2()");
            typeof(DemoMethod.NormalClass).GetMethod("NormalMethod3").GetMethodFullName().ShouldBe("string DemoMethod.NormalClass.NormalMethod3(int i, double? d)");
            typeof(DemoMethod.NormalClass).GetMethod("NormalMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<string> DemoMethod.NormalClass.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.NormalClass> classes)");

            typeof(DemoMethod.NormalClass).GetMethod("NormalMethod5").GetMethodFullName().ShouldBe("System.ValueTuple<string, int> DemoMethod.NormalClass.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.NormalClass normalClass, System.Collections.Generic.IDictionary<string, int> dic)");

            //泛型方法
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int)).GetMethodFullName().ShouldBe("string DemoMethod.NormalClass.GenericMethod1<int>(int t, int age, DemoMethod.NormalClass normalClass)");
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod2").MakeGenericMethod(typeof(int?), typeof(string), typeof(DemoMethod.NormalClass)).GetMethodFullName().ShouldBe("string DemoMethod.NormalClass.GenericMethod2<int?, string, DemoMethod.NormalClass>(int? t1, string t2, DemoMethod.NormalClass t3)");

            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod3").MakeGenericMethod(typeof(int), typeof(float?), typeof(bool)).GetMethodFullName().ShouldBe("System.ValueTuple<float?, bool> DemoMethod.NormalClass.GenericMethod3<int, float?, bool>(int t1, float? t2, bool t3)");
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod4").MakeGenericMethod(typeof(int), typeof(int?[][])).GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<int?[][], int>> DemoMethod.NormalClass.GenericMethod4<int, int?[][]>(int t1, int?[][] t2, System.ValueTuple<int, int?[][]> no, System.Collections.Generic.List<int> li, System.Collections.Generic.Dictionary<int?[][], System.Collections.Generic.List<int?[][]>> dic)");

            //非具化泛型方法
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod1").GetMethodFullName().ShouldBe("string DemoMethod.NormalClass.GenericMethod1<T>(T t, int age, DemoMethod.NormalClass normalClass)");
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod2").GetMethodFullName().ShouldBe("T2 DemoMethod.NormalClass.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod3").GetMethodFullName().ShouldBe("System.ValueTuple<T2, T3> DemoMethod.NormalClass.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.NormalClass).GetMethod("GenericMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.NormalClass.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
        }

        //嵌套类
        [Test]
        public void TestMethodFullNameInner()
        {
            //普通方法
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod1").GetMethodFullName().ShouldBe("void DemoMethod.Outer.Middle.Inner.NormalMethod1()");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod2").GetMethodFullName().ShouldBe("void DemoMethod.Outer.Middle.Inner.NormalMethod2()");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod3").GetMethodFullName().ShouldBe("string DemoMethod.Outer.Middle.Inner.NormalMethod3(int i, double? d)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<string> DemoMethod.Outer.Middle.Inner.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.Outer.Middle.Inner> classes)");

            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod5").GetMethodFullName().ShouldBe("System.ValueTuple<string, int> DemoMethod.Outer.Middle.Inner.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.Outer.Middle.Inner normalClass, System.Collections.Generic.IDictionary<string, int> dic)");

            //泛型方法
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int)).GetMethodFullName().ShouldBe("string DemoMethod.Outer.Middle.Inner.GenericMethod1<int>(int t, int age, DemoMethod.Outer.Middle.Inner normalClass)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod2").MakeGenericMethod(typeof(int?), typeof(string), typeof(DemoMethod.Outer.Middle.Inner)).GetMethodFullName().ShouldBe("string DemoMethod.Outer.Middle.Inner.GenericMethod2<int?, string, DemoMethod.Outer.Middle.Inner>(int? t1, string t2, DemoMethod.Outer.Middle.Inner t3)");

            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod3").MakeGenericMethod(typeof(int), typeof(float?), typeof(bool)).GetMethodFullName().ShouldBe("System.ValueTuple<float?, bool> DemoMethod.Outer.Middle.Inner.GenericMethod3<int, float?, bool>(int t1, float? t2, bool t3)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod4").MakeGenericMethod(typeof(int), typeof(int?[][])).GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<int?[][], int>> DemoMethod.Outer.Middle.Inner.GenericMethod4<int, int?[][]>(int t1, int?[][] t2, System.ValueTuple<int, int?[][]> no, System.Collections.Generic.List<int> li, System.Collections.Generic.Dictionary<int?[][], System.Collections.Generic.List<int?[][]>> dic)");

            //非具化泛型方法
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod1").GetMethodFullName().ShouldBe("string DemoMethod.Outer.Middle.Inner.GenericMethod1<T>(T t, int age, DemoMethod.Outer.Middle.Inner normalClass)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod2").GetMethodFullName().ShouldBe("T2 DemoMethod.Outer.Middle.Inner.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod3").GetMethodFullName().ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Outer.Middle.Inner.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Outer.Middle.Inner.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
        }

        //泛型类
        [Test]
        public void TestMethodFullNameGeneric()
        {
            //普通方法
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod1").GetMethodFullName().ShouldBe("void DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.NormalMethod1()");
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod2").GetMethodFullName().ShouldBe("void DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.NormalMethod2()");
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod3").GetMethodFullName().ShouldBe("string DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.NormalMethod3(int i, double? d)");
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<string> DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>> classes)");

            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod5").GetMethodFullName().ShouldBe("System.ValueTuple<string, int> DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.Generic.Outer<int, int?>.Middle<string>.Inner<System.Collections.Generic.List<string>, System.Collections.Generic.Dictionary<string, System.Drawing.Point>> normalClass, System.Collections.Generic.IDictionary<string, int> dic)");

            //泛型方法
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int)).GetMethodFullName().ShouldBe("string DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.GenericMethod1<int>(int t, int age, DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>> normalClass)");
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod2").MakeGenericMethod(typeof(int?), typeof(string), typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>)).GetMethodFullName().ShouldBe("string DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.GenericMethod2<int?, string, DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>>(int? t1, string t2, DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>> t3)");

            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod3").MakeGenericMethod(typeof(int), typeof(float?), typeof(bool)).GetMethodFullName().ShouldBe("System.ValueTuple<float?, bool> DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.GenericMethod3<int, float?, bool>(int t1, float? t2, bool t3)");
            typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod4").MakeGenericMethod(typeof(int), typeof(int?[][])).GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<int?[][], int>> DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], System.Collections.Generic.List<int?>>.GenericMethod4<int, int?[][]>(int t1, int?[][] t2, System.ValueTuple<int, int?[][]> no, System.Collections.Generic.List<int> li, System.Collections.Generic.Dictionary<int?[][], System.Collections.Generic.List<int?[][]>> dic)");

            //非具化泛型方法
            typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod1").GetMethodFullName().ShouldBe("string DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod1<T>(T t, int age, DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2> normalClass)");
            typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod2").GetMethodFullName().ShouldBe("T2 DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod3").GetMethodFullName().ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod4").GetMethodFullName().ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
        }
        #endregion

        #region GetMethodGenericFullName
        //普通类
        [Test]
        public void TestMethodGenericFullNameNormal()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            //普通方法
            res = typeof(DemoMethod.NormalClass).GetMethod("NormalMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.NormalClass.NormalMethod1()");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.NormalClass).GetMethod("NormalMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.NormalClass.NormalMethod2()");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.NormalClass).GetMethod("NormalMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.NormalClass.NormalMethod3(int i, double? d)");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.NormalClass).GetMethod("NormalMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<string> DemoMethod.NormalClass.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.NormalClass> classes)");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.NormalClass).GetMethod("NormalMethod5").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<string, int> DemoMethod.NormalClass.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.NormalClass normalClass, System.Collections.Generic.IDictionary<string, int> dic)");
            res.GenericTypes.Count.ShouldBe(0);

            //泛型方法
            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int?[])).GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.NormalClass.GenericMethod1<T>(T t, int age, DemoMethod.NormalClass normalClass)");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int?[]));

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod2").MakeGenericMethod(typeof(float?), typeof(List<int?[]>), typeof(DemoMethod.NormalClass)).GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.NormalClass.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(float?));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(List<int?[]>));
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(DemoMethod.NormalClass));

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod3").MakeGenericMethod(typeof(Dictionary<int?, List<string>>), typeof((int?, string)), typeof(int?[][][])).GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.NormalClass.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(Dictionary<int?, List<string>>));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof((int?, string)));
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(int?[][][]));

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod4").MakeGenericMethod(typeof(Point?), typeof(TaskStatus[])).GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.NormalClass.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(Point?));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(TaskStatus[]));

            //非具化泛型方法
            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.NormalClass.GenericMethod1<T>(T t, int age, DemoMethod.NormalClass normalClass)");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T");

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.NormalClass.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.NormalClass.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.NormalClass).GetMethod("GenericMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.NormalClass.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
        }

        //嵌套类
        [Test]
        public void TestMethodGenericFullNameInner()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            //普通方法
            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.Outer.Middle.Inner.NormalMethod1()");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.Outer.Middle.Inner.NormalMethod2()");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Outer.Middle.Inner.NormalMethod3(int i, double? d)");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<string> DemoMethod.Outer.Middle.Inner.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.Outer.Middle.Inner> classes)");
            res.GenericTypes.Count.ShouldBe(0);

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("NormalMethod5").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<string, int> DemoMethod.Outer.Middle.Inner.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.Outer.Middle.Inner normalClass, System.Collections.Generic.IDictionary<string, int> dic)");
            res.GenericTypes.Count.ShouldBe(0);

            //泛型方法
            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int)).GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Outer.Middle.Inner.GenericMethod1<T>(T t, int age, DemoMethod.Outer.Middle.Inner normalClass)");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod2").MakeGenericMethod(typeof(int?), typeof(string), typeof(DemoMethod.Outer.Middle.Inner)).GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.Outer.Middle.Inner.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int?));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(DemoMethod.Outer.Middle.Inner));

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod3").MakeGenericMethod(typeof(int), typeof(float?), typeof(bool)).GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Outer.Middle.Inner.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(float?));
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool));

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod4").MakeGenericMethod(typeof(int), typeof(int?[][])).GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Outer.Middle.Inner.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(int?[][]));

            //非具化泛型方法
            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Outer.Middle.Inner.GenericMethod1<T>(T t, int age, DemoMethod.Outer.Middle.Inner normalClass)");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T");

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.Outer.Middle.Inner.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Outer.Middle.Inner.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(3);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
            res.GenericTypes[2].name.ShouldBe("T3");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.Outer.Middle.Inner).GetMethod("GenericMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Outer.Middle.Inner.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(2);
            res.GenericTypes[0].name.ShouldBe("T1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("T1");
            res.GenericTypes[1].name.ShouldBe("T2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("T2");
        }

        //泛型类
        [Test]
        public void TestMethodGenericFullNameGeneric()
        {
            (string Name, List<(string name, bool isGeneric, Type type)> GenericTypes) res;
            //普通方法
            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.NormalMethod1()");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));

            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("void DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.NormalMethod2()");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));

            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.NormalMethod3(int i, double? d)");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));

            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<string> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.NormalMethod4(System.Collections.Generic.Dictionary<string, byte> dic, System.Collections.Generic.List<DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>> classes)");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));


            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("NormalMethod5").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<string, int> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.NormalMethod5(System.Collections.Generic.List<System.ValueTuple<string, int>> values, DemoMethod.Generic.Outer<int, int?>.Middle<string>.Inner<System.Collections.Generic.List<string>, System.Collections.Generic.Dictionary<string, System.Drawing.Point>> normalClass, System.Collections.Generic.IDictionary<string, int> dic)");
            res.GenericTypes.Count.ShouldBe(5);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));

            //泛型方法
            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod1").MakeGenericMethod(typeof(int)).GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod1<T>(T t, int age, DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2> normalClass)");
            res.GenericTypes.Count.ShouldBe(6);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));
            res.GenericTypes[5].name.ShouldBe("T");
            res.GenericTypes[5].isGeneric.ShouldBe(false);
            res.GenericTypes[5].type.ShouldBe(typeof(int));


            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod2").MakeGenericMethod(typeof(int?), typeof(string), typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>)).GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(8);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(false);
            res.GenericTypes[5].type.ShouldBe(typeof(int?));
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(false);
            res.GenericTypes[6].type.ShouldBe(typeof(string));
            res.GenericTypes[7].name.ShouldBe("T3");
            res.GenericTypes[7].isGeneric.ShouldBe(false);
            res.GenericTypes[7].type.ShouldBe(typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>));

            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod3").MakeGenericMethod(typeof(int), typeof(float?), typeof(bool)).GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(8);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(false);
            res.GenericTypes[5].type.ShouldBe(typeof(int));
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(false);
            res.GenericTypes[6].type.ShouldBe(typeof(float?));
            res.GenericTypes[7].name.ShouldBe("T3");
            res.GenericTypes[7].isGeneric.ShouldBe(false);
            res.GenericTypes[7].type.ShouldBe(typeof(bool));

            res = typeof(DemoMethod.Generic.Outer<int, string>.Middle<bool?>.Inner<byte[], List<int?>>).GetMethod("GenericMethod4").MakeGenericMethod(typeof(int), typeof(int?[][])).GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(7);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(false);
            res.GenericTypes[1].type.ShouldBe(typeof(string));
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(false);
            res.GenericTypes[2].type.ShouldBe(typeof(bool?));
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(false);
            res.GenericTypes[3].type.ShouldBe(typeof(byte[]));
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(false);
            res.GenericTypes[4].type.ShouldBe(typeof(List<int?>));
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(false);
            res.GenericTypes[5].type.ShouldBe(typeof(int));
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(false);
            res.GenericTypes[6].type.ShouldBe(typeof(int?[][]));

            //非具化泛型方法
            res = typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod1").GetMethodGenericFullName();
            res.Name.ShouldBe("string DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod1<T>(T t, int age, DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2> normalClass)");
            res.GenericTypes.Count.ShouldBe(6);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TOuter1");
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("TOuter2");
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("TMiddle");
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(true);
            res.GenericTypes[3].type.Name.ShouldBe("TInner1");
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(true);
            res.GenericTypes[4].type.Name.ShouldBe("TInner2");
            res.GenericTypes[5].name.ShouldBe("T");
            res.GenericTypes[5].isGeneric.ShouldBe(true);
            res.GenericTypes[5].type.Name.ShouldBe("T");

            res = typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod2").GetMethodGenericFullName();
            res.Name.ShouldBe("T2 DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod2<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(8);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TOuter1");
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("TOuter2");
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("TMiddle");
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(true);
            res.GenericTypes[3].type.Name.ShouldBe("TInner1");
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(true);
            res.GenericTypes[4].type.Name.ShouldBe("TInner2");
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(true);
            res.GenericTypes[5].type.Name.ShouldBe("T1");
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(true);
            res.GenericTypes[6].type.Name.ShouldBe("T2");
            res.GenericTypes[7].name.ShouldBe("T3");
            res.GenericTypes[7].isGeneric.ShouldBe(true);
            res.GenericTypes[7].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod3").GetMethodGenericFullName();
            res.Name.ShouldBe("System.ValueTuple<T2, T3> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod3<T1, T2, T3>(T1 t1, T2 t2, T3 t3)");
            res.GenericTypes.Count.ShouldBe(8);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TOuter1");
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("TOuter2");
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("TMiddle");
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(true);
            res.GenericTypes[3].type.Name.ShouldBe("TInner1");
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(true);
            res.GenericTypes[4].type.Name.ShouldBe("TInner2");
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(true);
            res.GenericTypes[5].type.Name.ShouldBe("T1");
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(true);
            res.GenericTypes[6].type.Name.ShouldBe("T2");
            res.GenericTypes[7].name.ShouldBe("T3");
            res.GenericTypes[7].isGeneric.ShouldBe(true);
            res.GenericTypes[7].type.Name.ShouldBe("T3");

            res = typeof(DemoMethod.Generic.Outer<,>.Middle<>.Inner<,>).GetMethod("GenericMethod4").GetMethodGenericFullName();
            res.Name.ShouldBe("System.Collections.Generic.List<System.ValueTuple<T2, T1>> DemoMethod.Generic.Outer<TOuter1, TOuter2>.Middle<TMiddle>.Inner<TInner1, TInner2>.GenericMethod4<T1, T2>(T1 t1, T2 t2, System.ValueTuple<T1, T2> no, System.Collections.Generic.List<T1> li, System.Collections.Generic.Dictionary<T2, System.Collections.Generic.List<T2>> dic)");
            res.GenericTypes.Count.ShouldBe(7);
            res.GenericTypes[0].name.ShouldBe("TOuter1");
            res.GenericTypes[0].isGeneric.ShouldBe(true);
            res.GenericTypes[0].type.Name.ShouldBe("TOuter1");
            res.GenericTypes[1].name.ShouldBe("TOuter2");
            res.GenericTypes[1].isGeneric.ShouldBe(true);
            res.GenericTypes[1].type.Name.ShouldBe("TOuter2");
            res.GenericTypes[2].name.ShouldBe("TMiddle");
            res.GenericTypes[2].isGeneric.ShouldBe(true);
            res.GenericTypes[2].type.Name.ShouldBe("TMiddle");
            res.GenericTypes[3].name.ShouldBe("TInner1");
            res.GenericTypes[3].isGeneric.ShouldBe(true);
            res.GenericTypes[3].type.Name.ShouldBe("TInner1");
            res.GenericTypes[4].name.ShouldBe("TInner2");
            res.GenericTypes[4].isGeneric.ShouldBe(true);
            res.GenericTypes[4].type.Name.ShouldBe("TInner2");
            res.GenericTypes[5].name.ShouldBe("T1");
            res.GenericTypes[5].isGeneric.ShouldBe(true);
            res.GenericTypes[5].type.Name.ShouldBe("T1");
            res.GenericTypes[6].name.ShouldBe("T2");
            res.GenericTypes[6].isGeneric.ShouldBe(true);
            res.GenericTypes[6].type.Name.ShouldBe("T2");
        }

        #endregion

        class Fu<T>
        {
            public void Show(T name) { }
            public void Show(Expression<Func<T, bool>> id) { }
        }

        [Test]
        public void Test_When_Parameter_IsGeneric_NeedJudge()
        {
            var m = typeof(Fu<int>).GetMethods()[1];
            var res = m.GetMethodGenericFullName();
            res.Name.ShouldBe("void DotNetCommon.Test.Extensions.GetClassMethodFullNameTests.Fu<T>.Show(System.Linq.Expressions.Expression<System.Func<T, bool>> id)");
            res.GenericTypes.Count.ShouldBe(1);
            res.GenericTypes[0].name.ShouldBe("T");
            res.GenericTypes[0].isGeneric.ShouldBe(false);
            res.GenericTypes[0].type.ShouldBe(typeof(int));
        }
    }
}