﻿using DotNetCommon.Serialize;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using System.Text.Json.Nodes;
using System.Diagnostics.CodeAnalysis;
using DotNetCommon.Data;
using System.Xml.Linq;
using System.Text.Json.Serialization;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class JsonExtensionTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Age = 18,
                Name = "小明",
                Birth = DateTime.Now
            };
            //{"Id":"1","Age":18,"Name":"小明","Birth":"2021-05-25 21:04:43"}
            var json = person.ToJsonFast(number2String: true);

            //{"Id":"1","Age":"18","Name":"小明","Birth":"2021-05-25"}
            json = person.ToJsonFast(number2String: true, dateTimeFormatString: "yyyy-MM-dd");


            json = new { Dic = new Dictionary<string, object> { { "Age", 18 }, { "name", "tom" } } }.ToJsonFast(lowerCamelCase: true, lowerCamelCaseDictionaryKey: true);
            json.ShouldBe("{\"dic\":{\"age\":18,\"name\":\"tom\"}}");

            json = new { Dic = new Dictionary<string, object> { { "Age", 18 }, { "name", "tom" } } }.ToJsonFast(lowerCamelCase: true);
            json.ShouldBe("{\"dic\":{\"Age\":18,\"name\":\"tom\"}}");

            json = new { Obj = new { Age = 18, name = "tom" }.ToJsonObject() }.ToJsonFast(lowerCamelCase: true);
            json.ShouldBe("{\"obj\":{\"Age\":18,\"name\":\"tom\"}}");

            json = new { Obj = new { Age = 18, name = "tom" }.ToJsonObject() }.ToJsonFast(lowerCamelCase: true, lowerCamelCaseDictionaryKey: true);
            //无法将 JsonObject 中的key首字母小写
            json.ShouldBe("{\"obj\":{\"Age\":18,\"name\":\"tom\"}}");
        }

        [Test]
        public void TestJsonObjectContainsObject()
        {
            var arr = new[] { 1, 2, 3 }.ToJson().ToObject<JsonArray>();
            arr.ContainsObject(1).ShouldBe(true);

            var arr2 = new[] { new Person { Id = 1, Name = "小明" }, new Person { Id = 2, Name = "小明" } }.ToJson().ToObject<JsonArray>();
            arr2.ContainsObject(new Person { Id = 1, Name = "小明" }, new EqualityComparerPeson()).ShouldBe(true);
            //处理null
            arr2.ContainsObject(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2 = null;
            arr2.ContainsObject(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObject(new Person { Id = 1, Name = "小明" }, new EqualityComparerPeson()).ShouldBe(false);
        }

        [Test]
        public void TestJsonObjectContainsObjectAny()
        {
            var arr = new[] { 1, 2, 3 }.ToJson().ToObject<JsonArray>();
            arr.ContainsObjectAny(new[] { 1, 2 }).ShouldBe(true);
            arr.ContainsObjectAny(new[] { 1, 5 }).ShouldBe(true);
            arr.ContainsObjectAny(new[] { 4, 5 }).ShouldBe(false);

            var arr2 = new[] { new Person { Id = 1, Name = "小明" }, new Person { Id = 2, Name = "小明" } }.ToJson().ToObject<JsonArray>();
            arr2.ContainsObjectAny(new[] { new Person { Id = 1, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObjectAny(new[] { new Person { Id = 1, Name = "小明" }, new Person { Id = 5, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObjectAny(new[] { new Person { Id = 4, Name = "小明" }, new Person { Id = 5, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(false);

            //处理null
            arr2.ContainsObjectAny(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2 = null;
            arr2.ContainsObjectAny(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObjectAny(new[] { new Person { Id = 1, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(false);
        }

        [Test]
        public void TestJsonObjectContainsObjectAll()
        {
            var arr = new[] { 1, 2, 3 }.ToJson().ToObject<JsonArray>();
            arr.ContainsObjectAll(new[] { 1, 2 }).ShouldBe(true);
            arr.ContainsObjectAll(new[] { 1, 5 }).ShouldBe(false);
            arr.ContainsObjectAll(new[] { 4, 5 }).ShouldBe(false);

            var arr2 = new[] { new Person { Id = 1, Name = "小明" }, new Person { Id = 2, Name = "小明" } }.ToJson().ToObject<JsonArray>();
            arr2.ContainsObjectAll(new[] { new Person { Id = 1, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObjectAll(new[] { new Person { Id = 1, Name = "小明" }, new Person { Id = 5, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(false);
            arr2.ContainsObjectAll(new[] { new Person { Id = 4, Name = "小明" }, new Person { Id = 5, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(false);

            //处理null
            arr2.ContainsObjectAll(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2 = null;
            arr2.ContainsObjectAll(null, new EqualityComparerPeson()).ShouldBe(true);
            arr2.ContainsObjectAll(new[] { new Person { Id = 1, Name = "小明" } }, new EqualityComparerPeson()).ShouldBe(false);
        }

        public class Person
        {
            public long Id { get; set; }
            public int Age { get; set; }
            public string Name { get; set; }
            public DateTime Birth { get; set; }
        }

        public class EqualityComparerPeson : EqualityComparer<Person>
        {
            public override bool Equals(Person x, Person y)
            {
                if (x == null && y == null) return true;
                if (x == null || y == null) return false;
                return x.Id == y.Id;
            }

            public override int GetHashCode([DisallowNull] Person obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        [Test]
        public void TestJsonArray()
        {
            var arr = new JsonArray();
            arr.AddFluent(1).AddFluent(2);
            arr.ToJson().ShouldBe("[1,2]");
            arr.InsertAtFluent(0, 9).InsertAtFluent(1, 30);
            arr.ToJson().ShouldBe("[9,30,1,2]");
            arr.RemoveAtFluent(0).RemoveAtFluent(1);
            arr.ToJson().ShouldBe("[30,2]");
            arr.AddRangeFluent(new[] { 3, 4 });
            arr.ToJson().ShouldBe("[30,2,3,4]");
            arr.ClearFluent().ToJson().ShouldBe("[]");

            arr.AddFluent(1).AddFluent(2).AddFluent(3).ToJson().ShouldBe("[1,2,3]");
            arr.SetFluent(1, 4).ToJson().ShouldBe("[1,4,3]");
        }

        [Test]
        public void TestJsonObject()
        {
            var obj = new JsonObject();
            obj.SetFluent("name", "小明").SetFluent("age", 18);
            obj.ToJson().ShouldBe("{\"name\":\"小明\",\"age\":18}");
            obj.SetFluent("name", "tom").RemoveFluent("age").RemoveFluent("addr").ToJson().ShouldBe("{\"name\":\"tom\"}");
            obj.ClearFluent().ToJson().ShouldBe("{}");
        }

        //[Test]
        //public void TestDateTimeConvert()
        //{
        //    var dt = DateTime.Parse("2023-09-18 18:01:02.1234567");
        //    var obj = new
        //    {
        //        Birth = dt
        //    };
        //    var defaultStr = System.Text.Json.JsonSerializer.Serialize(obj);
        //    defaultStr.ShouldBe("{\"Birth\":\"2023-09-18T18:01:02.1234567\"}");

        //    var options = JsonHelper.GetDefaultJsonSerializerOptions();
        //    options.Converters.Add(new JsonConverterDatetime());
        //    var str = System.Text.Json.JsonSerializer.Serialize(obj, options);
        //    str.ShouldBe("{\"Birth\":\"2023-09-18T18:01:02.1234567\"}");

        //    options = JsonHelper.GetDefaultJsonSerializerOptions(dateTimeFormatString: "yyyy-MM-dd HH:mm:ss");
        //    str = System.Text.Json.JsonSerializer.Serialize(obj, options);
        //    str.ShouldBe("{\"Birth\":\"2023-09-18 18:01:02\"}");
        //}

        //[Test]
        //public void TestDateTimeOffsetConvert()
        //{
        //    var dt = DateTimeOffset.Parse("2023-09-18 18:01:02.1234567");
        //    var obj = new
        //    {
        //        Birth = dt
        //    };
        //    var defaultStr = System.Text.Json.JsonSerializer.Serialize(obj);
        //    defaultStr.ShouldBe("{\"Birth\":\"2023-09-18T18:01:02.1234567+08:00\"}");

        //    var options = JsonHelper.GetDefaultJsonSerializerOptions();
        //    options.Converters.Add(new JsonConverterDateTimeOffset());
        //    var str = System.Text.Json.JsonSerializer.Serialize(obj, options);
        //    str.ShouldBe("{\"Birth\":\"2023-09-18T18:01:02.1234567+08:00\"}");

        //    options = JsonHelper.GetDefaultJsonSerializerOptions(dateTimeOffsetFormatString: "yyyy-MM-dd HH:mm:ss");
        //    str = System.Text.Json.JsonSerializer.Serialize(obj, options);
        //    str.ShouldBe("{\"Birth\":\"2023-09-18 18:01:02\"}");
        //}
    }
}
