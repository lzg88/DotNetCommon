﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using Shouldly;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class TypeExtensionsTests
    {
        [Test]
        public void IsNullableTest()
        {
            typeof(int).IsNullable().ShouldBeFalse();
            typeof(int?).IsNullable().ShouldBeTrue();
        }

        [Test]
        public void IsNumericTest()
        {
            typeof(int).IsNumeric().ShouldBeTrue();
            typeof(int?).IsNullable().ShouldBeTrue();

            typeof(bool).IsNullable().ShouldBeFalse();
        }

        [Test]
        public void IsSequenceTest()
        {
            typeof(List<int>).IsSequence(out var seqType).ShouldBeTrue();
            typeof(int).IsSequence(out var seqType2).ShouldBeFalse();
            typeof(string).IsSequence(out var seqType3).ShouldBeTrue();
            typeof(object).IsSequence(out var seqType4).ShouldBeFalse();
        }

        [Test]
        public void IsArrayOfTest()
        {
            typeof(List<int>).IsArrayOf<int>().ShouldBeFalse();
            typeof(int[]).IsArrayOf<int>().ShouldBeTrue();
            typeof(object[]).IsArrayOf<object>().ShouldBeTrue();
        }

        [Test]
        public void IsGenericListTest()
        {
            typeof(List<int>).IsGenericList().ShouldBeTrue();
            typeof(IList<int>).IsGenericList().ShouldBeTrue();
            typeof(ICollection<int>).IsGenericList().ShouldBeFalse();
        }

        [Test]
        public void IsAnonymousTest()
        {
            new { id = 1 }.GetType().IsAnonymous().ShouldBe(true);
            new { }.GetType().IsAnonymous().ShouldBe(true);
            new object().GetType().IsAnonymous().ShouldBe(false);
            1.GetType().IsAnonymous().ShouldBe(false);
            Type type = null;
            type.IsAnonymous().ShouldBe(false);
            object obj = new { };
            obj.GetType().IsAnonymous().ShouldBe(true);
        }
    }
}
