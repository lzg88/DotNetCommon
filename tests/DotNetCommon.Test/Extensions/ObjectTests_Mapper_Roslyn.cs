﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;
using System.Diagnostics;
using Newtonsoft.Json;
using DotNetCommon.Serialize;
using System.Threading;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class ObjectTests_Mapper_Roslyn
    {
        [Test]
        public void Test()
        {
            var str = typeof(SimpleDto[]).GetClassFullName();
            var ctx = new RoslynMapper().GenerateMapCode(typeof(Simple), typeof(SimpleDto));
            RoslynMapper.RegisterConvert<Simple, SimpleDto>();
            Thread.Sleep(10000);
            var simple = new Simple
            {
                Id = 1,
                Name = "Name"
            };
            var dto = simple.Mapper<SimpleDto>();

        }

        public class Simple
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class SimpleDto
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
