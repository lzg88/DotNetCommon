﻿using NUnit.Framework;
using System.Collections.Generic;
using DotNetCommon.Extensions;
using System.Text.Json.Nodes;
using Shouldly;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    internal class EnumerableTests
    {
        [Test]
        public void TestContainsAnyAll()
        {
            var list = new List<int>() { 1, 2, 3, 4, 5 };
            list.ContainsAny(new[] { 1, 6 }).ShouldBe(true);
            list.ContainsAny(new[] { 8, 9 }).ShouldBe(false);
            list.ContainsAll(new[] { 1, 2 }).ShouldBe(true);
            list.ContainsAll(new[] { 1, 9 }).ShouldBe(false);
            list.ContainsAll(new[] { 8, 9 }).ShouldBe(false);

            //null值处理
            list = null;
            list.ContainsAny(new[] { 1 }).ShouldBe(false);
            list.ContainsAny(list).ShouldBe(true);

            list.ContainsAll(new[] { 1 }).ShouldBe(false);
            list.ContainsAll(list).ShouldBe(true);
        }

        [Test]
        public void TestContainsAnyAllJson()
        {
            var arr = new[] { 1, 2, 3 }.ToJson().ToObject<JsonArray>();
            arr.ContainsObjectAny(new[] { 1, 5 }).ShouldBe(true);
            arr.ContainsObjectAny(new[] { 8, 9 }).ShouldBe(false);

            arr.ContainsObjectAll(new[] { 1, 3 }).ShouldBe(true);
            arr.ContainsObjectAll(new[] { 1, 9 }).ShouldBe(false);
            arr.ContainsObjectAll(new[] { 8, 9 }).ShouldBe(false);
        }
    }
}
