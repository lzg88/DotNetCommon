﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class ObjectTests_IfNullUse
    {
        [Test]
        public void Test()
        {
            int? num = null;
            num.IfNullOrDefaultUse(5).ShouldBe(5);
            num.IfNullOrDefaultUse().ShouldBe(0);

            var num2 = 0;
            num2.IfDefaultUse(2).ShouldBe(2);

            bool? flag = null;
            flag.IfNullUse().ShouldBe(false);
            flag.IfNullUse(true).ShouldBe(true);

            byte? b = null;
            b.IfNullOrDefaultUse(5);


            object obj = null;
            obj.IfNullUse(new object()).ShouldNotBeNull();
        }
        [Test]
        public void Test2()
        {
            //集合null
            List<int> nums = null;
            nums.IfNullUseNewList().Count.ShouldBe(0);

            //数组null
            int[] ints = null;
            ints.IfNullUseNewArray().Length.ShouldBe(0);
            ints.IfNullUseNewList().Count.ShouldBe(0);

            //字典null
            Dictionary<string, string> dic = null;
            dic.IfNullUseNewDictinoary().Count.ShouldBe(0);

            //字符串
            string str = null;
            str.IfNullOrEmptyUse("haha").ShouldBe("haha");
            str = "";
            str.IfNullOrEmptyUse("hehe").ShouldBe("hehe");
        }
    }
}
