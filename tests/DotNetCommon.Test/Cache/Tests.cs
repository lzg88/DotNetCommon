﻿//using DotNetCommon.Cache;
//using NUnit.Framework;
//using Shouldly;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace DotNetCommon.Test.Cache
//{
//    [TestFixture]
//    internal class Tests
//    {
//        [Test]
//        public void Test()
//        {
//            //IsInCache
//            var cache = new MemoryCache();
//            cache.Set_NotExpire("小明", "Set_NotExpire");
            
//            cache.IsInCache("小明").ShouldBeTrue();
//            cache.IsInCache("小红").ShouldBeFalse();
//            cache.Count.ShouldBe(1);

//            cache.Clear();
//            cache.Count.ShouldBe(0);

//            var obj = new
//            {
//                Id = 1,
//                Name = "小明"
//            };
//            cache.Set_NotExpire("小明", obj);
//            cache.Get("小明").ShouldBe(obj);

//            cache.Remove("小明");
//            cache.Get("小明").ShouldBeNull();
//            cache.Count.ShouldBe(0);

//            cache.Set_NotExpire("小a", new
//            {
//                Id = 1,
//                Name = "小a"
//            });
//            cache.Set_NotExpire("小b", new
//            {
//                Id = 2,
//                Name = "小b"
//            });
//            cache.Count.ShouldBe(2);
//            cache.GetAllKeys().OrderBy(i => i).ToList().ShouldBe(new List<string>() { "小a", "小b" });
//        }

//        [Test]
//        public void AbsoulteTest()
//        {
//            var cache = new MemoryCache();
//            cache.Set_AbsoluteExpire("小明", "Set_AbsoluteExpire", TimeSpan.FromSeconds(1));
//            cache.Count.ShouldBe(1);
//            cache.GetAllKeys().ShouldBe(new List<string>() { "小明" });
//            Task.Run(() =>
//            {
//                Thread.Sleep(2000);
//                cache.Count.ShouldBe(0);
//            }).Wait();
//            cache.Count.ShouldBe(0);
//        }

//        [Test]
//        public void SlideTest()
//        {
//            var cache = new MemoryCache();
//            cache.Set_SlidingExpire("小明", "Set_SlidingExpire", TimeSpan.FromSeconds(2));
//            cache.Count.ShouldBe(1);
//            cache.GetAllKeys().ShouldBe(new List<string>() { "小明" });
//            Task.Run(() =>
//            {
//                //连续访问3次 都不过期
//                Thread.Sleep(1000);
//                cache.Get("小明");
//                cache.Count.ShouldBe(1);
//                Thread.Sleep(1000);
//                cache.Get("小明");
//                cache.Count.ShouldBe(1);
//                Thread.Sleep(1000);
//                cache.Get("小明");
//                cache.Count.ShouldBe(1);

//                //超过2秒不访问，再访问时过期
//                Thread.Sleep(2500);
//                cache.Count.ShouldBe(0);
//                cache.Get("小明").ShouldBeNull();

//            }).Wait();
//            cache.Count.ShouldBe(0);
//        }

//        [Test]
//        public void SlidingAndAbsoluteTest()
//        {
//            var cache = new MemoryCache();
//            cache.Set_SlidingAndAbsoluteExpire("小明", "Set_SlidingAndAbsoluteExpire", TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(3));
//            cache.Count.ShouldBe(1);
//            cache.GetAllKeys().ShouldBe(new List<string>() { "小明" });
//            var now = DateTime.Now;
//            var task = Task.Run(() =>
//              {
//                  while (true)
//                  {
//                      Thread.Sleep(800);
//                      var val = cache.Get("小明");
//                      var count = cache.Count;
//                      if (count == 0)
//                      {
//                          (DateTime.Now - now).ShouldBeGreaterThan(TimeSpan.FromSeconds(3));
//                          break;
//                      }
//                  }
//              });
//            Task.Delay(3000).Wait();
//            cache.Get("小明").ShouldBeNull();
//            task.Wait();
//            cache.Count.ShouldBe(0);
//        }
//    }
//}
