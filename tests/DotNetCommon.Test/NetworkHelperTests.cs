﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class NetworkHelperTests
    {
        [Test]
        public void Test()
        {
            //输出: DESKTOP-JACKLETTER.
            var name = NetworkHelper.GetFQDN();

            //输出: 192.168.0.6
            var ip = NetworkHelper.GetLocalIPAddress();

            //输出:
            /*
            {[{192.168.0.6}, 以太网]}
            {[{192.168.222.1}, VMware Network Adapter VMnet1]}
            {[{192.168.186.1}, VMware Network Adapter VMnet8]}
            */
            var ips = NetworkHelper.GetLocalIPAddresses();
        }
    }
}
