﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class JsonHelperTests
    {
        [Test]
        public void IsArray()
        {
            var json = @"[]";
            JsonHelper.IsArray(json).ShouldBeTrue();
            json = @"{}";
            JsonHelper.IsArray(json).ShouldBeFalse();
            json = @"//
//
{}";
            JsonHelper.IsArray(json).ShouldBeFalse();

            json = @"//
//
[]";
            JsonHelper.IsArray(json).ShouldBeTrue();
            json = @"/* 4545
*/
{}";
            JsonHelper.IsArray(json).ShouldBeFalse();
            json = @"/* 4545
*/
[]";
            JsonHelper.IsArray(json).ShouldBeTrue();
            json = @"/* 4545
*/
///
[]";
            JsonHelper.IsArray(json).ShouldBeTrue();
            json = @"/* 4545
*/
///
{}";
            JsonHelper.IsArray(json).ShouldBeFalse();
            json = @"
///
///
/*456*/
[]";
            JsonHelper.IsArray(json).ShouldBeTrue();
            json = @"
///
///
/*456*/
{}";
            JsonHelper.IsArray(json).ShouldBeFalse();
        }

        [Test]
        public void NoComment()
        {
            var json = @"{
    ""name"":""小明"",
    ""age"":18,
    ""birth"":""1998-02-01"",
    ""score:"":98.5,
    ""ref"":null,
}";

            var result = JsonHelper.Format(json);
            result.ShouldBe("""
                {
                  "name": "小明",
                  "age": 18,
                  "birth": "1998-02-01",
                  "score:": 98.5,
                  "ref": null
                }
                """);
        }

        [Test]
        public void Simple()
        {
            var json = @"
{
    ""name"":""小明"",
    ""age"":18,
    ""birth"":""1998-02-01""
    //""score:"":98.5,
    /*""ref"":null,
    ""desc"":undefined*/
}
";
            var res = JsonHelper.Format(json);
            res.ShouldBe("""
                {
                  "name": "小明",
                  "age": 18,
                  "birth": "1998-02-01"
                }
                """);

            json = @"
//789
/*
78945
*/
[
{
    ""name"":""小明"",
    ""age"":18,
    ""birth"":""1998-02-01""
    //""score:"":98.5,
    /*""ref"":null,
    ""desc"":undefined*/
},
//45
{
    ""name"":""jack""/*,
/*jdia*/
}]
";
            res = JsonHelper.Format(json);
            res.ShouldBe("""
                [
                  {
                    "name": "小明",
                    "age": 18,
                    "birth": "1998-02-01"
                  },
                  {
                    "name": "jack"
                  }
                ]
                """);
        }
    }
}
