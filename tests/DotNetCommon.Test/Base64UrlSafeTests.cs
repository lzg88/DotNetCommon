﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class Base64UrlSafeTests
    {
        [Test]
        public void Test()
        {
            Base64UrlSafe.Encode(new byte[] { 1, 15, 30, 40, 0, 13, 10, 43 }).ShouldBe("AQ8eKAANCis");
            Base64UrlSafe.Decode("AQ8eKAANCis").ShouldBe(new byte[] { 1, 15, 30, 40, 0, 13, 10, 43 });
            //demo jwt反序列化 jwt中存在中划线
            var jwtpayload = "eyJpZCI6IjEwMDExMjciLCJuYSI6IuWwj-aYjiIsInBvbGljeSI6IiIsInN1YiI6Ik1pY3Jvc29mdCIsImp0aSI6IjcxMDJkNDJlLTM4OTktNGI5ZS05MzlmLTIyZDA1ZmYzNjU4ZCIsImlhdCI6IjIwMjEvNS8yNSAxNDo1Mjo0OSIsInVuIjoiMjcyMzNFRThBMTZBMDg0NDVFNzBEMDQ1NEMyMzMxQjNFMjVGRkYiLCJzYyI6IklNUyIsInBjIjoicGMiLCJleHAiOjE2MjE5ODU1NjksImlzcyI6Ik1pY3Jvc29mdCJ9";
            var bs = Base64UrlSafe.Decode(jwtpayload);
            var str = UTF8Encoding.UTF8.GetString(bs);

            //demo jwt序列化
            var jwtpayload_src = new { id = "1001127", na = "小明", policy = "", sub = "Microsoft", jti = "7102d42e-3899-4b9e-939f-22d05ff3658d", iat = "2021/5/25 14:52:49", un = "27233EE8A16A08445E70D0454C2331B3E25FFF", sc = "IMS", pc = "pc", exp = 1621985569, iss = "Microsoft" };
            var json = jwtpayload_src.ToJson();
            var res = Base64UrlSafe.Encode(UTF8Encoding.UTF8.GetBytes(json));
        }
    }
}
