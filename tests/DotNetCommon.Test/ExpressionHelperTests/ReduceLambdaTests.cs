﻿using DotNetCommon.Extensions;
using DotNetCommon.Logger;
using Model3;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Model3
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Teacher2 : Teacher
    {
        public int Age { get; set; }
    }
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeacherId { get; set; }
    }

    public class Person
    {
        public Person(int age)
        {
            Age = age;
        }

        public int Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }

        public int? Score { get; set; }
    }
    public class Person2
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Name { get; set; }
    }
}

namespace DotNetCommon.Test.ExpressionHelperTests
{

    #region 各类运算
    //具有短路功能的: && || ?: ??
    internal class EveryNodeTypeTests
    {
        #region 算数运算
        [Test]
        public void SuanShuTest()
        {
            Expression<Func<Person, bool>> expression = null;
            var t = new Teacher() { Id = 5 };
            var num = 2;
            //+            
            expression = p => p.Id + (2 + num) > t.Id + 5;
            var res = ExpressionHelper.ReduceLambda(expression);
            var str = res.exp.ToString();
            str.ShouldBe("p => ((p.Id + Param_0) > Param_1)");

            //-
            expression = p => p.Id + (2 - num) > t.Id - 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => ((p.Id + Param_0) > Param_1)");

            //*
            expression = p => p.Id + (2 * num) > t.Id * 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => ((p.Id + Param_0) > Param_1)");

            // /
            expression = p => p.Id + (2 / num) > t.Id / 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => ((p.Id + Param_0) > Param_1)");

            // %
            expression = p => p.Id + (2 % num) > t.Id % 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => ((p.Id + Param_0) > Param_1)");

            //UnaryPlusVisit +
            Expression<Func<Person, object>> exp2 = null;
            exp2 = p => (+p.Id) & (+num + t.Id) & (3 - +t.Id);
            res = ExpressionHelper.ReduceLambda(exp2);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((p.Id & Param_0) & Param_1), Object)");

            //NegateVisit
            exp2 = null;
            exp2 = p => (-p.Id) & (-num + t.Id) & (3 - -t.Id);
            res = ExpressionHelper.ReduceLambda(exp2);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((-p.Id & Param_0) & Param_1), Object)");
        }
        #endregion
        #region 逻辑运算
        [Test]
        public void LuoJiTest()
        {
            Expression<Func<Person, bool>> expression = null;
            var t = new Teacher() { Id = 5 };
            var num = 2;
            // &&
            expression = p => p.Id >= 5 && num > 1 && t.Id < 6;
            var res = ExpressionHelper.ReduceLambda(expression);
            var str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) AndAlso Param_0) AndAlso Param_1)");

            //短路&&
            expression = p => p.Id >= 5 && num > 1 && t.Id < 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("False");

            // ||
            expression = p => p.Id >= 5 || num > 3 || t.Id < 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) OrElse Param_0) OrElse Param_1)");

            // 短路||
            expression = p => p.Id >= 5 || num > 1 || t.Id < 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("True");

            // !
            expression = p => !(p.Id >= 5) && !(num > 3);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (Not((p.Id >= 5)) AndAlso Param_0)");

            // &
            expression = p => p.Id >= 5 & num > 1 & t.Id < 6;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) And Param_0) And Param_1)");

            // 不能短路&
            expression = p => p.Id >= 5 & num > 3 & t.Id < 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) And Param_0) And Param_1)");

            // |
            expression = p => p.Id >= 5 | num > 3 | t.Id < 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) Or Param_0) Or Param_1)");

            // 不能短路|
            expression = p => p.Id >= 5 | num > 1 | t.Id < 6;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) Or Param_0) Or Param_1)");
        }
        #endregion
        #region 位运算
        [Test]
        public void WeiTest()
        {
            Expression<Func<Person, bool>> expression = null;
            var t = new Teacher() { Id = 5 };
            var num = 8;
            // <<
            expression = p => (p.Id << 5) > (num << 1) & (num << 1) > 4;
            var res = ExpressionHelper.ReduceLambda(expression);
            var str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id << 5) > Param_0) And Param_1)");

            // >>
            expression = p => (p.Id >> 5) > (num >> 1) & (num >> 1) > 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >> 5) > Param_0) And Param_1)");

            // ^
            expression = p => (p.Id ^ 5) > (num ^ 1) & (num ^ 1) > 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id ^ 5) > Param_0) And Param_1)");

            // ~
            expression = p => (~p.Id) > (~num) & (~num) > 4;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => ((Not(p.Id) > Param_0) And Param_1)");
        }
        #endregion
        #region 关系运算
        [Test]
        public void GuanXiTest()
        {
            Expression<Func<Person, bool>> expression = null;
            var t = new Teacher() { Id = 5 };
            var num = 8;
            //==
            expression = p => p.Id == 5 & num == 8 & t.Id == 5;
            var res = ExpressionHelper.ReduceLambda(expression);
            var str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id == 5) And Param_0) And Param_1)");

            // !=
            expression = p => p.Id != 5 & num != 8 & t.Id != 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id != 5) And Param_0) And Param_1)");

            // <
            expression = p => p.Id < 5 & num < 8 & t.Id < 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id < 5) And Param_0) And Param_1)");

            // <=
            expression = p => p.Id <= 5 & num <= 8 & t.Id <= 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id <= 5) And Param_0) And Param_1)");

            // >
            expression = p => p.Id > 5 & num > 8 & t.Id > 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id > 5) And Param_0) And Param_1)");

            // >=
            expression = p => p.Id >= 5 & num >= 8 & t.Id >= 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => (((p.Id >= 5) And Param_0) And Param_1)");
        }
        #endregion
        #region 其他
        [Test]
        public void QiTaTest()
        {
            Expression<Func<Person, object>> expression = null;
            var t = new Teacher() { Id = 5 };
            var arr = new[] { t, t };
            var num = 8;
            var json = string.Empty;
            var str = string.Empty;

            //arr[index]
            expression = p => p.Id > arr[1].Id & arr[0].Id == 5 & arr[0].Id < 6;
            var res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((((p.Id > Param_0) And Param_1) And Param_2), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[5,true,true]");

            //arr.Length
            expression = p => p.Id > arr.Length & arr.Length == 5 & arr.Length < 6;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((((p.Id > Param_0) And Param_1) And Param_2), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[2,false,true]");

            //Call
            expression = p => TestCallClass.Add(p.Id, 1) & TestCallClass.Add(num, 5) & (TestCallClass.Add(t.Id, num) | t.Id);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((Add(p.Id, 1) & Param_0) & Param_1), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[13,13]");

            //?? (也具有短路功能)
            int? ok = null;
            int? notOk = 2;
            expression = p => p.Score ?? 2 + ok ?? 5;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((p.Score ?? Param_0), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[5]");

            expression = p => (p.Score ?? 2) + (notOk ?? 1 + p.Id);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((p.Score ?? 2) + (Param_0 ?? (1 + p.Id))), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[2]");

            expression = p => p.Score ?? (2 + (notOk ?? 1 + p.Id));
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((p.Score ?? (2 + (Param_0 ?? (1 + p.Id)))), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[2]");

            //?:(也具有短路功能)
            expression = p => p.Id > 5 ? 2 : (num > 3 ? t.Id : p.Id + 3);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(IIF((p.Id > 5), 2, Param_0), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[true,5,5]");

            //constant
            //convert
            var f = 2.3f;
            expression = p => p.Id - (int)(f + t.Id) - (float)num;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((Convert((p.Id - Param_0), Single) - Param_1), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[7,8]");

            //default
            expression = p => p.Id + default(int) - (default(float) + num) - (default(int) + t.Id);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((Convert((p.Id + 0), Single) - Param_0) - Param_1), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[8,5]");

            //IndexVisit
            var dic = new Dictionary<int, int>() { { 1, 1 }, { 2, 2 }, { 5, 5 }, { 8, 8 } };
            expression = p => p.Id + dic[t.Id] + dic[num];
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((p.Id + Param_0) + Param_1), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[5,8]");

            //InvokeVisit
            Func<int, int, int> add = (x, y) => x + y;
            expression = p => add(p.Id, 2) + add(num, t.Id) + add(t.Id, 5);
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((Invoke(Param_0, p.Id, 2) + Param_1) + Param_2), Object)");
            json = res.midValues.Where(i => i.Value.GetType().IsSimpleType()).Select(i => i.Value).ToJson();
            json.ShouldBe("[13,10]");

            //LambdaVisit
            //ListInitVisit
            //new List 的构造参数被简化,第一个Person的构造参数被简化,第二个Person整体变成一个常量
            Expression<Func<int, List<Person>>> exp = i => new List<Person>(num - 1) {
                new Person(num+3){Id=i},
                new Person(num+1){Id=num,Name="小明"}
            };
            res = ExpressionHelper.ReduceLambda(exp);
            str = res.exp.ToString();
            str.ShouldBe("i => new List`1(Param_0) {Void Add(Model3.Person)(new Person(Param_1) {Id = i}), Void Add(Model3.Person)(Param_2)}");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[11,7,{\"Id\":8,\"Age\":9,\"Name\":\"小明\",\"Score\":null}]");

            //MemberAccessVisit
            expression = p => p.Id + 5 - t.Id;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((p.Id + 5) - Param_0), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[5]");

            //MemberInitVisit
            //构造函数中的参数可以简化,但memberInit整体不能简化
            Expression<Func<int, Person>> exp2 = i => new Person(num + 3)
            {
                Id = i,
                Name = "小明"
            };
            res = ExpressionHelper.ReduceLambda(exp2);
            str = res.exp.ToString();
            str.ShouldBe("i => new Person(Param_0) {Id = i, Name = \"小明\"}");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[11]");

            //NewArrayBoundsVisit
            expression = p => new int[p.Id, 2 + t.Id, num - 3];
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => new System.Int32[,,](p.Id, Param_0, Param_1)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[7,5]");

            expression = p => new int[num, 2 + t.Id, num - 3].Length > 1;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("True");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[]");

            //NewArrayInitVisit
            //因为 当使用 NewArrayInit 即: 创建数组并初始化值的时候,要么不指定数组的大小,要么指定数组的大小为一常量值且与初始化的个数一致,所以,对 NewArrayInit 的处理与其他的一样
            Expression<Func<int, Person[]>> exp3 = i => new Person[2]
            {
                new Person(num+3){Id=i},//number+3 被求值
                new Person(num+1){Id=num,Name="小明"}//整体变为常量
            };
            res = ExpressionHelper.ReduceLambda(exp3);
            str = res.exp.ToString();
            str.ShouldBe("i => new [] {new Person(Param_0) {Id = i}, Param_1}");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[11,{\"Id\":8,\"Age\":9,\"Name\":\"小明\",\"Score\":null}]");

            //NewVisit
            expression = p => new Teacher().Id + new Teacher().Id == 0 ? t.Id : num - 2;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("5");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[]");

            //ParameterVisit
            //TypeAsVisit
            object obj = new Teacher2()
            {
                Id = 1,
                Age = 10
            };
            expression = p => (obj as Teacher).Id + p.Id + (obj as Teacher2).Age + t.Id;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert((((Param_0 + p.Id) + Param_1) + Param_2), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[1,10,5]");

            //TypeIsVisit
            expression = p => obj is Teacher & p.Id > 10 & obj is Teacher2;
            res = ExpressionHelper.ReduceLambda(expression);
            str = res.exp.ToString();
            str.ShouldBe("p => Convert(((Param_0 And (p.Id > 10)) And Param_1), Object)");
            json = res.midValues.Select(i => i.Value).ToJson();
            json.ShouldBe("[true,true]");
        }
        #endregion

        #region lambda表达式嵌套
        class Person2
        {
            public int Id { get; set; }
            public int Age { get; set; }
        }
        [Test]
        public void NestedLambdaTest()
        {
            //内嵌lambda表达式
            var flag = 1 > 0;
            Expression<Func<IEnumerable<Person2>, bool>> lambda2 = (IEnumerable<Person2> i) => i.Count() > 0 && i.Where(p => p.Id > 100 && flag).Any();
            var ret2 = ExpressionHelper.ReduceLambda(lambda2);
            ret2.exp.ToString().ShouldBe("i => ((i.Count() > 0) AndAlso i.Where(p => ((p.Id > 100) AndAlso Param_0)).Any())");
            ret2.midValues.Count.ShouldBe(1);
            ret2.midValues.FirstOrDefault().Value.ShouldBe(true);

            Expression<Func<IEnumerable<Person2>, object>> lambda = (IEnumerable<Person2> i) => i.Where(p => p.Id > 100 || flag);
            var ret = ExpressionHelper.ReduceLambda(lambda);
            ret.exp.ToString().ShouldBe("i => i.Where(p => True)");
            var del = (ret.exp as LambdaExpression).Compile();
            var res = del.DynamicInvoke(new[] { new List<Person2> { new Person2() { Id = 2, Age = 18 } } }) as IEnumerable<Person2>;
            res.Count().ShouldBe(1);
            res.FirstOrDefault().Id.ShouldBe(2);
            res.FirstOrDefault().Age.ShouldBe(18);
        }

        [Test]
        public void NestedLambdaTest2()
        {
            var list = new List<int> { 1, 2, 3 };
            Expression<Func<int, int>> exp = (x) => x + list.Where(i => i > 1).FirstOrDefault();
            var res = ExpressionHelper.ReduceLambda(exp);
            res.exp.ToString().ShouldBe("x => (x + Param_0)");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ShouldBe(2);

            Expression<Func<int, int>> exp2 = (x) => x + TestLambdaMethodWhere(i => i > 1).FirstOrDefault();
            var res2 = ExpressionHelper.ReduceLambda(exp2);
            res2.exp.ToString().ShouldBe("x => (x + Param_0)");
            res2.midValues.Count.ShouldBe(1);
            res2.midValues.FirstOrDefault().Value.ShouldBe(2);
        }

        public List<int> TestLambdaMethodWhere(Expression<Func<int, bool>> exp)
        {
            return Enumerable.Range(1, 100).Where(exp.Compile()).ToList();
        }
        #endregion

        #region canReduce
        class Person8
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public List<Person8> Children { get; set; }
        }


        [Test]
        public void IsKeepTest()
        {
            var names = new List<string>() { "tom", "lisa" };
            Expression<Func<Person8, Person8>> lambda = p => new Person8
            {
                Id = p.Id + 1,
                Name = names.FirstOrDefault(),
                Children = new List<Person8>
                {
                    new Person8() { Id = names.Count},
                    p
                }
            };
            var res = ExpressionHelper.ReduceLambda(lambda, exp => exp.NodeType == ExpressionType.MemberInit);
            res.exp.ToString().ShouldBe("p => new Person8() {Id = (p.Id + 1), Name = Param_0, Children = new List`1() {Void Add(Person8)(new Person8() {Id = Param_1}), Void Add(Person8)(p)}}");
            res.midValues.Count().ShouldBe(2);
            res.midValues.FirstOrDefault().Value.ShouldBe(2);
            res.midValues.LastOrDefault().Value.ShouldBe("tom");

            lambda = p => new Person8
            {
                Id = p.Id + 1,
                Name = names.FirstOrDefault(),
                Children = new List<Person8>
                {
                    new Person8() { Id = names.Count},
                    new Person8() { Id = names.Count},
                }
            };
            res = ExpressionHelper.ReduceLambda(lambda, exp => exp.NodeType == ExpressionType.ListInit);
            res.exp.ToString().ShouldBe("p => new Person8() {Id = (p.Id + 1), Name = Param_0, Children = new List`1() {Void Add(Person8)(Param_1), Void Add(Person8)(Param_2)}}");
            res.midValues.Count.ShouldBe(3);
            res.midValues.Count(i => (i.Value is Person8 p) && p.Id == 2).ShouldBe(2);
            res.midValues.Count(i => (i.Value as string) == "tom").ShouldBe(1);
        }

        [Test]
        public void IsKeepTest2()
        {
            Expression<Func<Person8, Person8>> lambda = p => new Person8
            {
                Id = 1,
                Name = "a",
            };

            Expression<Func<Person8>> lambda2 = () => new Person8
            {
                Id = 1,
                Name = "a",
            };
            Expression<Func<object>> lambda3 = () => new
            {
                Id = 1,
                Name = "a",
            };
            var res = ExpressionHelper.ReduceLambda(lambda, exp => exp.NodeType == ExpressionType.MemberInit);
            res.exp.ToString().ShouldBe("p => new Person8() {Id = 1, Name = \"a\"}");
            var res2 = ExpressionHelper.ReduceLambda(lambda2, exp => exp.NodeType == ExpressionType.MemberInit);
            res2.exp.ToString().ShouldBe("() => new Person8() {Id = 1, Name = \"a\"}");
            var res3 = ExpressionHelper.ReduceLambda(lambda3, exp => exp.NodeType == ExpressionType.New && exp.Type.IsAnonymous());
            res3.exp.ToString().ShouldBe("() => new <>f__AnonymousType2`2(Id = 1, Name = \"a\")");
        }

        #endregion

        [Test]
        public void TestMideValuesBug()
        {
            var msg = "";
            LoggerFactory.SetLogger(ctx =>
            {
                msg += $"{ctx.CategoryName} {ctx.Message}\r\n";
            }, false);
            var xp = "12";
            Expression<Func<string, string>> exp = (str) => "小明" + xp + str;
            var res = ExpressionHelper.ReduceLambda(exp);
            res.exp.ToString().ShouldBe("str => (Param_0 + str)");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ShouldBe("小明12");

            Expression<Func<string, string>> exp3 = (str) => "小红" + xp + str;
            res = ExpressionHelper.ReduceLambda(exp3);
            res.exp.ToString().ShouldBe("str => (Param_0 + str)");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ShouldBe("小红12");

            var list = new List<int> { 1, 2, 3 };
            Expression<Func<string, bool>> exp5 = (p) => p == "none" || new Dictionary<string, object>() { { "name", "小明" }, { "age", list.FirstOrDefault() } }.ContainsKey(p);
            res = ExpressionHelper.ReduceLambda(exp5);
            res.exp.ToString().ShouldBe("p => ((p == \"none\") OrElse Param_0.ContainsKey(p))");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ToJson().ShouldBe("{\"name\":\"小明\",\"age\":1}");

            //触发了,但没有编译,用的是缓存的
            list.RemoveAtFluent(0);
            exp5 = (p) => p == "none" || new Dictionary<string, object>() { { "name", "小明" }, { "age", list.FirstOrDefault() } }.ContainsKey(p);
            res = ExpressionHelper.ReduceLambda(exp5);
            res.exp.ToString().ShouldBe("p => ((p == \"none\") OrElse Param_0.ContainsKey(p))");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ToJson().ShouldBe("{\"name\":\"小明\",\"age\":2}");
            //判断 exp5 仅触发了一次 lambda.Compile
            Assert.IsTrue(msg.IndexOf("System.Collections.Generic.Dictionary") == msg.LastIndexOf("System.Collections.Generic.Dictionary"));
        }

        [Test]
        public void Test2()
        {
            var xp = "12";
            Expression<Func<string, string>> exp = (str) => "小明" + xp + str.Length.ToString();
            var res = ExpressionHelper.ReduceLambda(exp);
            res.exp.ToString().ShouldBe("str => (Param_0 + str.Length.ToString())");
            res.midValues.Count.ShouldBe(1);
            res.midValues.FirstOrDefault().Value.ShouldBe("小明12");
        }
    }
    #endregion

    #region 复杂场景
    [TestFixture]
    public class ComplexTests
    {
        [Test]
        public void Test()
        {
            var ids = new List<int>() { 1, 2, 3 };
            Expression<Func<Person, bool>> expression = p => p.Id < 100 || ids.Count > 1;
            var res = ExpressionHelper.ReduceLambda(expression);
            res.exp.NodeType.ShouldBe(ExpressionType.Constant);
            (res.exp as ConstantExpression).Value.ShouldBe(true);
        }
    }
    #endregion

    public class TestCallClass
    {
        public static int Add(int x, int y) => x + y;
    }

}
