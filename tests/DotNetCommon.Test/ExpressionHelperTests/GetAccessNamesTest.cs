﻿using DotNetCommon.Extensions;
using DotNetCommon.Test.ExpressionHelperTests.Model;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCommon.Test.ExpressionHelperTests
{
    namespace Model
    {
        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public object Obj { get; set; }
            public int Age { get; set; }
            public DateTime? Birth { get; set; }
            public TaskStatus Status { get; set; }
            public Person InnerPerson { get; set; }
        }
    }

    [TestFixture]
    internal class GetAccessNamesTest
    {
        [Test]
        public void OneParamenterTest()
        {
            Expression<Func<Person, Object>> expression = p => p.Id;
            var cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Id" });

            expression = p => new { p.Id, p.Name };
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Id", "Name" });

            expression = p => new { p.Id, p.Name, p.Obj };
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Id", "Name", "Obj" });

            expression = p => p.Id + p.Name + p.Obj;
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Id", "Name", "Obj" });

            expression = p => p.Age + p.InnerPerson.Id;
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Age", "InnerPerson" });

            expression = p => p.Id > 0 ? p.Name : $"小明{p.Birth}";
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>() { "Id", "Name", "Birth" });

            expression = p => p;
            cols = ExpressionHelper.GetAccessNames(expression);
            cols.ShouldBe(new List<string>());
        }

        [Test]
        public void TwoParamenterTest()
        {
            Expression<Func<Person, Person, Object>> expression = (p1, p2) => p1.Id;
            var cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("Id");
            cols[1].ToStringSeparated(",").ShouldBe("");

            expression = (p1, p2) => new { p1.Id, p2.Name };
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("Id");
            cols[1].ToStringSeparated(",").ShouldBe("Name");

            expression = (p1, p2) => new { p1.Id, p2.Name, p2.Obj };
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("Id");
            cols[1].ToStringSeparated(",").ShouldBe("Name,Obj");

            expression = (p1, p2) => p1.Id + p2.Name + p1.Obj;
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("Id,Obj");
            cols[1].ToStringSeparated(",").ShouldBe("Name");

            expression = (p1, p2) => p2.Age + p1.InnerPerson.Id;
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("InnerPerson");
            cols[1].ToStringSeparated(",").ShouldBe("Age");

            expression = (p1, p2) => p1.Id > 0 ? p2.Name : $"小明{p2.Birth}";
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("Id");
            cols[1].ToStringSeparated(",").ShouldBe("Name,Birth");

            expression = (p1, p2) => p1;
            cols = ExpressionHelper.GetAccessMutilNames(expression);
            cols[0].ToStringSeparated(",").ShouldBe("");
            cols[1].ToStringSeparated(",").ShouldBe("");
        }
    }
}
