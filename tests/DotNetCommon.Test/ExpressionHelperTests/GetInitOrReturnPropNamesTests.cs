﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DotNetCommon.Test.ExpressionHelperTests.Model2;
using Shouldly;
using System.Xml.Linq;

namespace DotNetCommon.Test.ExpressionHelperTests
{
    namespace Model2
    {
        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public object Obj { get; set; }
            public int Age { get; set; }
            public DateTime? Birth { get; set; }
            public TaskStatus Status { get; set; }
            public Person InnerPerson { get; set; }
        }
    }

    [TestFixture]
    internal class GetInitOrReturnPropNamesTests
    {
        [Test]
        public void Test()
        {
            Expression<Func<Person>> expression = () => new Person
            {
                Id = 1,
                Name = "",
                Age = 2
            };
            var names = ExpressionHelper.GetInitOrReturnPropNames(expression);
            names.ShouldBe(new List<string> { "Id", "Name", "Age" });

            Expression<Func<Person, Person>> expression2 = (p) => new Person
            {
                Id = 1,
                Name = "",
                Age = 2
            };
            names = ExpressionHelper.GetInitOrReturnPropNames(expression2);
            names.ShouldBe(new List<string> { "Id", "Name", "Age" });

            expression2 = p => p;
            names = ExpressionHelper.GetInitOrReturnPropNames(expression2);
            names.ShouldBe(new List<string> { });

            Expression<Func<Person, Object>> expression3 = (p) => p.Id;
            names = ExpressionHelper.GetInitOrReturnPropNames(expression3);
            names.ShouldBe(new List<string> { "Id" });

            expression3 = (p) => new { p.Id, p.Name };
            names = ExpressionHelper.GetInitOrReturnPropNames(expression3);
            names.ShouldBe(new List<string> { "Id", "Name" });

            expression3 = (p) => new { Id2 = p.Id, Name2 = p.Name };
            names = ExpressionHelper.GetInitOrReturnPropNames(expression3);
            names.ShouldBe(new List<string> { "Id2", "Name2" });
        }
    }
}
