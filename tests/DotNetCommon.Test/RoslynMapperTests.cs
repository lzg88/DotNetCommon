﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class RoslynMapperTests
    {
        [Test]
        public void TestNeedContainer()
        {
            /*
            循环引用,两个转换方法都需要防循环
            A       =>  A1
              B     =>      B1
                 A  =>         A1
            */
            var res = new RoslynMapper().GenerateMapCode(typeof(A), typeof(A1));
            var list = res.Methods.Where(i => i.IsToArray || i.IsToList).ToList();
            list.Count.ShouldBe(4);
            list.ForEach(i => i.NeedContainer.ShouldBeFalse());

            list = res.Methods.Where(i => i.IsToArray == false && i.IsToList == false).ToList();
            list.Count.ShouldBe(2);
            list.ForEach(i => i.NeedContainer.ShouldBeTrue());

            /* C需要防循环,D/E/F/G均不需要,这样需要分裂4个D/E/F/G出来
            C         =>    C1
              D       =>       D1
                 F    =>          F1
              E       =>       E1
                 G    =>          G1
                 D    =>          D1           
            */
            res = new RoslynMapper().GenerateMapCode(typeof(C), typeof(C1));
            list = res.Methods.Where(i => i.IsToArray || i.IsToList).ToList();
            list.Count.ShouldBe(10);
            list.ForEach(i => i.NeedContainer.ShouldBeFalse());

            list = res.Methods.Where(i => i.IsToArray == false && i.IsToList == false).ToList();
            //5+4(分裂的)
            list.Count.ShouldBe(9);
            list.First(i => i.SrcType == typeof(C) && i.DestType == typeof(C1)).NeedContainer.ShouldBeTrue();

            var groups = list.GroupBy(i => (i.SrcType, i.DestType)).ToList();

            //C -> C1
            var g = groups.Where(i => i.Key.SrcType == typeof(C) && i.Key.DestType == typeof(C1)).FirstOrDefault().ToList();
            g.Count.ShouldBe(1);
            g.FirstOrDefault().NeedContainer.ShouldBeTrue();
            g.FirstOrDefault().IsExpose.ShouldBeTrue();

            //D -> D1
            g = groups.Where(i => i.Key.SrcType == typeof(D) && i.Key.DestType == typeof(D1)).FirstOrDefault().ToList();
            g.Count.ShouldBe(2);
            g[0].NeedContainer.ShouldBeTrue();
            g[0].IsExpose.ShouldBeFalse();
            g[1].NeedContainer.ShouldBeFalse();
            g[1].IsExpose.ShouldBeTrue();

            //E -> E1
            g = groups.Where(i => i.Key.SrcType == typeof(E) && i.Key.DestType == typeof(E1)).FirstOrDefault().ToList();
            g.Count.ShouldBe(2);
            g[0].NeedContainer.ShouldBeTrue();
            g[0].IsExpose.ShouldBeFalse();
            g[1].NeedContainer.ShouldBeFalse();
            g[1].IsExpose.ShouldBeTrue();

            //F -> F1
            g = groups.Where(i => i.Key.SrcType == typeof(F) && i.Key.DestType == typeof(F1)).FirstOrDefault().ToList();
            g.Count.ShouldBe(2);
            g[0].NeedContainer.ShouldBeTrue();
            g[0].IsExpose.ShouldBeFalse();
            g[1].NeedContainer.ShouldBeFalse();
            g[1].IsExpose.ShouldBeTrue();

            //G -> G1
            g = groups.Where(i => i.Key.SrcType == typeof(G) && i.Key.DestType == typeof(G1)).FirstOrDefault().ToList();
            g.Count.ShouldBe(2);
            g[0].NeedContainer.ShouldBeTrue();
            g[0].IsExpose.ShouldBeFalse();
            g[1].NeedContainer.ShouldBeFalse();
            g[1].IsExpose.ShouldBeTrue();
        }

        #region 模型
        #region 容器1
        public class A
        {
            public B B { get; set; }
        }
        public class B
        {
            public A A { get; set; }
        }

        public class A1
        {
            public B1 B { get; set; }
        }

        public class B1
        {
            public A1 A { get; set; }
        }
        #endregion
        #region 容器2
        public class C { public D D { get; set; } public E E { get; set; } public int Id { get; set; } }
        public class D { public F F { get; set; } public int Id { get; set; } }
        public class E { public G G { get; set; } public D D { get; set; } public int Id { get; set; } }
        public class F { public int Id { get; set; } }
        public class G { public int Id { get; set; } }

        public class C1 { public D1 D { get; set; } public E1 E { get; set; } public int Id { get; set; } }
        public class D1 { public F1 F { get; set; } public int Id { get; set; } }
        public class E1 { public G1 G { get; set; } public D1 D { get; set; } public int Id { get; set; } }
        public class F1 { public int Id { get; set; } }
        public class G1 { public int Id { get; set; } }

        #endregion

        #endregion
    }
}
