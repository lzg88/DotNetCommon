﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;
using Shouldly;
using System;
using DotNetCommon.Extensions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Text.Json;
using System.Collections.Generic;

namespace DotNetCommon.Test.Serialize
{
    [TestFixture]
    internal sealed class SerializeTests
    {
        /// <summary>
        /// NumberConverter、DateTimeConverter
        /// </summary>
        [Test]
        public void ToJsonTest()
        {
            var obj = new Person()
            {
                Id = long.MaxValue,
                Name = "小明",
                State = EnumState.Close,
                Birth = DateTime.Parse("2023-06-04 15:52:01.1234567 +08:00"),
                Addr = "天明路"
            };
            var str = obj.ToJson();
            str.ShouldBe("{\"Id\":9223372036854775807,\"Birth\":\"2023-06-04T15:52:01.1234567+08:00\",\"Addr\":\"天明路\",\"Name\":\"小明\",\"StudentId\":0,\"ClassId\":null,\"State\":1}");
            var str2 = str.ToObject<JObject>().ToJson();
            str2.ShouldBe(str);
        }


        [Test]
        public void ToJsonFastTest()
        {
            //system.text.json 默认:
            var str = System.Text.Json.JsonSerializer.Serialize(DateTime.Parse("2023-06-04 16:40:17"));
            str.ShouldBe("\"2023-06-04T16:40:17\"");
            var str3 = System.Text.Json.JsonSerializer.Serialize(DateTime.Parse("2023-06-04"));
            str3.ShouldBe("\"2023-06-04T00:00:00\"");

            //datetime.tostring 默认:
            var s = DateTime.Parse("2023-06-04 16:40:17").ToString();
            s.ShouldBe("2023/6/4 16:40:17");
            var s3 = DateTime.Parse("2023-06-04").ToString();
            s3.ShouldBe("2023/6/4 0:00:00");

            //newtonsoft 默认
            var n = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("2023-06-04 16:40:17"));
            n.ShouldBe("\"2023-06-04T16:40:17\"");
            var n3 = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("2023-06-04"));
            n3.ShouldBe("\"2023-06-04T00:00:00\"");

            //修饰 system.text.json 后
            var options = JsonHelper.GetDefaultJsonSerializerOptions();
            var str2 = System.Text.Json.JsonSerializer.Serialize(DateTime.Parse("2023-06-04 16:40:17"), options);
            str2.ShouldBe("\"2023-06-04T16:40:17.0000000\"");
            var str4 = System.Text.Json.JsonSerializer.Serialize(DateTime.Parse("2023-06-04"), options);
            str4.ShouldBe("\"2023-06-04T00:00:00.0000000\"");

            var person = new Person2()
            {
                PSByte = (sbyte)-1,
                PSByteNull = (sbyte?)-2,
                PByte = (byte)1,
                PByteNull = (byte?)2,

                PShort = (short)3,
                PShortNull = (short?)4,
                PUShort = (ushort)5,
                PUShortNull = (ushort)6,

                PInt = 7,
                PUInt = 8,
                PIntNull = 9,
                PUIntNull = 10,

                PLong = 11,
                PULong = 12,
                PLongNull = 13,
                PULongNull = 14,

                PFloat = 15.1f,
                PUFloat = 16.1234567890f,

                PDouble = 17.1234567890,
                PDoubleNull = 18.12345678901234567890,

                PDecimal = 19.12345678901234567890m,
                PDecimalNull = 20.123456789012345678901234567890m,

                PChar = 'a',
                PCharNull = 'b',

                PBool = false,
                PBoolNull = null,

                PString = "abc",

                PDateTime = DateTime.Parse("2023-06-04 16:40:17"),
                PDateTimeNull = DateTime.Parse("2023-06-04 16:40:17"),

            };
            var json = person.ToJsonFast();
            json.ShouldBe("{\"PByte\":1,\"PByteNull\":2,\"PSByte\":-1,\"PSByteNull\":-2,\"PShort\":3,\"PShortNull\":4,\"PUShort\":5,\"PUShortNull\":6,\"PInt\":7,\"PUInt\":8,\"PIntNull\":9,\"PUIntNull\":10,\"PLong\":11,\"PULong\":12,\"PLongNull\":13,\"PULongNull\":14,\"PFloat\":15.1,\"PUFloat\":16.123457,\"PDouble\":17.123456789,\"PDoubleNull\":18.123456789012344,\"PDecimal\":19.12345678901234567890,\"PDecimalNull\":20.123456789012345678901234568,\"PChar\":\"a\",\"PCharNull\":\"b\",\"PBool\":false,\"PBoolNull\":null,\"PString\":\"abc\",\"PDateTime\":\"2023-06-04T16:40:17.0000000\",\"PDateTimeNull\":\"2023-06-04T16:40:17.0000000\",\"State\":0}");

            var p = "{\"PCharNull\":\"b\"}".ToObject<Person2>();

            json.ToObject<Person2>().ToJson().ShouldBe(json);

            var obj = new TestJObject
            {
                Id = 1,
                Name = "小明",
                BirthDay = DateTime.Parse("2023-06-04 16:43:01"),
                State = EnumState.Open
            };

            json = obj.ToJson();
            json.ShouldBe("{\"Id\":1,\"Name\":\"小明\",\"BirthDay\":\"2023-06-04T16:43:01.0000000\",\"NullObj\":null,\"State\":2}");
            var json2 = json.ToObject<JObject>().ToJsonFast(dateTimeFormatString: "yyyy-MM-dd", ignoreNull: true, enum2String: true, lowerCamelCase: true, isIntend: true);
            json2.ShouldBe("{\r\n  \"Id\": 1,\r\n  \"Name\": \"小明\",\r\n  \"BirthDay\": \"2023-06-04\",\r\n  \"NullObj\": null,\r\n  \"State\": 2\r\n}");
        }

        [Test]
        public void TestBaseSetting()
        {
            new { Num = 3 }.ToJsonFast(number2String: true).ShouldBe("{\"Num\":\"3\"}");
            new { State = EnumState.Open }.ToJsonFast(enum2String: true).ShouldBe("{\"State\":\"Open\"}");
            new { Birth = DateTime.Parse("2023-09-22") }.ToJsonFast(dateTimeFormatString: "yyyy-MM-dd").ShouldBe("{\"Birth\":\"2023-09-22\"}");
            new { Name = "tom" }.ToJsonFast(lowerCamelCase: true).ShouldBe("{\"name\":\"tom\"}");
            new { Dic = new Dictionary<string, object> { { "Name", "tom" }, { "Age", 18 } } }.ToJsonFast(lowerCamelCaseDictionaryKey: true).ShouldBe("{\"Dic\":{\"name\":\"tom\",\"age\":18}}");
        }

        public class TestJObject
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime BirthDay { get; set; }
            public object NullObj { get; set; }
            public EnumState State { get; set; }
        }

        #region Model1
        public class Person
        {
            public long Id { get; set; }
            public DateTime Birth { get; set; }
            public string Addr { set; get; }
            public string Name { set; get; }
            public int StudentId { get; set; }
            public int? ClassId { get; set; }
            public EnumState State { set; get; }
        }

        public enum EnumState
        {
            Active,
            Close,
            Open
        }
        public class Person2
        {
            public byte PByte { get; set; }
            public byte? PByteNull { get; set; }
            public sbyte PSByte { get; set; }
            public sbyte? PSByteNull { get; set; }

            public short PShort { get; set; }
            public short? PShortNull { get; set; }
            public ushort PUShort { get; set; }
            public ushort? PUShortNull { get; set; }

            public int PInt { get; set; }
            public uint PUInt { get; set; }
            public int? PIntNull { get; set; }
            public uint? PUIntNull { get; set; }

            public long PLong { get; set; }
            public ulong PULong { get; set; }
            public long? PLongNull { get; set; }
            public ulong? PULongNull { get; set; }

            public float PFloat { get; set; }
            public float? PUFloat { get; set; }

            public double PDouble { get; set; }
            public double? PDoubleNull { get; set; }

            public decimal PDecimal { get; set; }
            public decimal? PDecimalNull { get; set; }

            public char PChar { get; set; }
            public char? PCharNull { get; set; }

            public bool PBool { get; set; }
            public bool? PBoolNull { get; set; }

            public string PString { get; set; }

            public DateTime PDateTime { get; set; }
            public DateTime? PDateTimeNull { get; set; }

            public EnumTest State { get; set; }

            public enum EnumTest
            {
                Open, Active, Dead
            }
        }
        #endregion
    }
}
