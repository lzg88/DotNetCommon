﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class LuanMaHelperTests
    {
        [Test]
        public void Test()
        {
            //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            LuanMaHelper.InitGBK();
            var str = "测试乱码123";
            var bs_utf8 = Encoding.UTF8.GetBytes(str);
            var bs_gbk = Encoding.GetEncoding("GBK").GetBytes(str);

            //"娴嬭瘯涔辩爜123"
            var str_futf8_tgbk = Encoding.GetEncoding("GBK").GetString(bs_utf8);
            //"��������123"
            var str_fgbk_tutf8 = Encoding.UTF8.GetString(bs_gbk);

            LuanMaHelper.IsLuanMa(str_futf8_tgbk).ShouldBe(false);
            LuanMaHelper.IsLuanMa(str_fgbk_tutf8).ShouldBe(true);


            var finalStr_ansi = LuanMaHelper.GetString(bs_utf8);
            var finalStr_utf8 = LuanMaHelper.GetString(bs_gbk);
            finalStr_ansi.ShouldBe(str);
            finalStr_utf8.ShouldBe(str);


        }

        [Test]
        public void TestFile()
        {
            var filePath = "TestFile_Guid.txt";
            var text = @"我是中国人123abcd,21啊";
            if (File.Exists(filePath)) File.Delete(filePath);
            File.WriteAllText(filePath, text, Encoding.UTF8);
            var text2 = LuanMaHelper.ReadAllText(filePath);
            text2.ShouldBe(text);

            File.Delete(filePath);
            File.WriteAllText(filePath, text, Encoding.GetEncoding("GBK"));
            text2 = LuanMaHelper.ReadAllText(filePath);
            text2.ShouldBe(text);

            var b = LuanMaHelper.Detect("—");//Unicode: 0x2014
            b.IsLuanMa.ShouldBeTrue();

            var res = LuanMaHelper.Detect("�");//Unicode: 0xFFFD
            res.Position.ShouldBe("第 1 行, 1 列出现乱码,乱码字符: [�],乱码判定规则: [0xFFFD] 不在常用Unicode范围内!");

        }

        [Test]
        public void TestGetUnicode()
        {
            var i = LuanMaHelper.GetUnicode('\0');
            i.ShouldBe(0);
            var str = LuanMaHelper.GetUnicodeHex('\0');
            str.ShouldBe("0x0000");
            i = LuanMaHelper.GetUnicode('1');
            i.ShouldBe(49);
            str = LuanMaHelper.GetUnicodeHex('1');
            str.ShouldBe("0x0031");
            i = LuanMaHelper.GetUnicode('我');
            Console.WriteLine(25105);
            str = LuanMaHelper.GetUnicodeHex('我');
            str.ShouldBe("0x6211");
            i = LuanMaHelper.GetUnicode('�');
            i.ShouldBe(65533);
            str = LuanMaHelper.GetUnicodeHex('�');
            str.ShouldBe("0xFFFD");
        }
    }
}
