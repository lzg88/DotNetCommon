﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    internal sealed class UnicodeHelperTests
    {
        [Test]
        public void Test()
        {
            var str = @"空 格_$\&^@%*";
            var res = UnicodeHelper.EnUnicode(str);
            res.ShouldBe(@"\u7a7a\u0020\u683c\u005f\u0024\u005c\u0026\u005e\u0040\u0025\u002a");
            str = UnicodeHelper.DeUnicode(res);
            str.ShouldBe(@"空 格_$\&^@%*");

            //emoji
            str = "😰👩‍🙈";
            str.Length.ShouldBe(7);
            res = UnicodeHelper.EnUnicode(str);
            res.ShouldBe(@"\ud83d\ude30\ud83d\udc69\u200d\ud83d\ude48");
            str = UnicodeHelper.DeUnicode(res);
            str.ShouldBe(@"😰👩‍🙈");

            //\u自身
            str = @"\u005f";
            res = UnicodeHelper.EnUnicode(str);
            res.ShouldBe(@"\u005c\u0075\u0030\u0030\u0035\u0066");
            str = UnicodeHelper.DeUnicode(res);
            str.ShouldBe(@"\u005f");
        }
    }
}
