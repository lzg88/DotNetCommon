﻿using DotNetCommon.Data;
using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DotNetCommon.Test
{
    [TestFixture]
    internal class AsyncLockerTests
    {
        [Test]
        public void Test()
        {
            HttpClient httpClient = new HttpClient();
            //判断加不加锁  通过对比 current 在http请求前后是否相等
            var current = 0;
            var tasks = new List<Task>();
            var res = new List<(int k, bool success)>();
            for (int i = 0; i < 10; i++)
            {
                var k = i;
                tasks.Add(Task.Run(async () =>
                {
                    //加锁的
                    await AsyncLocker.LockAsync("123", async () =>
                    {
                        current = k;
                        Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} start...");
                        var str = await httpClient.GetStringAsync("http://www.baidu.com");
                        Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} end...");
                        res.Add((k, current == k));
                    });
                    //不加锁的 
                    //current = k;
                    //Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} start...");
                    //var str = await httpClient.GetStringAsync("http://www.baidu.com");
                    //Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} end...");
                    //res.Add((k, current == k));
                }));
            }
            Task.WaitAll(tasks.ToArray());
            res.ForEach(i => i.success.ShouldBeTrue());
        }

        [Test]
        public void TestReturn()
        {
            HttpClient httpClient = new HttpClient();
            //判断加不加锁  通过对比 current 在http请求前后是否相等
            var current = 0;
            var tasks = new List<Task>();
            var res = new List<(int k, bool success)>();
            for (int i = 0; i < 10; i++)
            {
                var k = i;
                tasks.Add(Task.Run(async () =>
                {
                    //加锁的
                    var k2 = await AsyncLocker.LockRetAsync("123", async () =>
                      {
                          current = k;
                          Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} start...");
                          var str = await httpClient.GetStringAsync("http://www.baidu.com");
                          Console.WriteLine($"{DateTime.Now.ToCommonString()} Thread:{Thread.CurrentThread.ManagedThreadId} {k} end...");
                          res.Add((k, current == k));
                          return k;
                      });
                    Console.WriteLine($"k2={k2}");
                }));
            }
            Task.WaitAll(tasks.ToArray());
            res.ForEach(i => i.success.ShouldBeTrue());
        }


        [Test]
        public async Task TestTimeout()
        {
            var task1 = Task.Run(async () =>
             {
                 await AsyncLocker.LockAsync("123", async () =>
                 {
                     Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 123");
                     await Task.Delay(3000);
                     Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 456");
                 });
             });

            await Task.Delay(1000);
            try
            {
                await AsyncLocker.LockAsync("123", () =>
                {
                    Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 789");
                    return Task.CompletedTask;
                }, TimeSpan.FromSeconds(1));
                throw new Exception("not throw!!!");
            }
            catch (Exception ex)
            {
                ex.ShouldBeOfType<TimeoutException>();
                ex.Message.ShouldContain("异步锁超时(123)");
            }
            await task1;
        }

        [Test]
        public async Task TestTryLockTimeout()
        {
            var task1 = Task.Run(async () =>
            {
                var flag = await AsyncLocker.TryLockAsync("123", async () =>
                 {
                     Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 123");
                     await Task.Delay(3000);
                     Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 456");
                 }, Timeout.InfiniteTimeSpan);
                flag.ShouldBe(true);
            });

            await Task.Delay(1000);
            var flag = await AsyncLocker.TryLockAsync("123", () =>
            {
                Console.WriteLine($"{DateTime.Now.ToCommonStampString()} {Thread.CurrentThread.ManagedThreadId} 789");
                return Task.CompletedTask;
            }, TimeSpan.FromSeconds(1));
            flag.ShouldBe(false);
            await task1;
        }
    }
}