using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace DotNetCommonLoggerWebTest
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();
            DotNetCommon.Logger.LoggerFactory.SetLogger(ctx =>
            {
                //将日志输出到asp.net core框架
                loggerFactory.CreateLogger(ctx.CategoryName).Log(ctx.Level.To<LogLevel>(), ctx.Message);
            }, enableDefaultOutPut: true);//仍然保留DotNetCommon本身的输出能力

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    var logger = DotNetCommon.Logger.LoggerFactory.CreateLogger<Startup>();
                    logger.LogTrace("LogTrace");
                    logger.LogDebug("LogDebug");
                    logger.LogInformation("LogInformation");
                    logger.LogWarning("LogWarning");
                    logger.LogError("LogError");
                    logger.LogCritical("LogCritical");
                    await context.Response.WriteAsync("Hello World!");
                });
            });
        }
    }
}
