using DotNetCommon;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebUserTest
{
    public class TestService
    {
        public string Name { get; set; }
    }

    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddControllers();

            services.AddSingleton<IHostedService, MyBackGroupService>();

            //���� IComponentWraper
            services.AddScoped(provider => ComponentWraper.Create(new TestService() { Name = "���Է���" }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            IHttpContextAccessor httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>();
            DotNetCommon.User.RegisterIdString(() => httpContextAccessor.HttpContext.User.Identity.Name);
            DotNetCommon.User.RegisterIsAuthenticated(() => httpContextAccessor.HttpContext.User.Identity.IsAuthenticated);
            DotNetCommon.User.RegisterPropertiesDictionary(() => (httpContextAccessor.HttpContext?.Items) ?? new Dictionary<object, object>());

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
