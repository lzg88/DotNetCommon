﻿using DotNetCommon;
using DotNetCommon.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace WebUserTest.Controller
{
    [Route("api/[controller]/[action]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> logger;
        private readonly TestService testService;

        public HomeController(ILogger<HomeController> logger, ComponentWraper<TestService> testService)
        {
            this.logger = logger;
            this.testService = testService.Value;
        }

        /// <summary>
        /// 测试 IdString、IsAuthenticated、Propertites使用
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string> Get()
        {
            var id = DotNetCommon.User.Current.IdString;
            DotNetCommon.User.Current.SetIdStringTemp(123.ToString());
            var id2 = DotNetCommon.User.Current.IdString;
            Debug.Assert(id2 == "123");
            DotNetCommon.User.Current.RemoveIdStringTemp();
            var id3 = DotNetCommon.User.Current.IdString;
            Debug.Assert(id3 == id);

            var isAuthenticated = DotNetCommon.User.Current.IsAuthenticated;
            DotNetCommon.User.Current.SetProperty("name", "name" + DateTime.Now);
            DotNetCommon.User.Current.SetProperty("age", 10);
            var str = $"外层设置: id={id}, name={isAuthenticated}";
            await Task.Run(() =>
            {
                var name = DotNetCommon.User.Current.GetProperty("name");
                var age = DotNetCommon.User.Current.GetProperty<int>("age");
                str += $"\r\n内层获取: name={name}, age={age}";
                DotNetCommon.User.Current.SetProperty("age", age + 10);
                str += $"\r\n内层设置: age={age + 10}";
            });
            var name2 = DotNetCommon.User.Current.GetProperty("name");
            var age2 = DotNetCommon.User.Current.GetProperty("age");
            str += $"\r\n外层获取: name={name2}, age={age2}";
            return str;
        }

        /// <summary>
        /// 测试异步下 IdString、IsAuthenticated、Propertites使用
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string> Get2()
        {
            var id = await DotNetCommon.User.Current.IdStringAsync;
            var isAuthenticated = await DotNetCommon.User.Current.IsAuthenticatedAsync;
            DotNetCommon.User.Current.SetProperty("name", "name" + DateTime.Now);
            DotNetCommon.User.Current.SetProperty("age", 10);
            var str = $"外层设置: id={id}, name={isAuthenticated}";
            await Task.Run(() =>
            {
                var name = DotNetCommon.User.Current.GetProperty("name");
                var age = DotNetCommon.User.Current.GetProperty<int>("age");
                str += $"\r\n内层获取: name={name}, age={age}";
                DotNetCommon.User.Current.SetProperty("age", age + 10);
                str += $"\r\n内层设置: age={age + 10}";
            });
            var name2 = DotNetCommon.User.Current.GetProperty("name");
            var age2 = DotNetCommon.User.Current.GetProperty("age");
            str += $"\r\n外层获取: name={name2}, age={age2}";
            return str;
        }

        [HttpGet]
        public async Task<string> GetInner()
        {
            var str = "";
            await Task.Run(() =>
            {
                var id = DotNetCommon.User.Current.IdString;
                var isAuthenticated = DotNetCommon.User.Current.IsAuthenticated;
                var name = "name" + DateTime.Now;
                var age = 10;
                DotNetCommon.User.Current.SetProperty("name", name);
                DotNetCommon.User.Current.SetProperty("age", age);
                str += $"内层设置: name={name}, age={age}";
            });
            var name3 = DotNetCommon.User.Current.GetProperty("name");
            var age3 = DotNetCommon.User.Current.GetProperty("age");
            str += $"\r\n 外层获取: name={name3}, age:{age3}";
            return str;
        }
    }
}
